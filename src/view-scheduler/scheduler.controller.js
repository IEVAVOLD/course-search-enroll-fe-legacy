(function() {
	'use strict';

	angular
		.module('app.scheduler')
		.filter('formatDays', function() {
			return function(days) {
				var formatedDays = days.map(function(day) {
					if (day !== 'THURSDAY') {
						return day.charAt(0);
					} else {
						return 'R';
					}
				});
				return formatedDays.join('');
			};
		})
		.directive('forceAnimationScope', function() {
			return {
				restrict: 'A',
				link: function(scope, element, attributes) {
					element.data('$$ngAnimateKey', attributes.forceAnimationScope);
				}
			};
		})
		.controller('SchedulerController', SchedulerController);

	/* @ngInject */
	function SchedulerController(constants, VERSION, preferencesService, $timeout, $mdDialog, $scope, $rootScope, $location, toastService, sharedService, $mdSidenav, $mdToast,
								$mdPanel, $route, uiCalendarConfig, searchService, stateService, cartService, scheduleService, courseDigestService, $window) {
		var vm = this;

		vm.DEBUG						= constants.DEBUG;
		vm.VERSION						= VERSION;
		vm.userPrefs					= preferencesService.preferences;
		var currentDateString			= moment('2017-08-20');

		//////////////////////
		// BINDABLE MEMBERS //
		//////////////////////
		vm.viewSwitch					= 'agendaWeek';
		vm.appliedFilters				= (stateService.appliedFilters) ? stateService.appliedFilters : ['seats_OPEN', 'seats_WAITLISTED', 'seats_CLOSED'];
		vm.scheduleSelectedIndex		= 0;
		vm.pollingSum					= 0;
		vm.activeTermIndex				= 0;
		vm.searchPaneContent			= true; // Carry over from cart page
		vm.cart							= [];
		vm.cartData						= [];
		vm.scheduleHours				= [];
		vm.events						= [];
		vm.enrolledClassNumbers			= [];
		vm.scheduledExams				= [];
		vm.blockDays					= {};
		vm.gettingEnrolled				= false;
		vm.hasConflicts					= false;
		vm.isLoading					= false;
		vm.blockError					= false;
		vm.dayTouched					= false;
		vm.noScheduleResults			= false;
		vm.newTerm						= false;
		vm.hasGeneratedResults			= false;
		vm.expiredResults				= false;
		vm.schedulerErrorMessage		= false;
		vm.schedulerSelectedPackages	= {};
		vm.schedulerChangeWarning		= false;
		vm.paneNavClass					= 'active';
		vm.paneScheduleClass			= 'right';
		vm.totalGenerated				= [];
		vm.onScheduler					= true;
		vm.hideSections					= true;
		vm.eventSources					= [ vm.events ];
		vm.colors						= ['#e12c2c', '#6456b7', '#008080', '#044c29', '#00468c', '#e30b5c', '#87421f', '#8e3179', '#736a62'];
		vm.uiConfig = {
			calendar: {
				views: {
					agendaWeek: {
					},
					listWeek: {
						listDayAltFormat: false
					}
				},
				theme: false,
				allDaySlot: false,
				defaultDate: currentDateString,
				height: 'parent',
				minTime: '12:00',
				maxTime: '22:00',
				slotDuration: '00:15:00',
				slotLabelFormat: 'h a',
				slotLabelInterval: '01:00:00',
				editable: false,
				defaultView: 'agendaWeek',
				weekends: false,
				columnFormat: 'ddd',
				timezone: 'America/Chicago',
				businessHours: {
					dow: [ 0, 1, 2, 3, 4, 5, 6 ], // Sunday - Saturday
					start: '8:00', // a start time (8am in this example)
					end: '18:00' // an end time (6pm in this example)
				},
				header: false,
				titleFormat: '[]',
				eventClick: function(date, jsEvent, view) {
					scheduleSectionClick(date, jsEvent, view);
				},
				eventRender: function(event, element, view) {
					if (view.name === 'agendaWeek') {
						var markup = '<a class="fc-time-grid-event fc-v-event fc-event fc-start fc-end ' + event.className + '" style="background-color: ' + event.backgroundColor +
						'" tabindex="0" aria-label="' + event.title + ' ' + event.section + ' ' + moment(event.start).format('h:mmA') + '-' + moment(event.end).format('h:mmA') + ' ' + event.room + ' ' + event.buildingName + '">' +
						'<div class="fc-content">' +
							'<div class="fc-time" data-start="' + moment(event.start).format('h:mmA') + '" data-full="' +
								moment(event.start).format('h:mmA') + '-' + moment(event.end).format('h:mmA') + '">' +
								'<span>' + moment(event.start).format('h:mm A') + '-' + moment(event.end).format('h:mm A') + '</span>' +
							'</div>' +
							'<span class="fc-title">';

						if (event.validationStatus) {
							markup += '<md-icon class="material-icons error">error</md-icon>';
						}

						if ((!event.validationStatus && event.seatsStatus == 'CLOSED') || (!event.validationStatus && event.seatsStatus == 'WAITLISTED')) {
							markup += '<md-icon class="material-icons error">error</md-icon>';
						}

						if (event.courseOrigin == 'ENROLLED') {
							markup += '<md-icon class="material-icons scheduler-enrolled-icon">check_circle</md-icon>';
						}
						if (event.section) {
							markup += event.title + ' (' + event.section + ')';
						} else {
							markup += event.title;
						}
						markup += '</span>';
						if (event.buildingName != null && event.buildingName !== 'PENDING' || event.room != null && event.room !== 'ROOM') {
							markup += '<div class="fc-location">' + event.room + ' ' + event.buildingName + '</div>';
						}
						if ((event.courseOrigin === 'ROADMAP') && (vm.cart[event.cartIndex].enrollmentClassNumber)) {
							markup += '<div class="fc-enrollment-status fc-enrollment-status-locked">';
							markup +=   '<md-icon class="material-icons" role="img" aria-label="locked">lock_outline</md-icon>';
							markup += '</div>';
						}
						markup += '</div><div class="fc-bg"></div></a>';

						return markup;
					} else if (view.name === 'listWeek') {
						return '<tr class="fc-list-item" tabindex="0">' +
									'<td class="fc-list-item-time" aria-label="' +
										moment(event.start).format('dddd') + ' ' + moment(event.start).format('h:mmA') + '-' + moment(event.end).format('h:mmA') + '">' +
										moment(event.start).format('h:mmA') + '-' + moment(event.end).format('h:mmA') +
									'</td>' +
									'<td class="fc-list-item-marker">' +
										'<span class="fc-event-dot" style="background-color:' + event.backgroundColor + '"></span>' +
									'</td>' +
									'<td class="fc-list-item-title">' +
										'<a>' + event.title + '</a>' +
									'</td>' +
								'</tr>';
					}
				}
			}
		};

		/////////////////////
		// EXPOSED METHODS //
		/////////////////////
		vm.changeView					= changeView;
		vm.getCourseStatusMessage		= getCourseStatusMessage;
		vm.renderCalendar				= renderCalendar;
		vm.nextSchedule					= nextSchedule;
		vm.prevSchedule					= prevSchedule;
		vm.generate						= generate;
		vm.getCourseStatusMessage		= getCourseStatusMessage;
		// vm.showSections					= showSections;
		vm.saveSchedule					= saveSchedule;
		vm.addToCart					= addToCart;
		vm.addToCartFromSchedule		= addToCartFromSchedule;
		vm.removeFromCart				= removeFromCart;
		vm.setSection					= setSection;
		vm.toggleSidebar				= toggleSidebar;
		vm.closeSidebar					= closeSidebar;
		vm.scheduleBlocksDialog			= scheduleBlocksDialog;
		vm.schedulePreferences			= schedulePreferences;
		vm.syncMessageDialog			= syncMessageDialog;
		vm.scheduledExamsDialog			= scheduledExamsDialog;
		vm.scheduleSectionClickFromList = scheduleSectionClickFromList;
		vm.printPage					= printPage;
		vm.updatePanePositionClasses	= updatePanePositionClasses;
		vm.toggleSidenavSection			= toggleSidenavSection;
		vm.getCourseData				= getCourseData;
		vm.getCourseSections			= getCourseSections;
		vm.getSessions 					= getSessions;

		activate();

		function activate() {

			$rootScope.$on('run-generate', function() {
				vm.runFindSchedules = true;
			});

			if (Object.keys(vm.userPrefs).length === 0) {
				// Get user preferences
				preferencesService.getPreferences().then(function() {
					vm.userPrefs = preferencesService.preferences;
				}).catch(function(error) { console.error(error); });
			}

			getTerms().then(function() {
				if ((localStorage.selectedTerm) && (vm.terms.length)) {
					var selectedTerm = localStorage.selectedTerm.toString();
					vm.terms.forEach(function(termObj, i) {
						if (termObj.termCode === selectedTerm) {
							vm.activeTermIndex = i;
							vm.selectedTerm = selectedTerm;
						}
					});
				} else {
					localStorage.setItem('selectedTerm', vm.terms[0].termCode);
					vm.selectedTerm = vm.terms[0].termCode;
				}
				vm.gettingEnrolled = true;
				getBlocksList(vm.selectedTerm);
				$scope.$emit('cart-action', [vm.selectedTerm, 'get-cart']);
				vm.loadScheduleResults = true;
				pollValidationStatus();
				getSessions();
			}).catch(function(error) { console.error(error); });

			$scope.$watch('vm.selectedTerm', function(newTerm, oldTerm) {
				if ((oldTerm !== undefined) && (newTerm != oldTerm)) {
					vm.selectedTerm = newTerm;
					localStorage.setItem('selectedTerm', newTerm);
					$route.reload();
				}
			}, true);

			for (var hour = 7; hour < 22; hour++) {
				vm.scheduleHours.push({'time': moment({hour: hour}).format('h:mmA')});
				vm.scheduleHours.push({'time': moment({hour: hour, minute: 15}).format('h:mmA')});
				vm.scheduleHours.push({'time': moment({hour: hour, minute: 30}).format('h:mmA')});
				vm.scheduleHours.push({'time': moment({hour: hour, minute: 45}).format('h:mmA')});
			}
		}

		function getBlocksList(term) {
			scheduleService.getAllBlocks(term)
			.then(function success(data) {
				vm.allBlocks = data;
				if (vm.DEBUG) console.log('blocks', data);
			}).catch(function(error) { console.error(error); });
		}

		function getTerms() {
			return searchService.getTerms()
			.then(function success(data) {
				var terms = [];
				// Add the rest of the terms
				for (var i in data) {
					var term = {
						longDescription: getTermDescription(data[i].termCode, true),
						termCode: data[i].termCode
					};
					terms.push(term);
				}
				vm.terms = terms;
				if (vm.DEBUG)console.info('terms:', terms);
				return vm.terms;
			}).catch(function(error) { console.error(error); });
		}

		/**
		 * Takes termCode id and determine a short or long description of the code i.e. fall 2016 or fall 2016-2017
		 * termCode logic is a follows:
		 *	First digit is century, counting from 20 = 1, second and third are year, fourth is ‘semester'
		 *	1172 = (1 (century), 17 (2016-2017 academic year), 2 (fall)
		 *	Last digit: 2 (fall), 3 (winter), 4 (spring), 6 (summer)
		 * All terms are displayed with the year before to account for fall 2016 in the above example
		 * This is true for all terms except summer.
		 *
		 * @param {String} id String for the termCode
		 * @param {String} isShortDescription (optional) Boolean flag for when a term description with a single year is desired
		 * @returns {String} Either the short or long term description
		 */
		function getTermDescription(id, isShortDescription) {
			if (id !== undefined) {
				var termDescription = '';
				var splitId = id.split('');
				var year = Number(splitId.splice(1, 2).join(''));
				var term = Number(splitId[1]);

				switch (term) {
					case 2:
						termDescription = 'Fall 20';
						if (isShortDescription) termDescription += (year - 1).toString();
						break;
					case 3:
						//Replacing winter with more standard display of Fall
						termDescription = 'Fall 20';
						if (isShortDescription) termDescription += (year - 1).toString();
						break;
					case 4:
						termDescription = 'Spring 20';
						if (isShortDescription) termDescription += (year).toString();
						break;
					case 6:
						termDescription = 'Summer 20';
						if (isShortDescription) termDescription += (year).toString();
						break;
				}

				if (!isShortDescription) {
					termDescription += (term === 6) ? year.toString() : (year - 1).toString() + '-20' + year.toString();
				}
				return termDescription;
			}
		}

		function getCart(refreshState) {
			vm.gettingCart = true;
			return cartService.getCart(vm.selectedTerm).then(function success(data) {
				if (vm.DEBUG) console.log('cartData', data);
				vm.gettingCart = false;
				var cartSize = 0;

				vm.cartData = data;
				cartService.getCartTotal(vm.selectedTerm).then(function(data) {
					vm.hasValidationErrors = (data) ? data.errors : true;
					cartSize = data.items;
				});

				if ((refreshState) && (vm.cartData.length == vm.cart.length)) {
					// var checkedCartIds = sharedService.getCheckedCartIds(vm.cart);
					vm.cartData.map(function(cartDataItem) {
						for (var i = 0; i < vm.cart.length; i++) {
							cartDataItem.details.validationResults = cartDataItem.validationResults;
							cartDataItem.details.classMeetings = cartDataItem.classMeetings;
							cartDataItem.details.enrollmentResults = cartDataItem.enrollmentResults;
							cartDataItem.details.enrollmentClassNumber = cartDataItem.classNumber;
							cartDataItem.details.relatedClassNumber1 = cartDataItem.relatedClassNumber1;
							cartDataItem.details.relatedClassNumber2 = cartDataItem.relatedClassNumber2;
							cartDataItem.details.id = cartDataItem.id;
							cartDataItem.details.credits = cartDataItem.credits;
							cartDataItem.details.classPermissionNumber = cartDataItem.classPermissionNumber;
							cartDataItem.details.cartItemChecked = vm.cart[i].cartItemChecked;
							cartDataItem.details.scheduleColor = vm.cart[i].scheduleColor;
							vm.getCourseStatusMessage(cartDataItem);

							if ((cartDataItem.classNumber !== null) && (vm.cart[i].enrollmentClassNumber == cartDataItem.classNumber)) {
								vm.cart[i] = angular.extend({}, cartDataItem.details);
							} else if ((vm.cart[i].courseId == cartDataItem.courseId) && (vm.cart[i].subject.subjectCode == cartDataItem.subjectCode)) {
								vm.cart[i] = angular.extend({}, cartDataItem.details);
							}
						}
					});
				} else {
					// Unset any cart items, this is needed if a user selects a different cart term
					// vm.cart = [];
					var tempCart = [];

					// The cart data includes the course details on the course.details property. This is different than the search controller
					for (var i = 0; i < vm.cartData.length; i++) {
						vm.cartData[i].details.validationResults = vm.cartData[i].validationResults;
						vm.cartData[i].details.classMeetings = vm.cartData[i].classMeetings;
						vm.cartData[i].details.enrollmentResults = vm.cartData[i].enrollmentResults;
						vm.cartData[i].details.enrollmentClassNumber = vm.cartData[i].classNumber;
						vm.cartData[i].details.relatedClassNumber1 = vm.cartData[i].relatedClassNumber1;
						vm.cartData[i].details.relatedClassNumber2 = vm.cartData[i].relatedClassNumber2;
						vm.cartData[i].details.id = vm.cartData[i].id;
						vm.cartData[i].details.credits = vm.cartData[i].credits;
						vm.cartData[i].details.classPermissionNumber = vm.cartData[i].classPermissionNumber;
						vm.cartData[i].details.scheduleColor = vm.colors[cartSize + i];
						if (vm.cartData[i].enrollmentOptions) { vm.cartData[i].details.enrollmentOptions = vm.cartData[i].enrollmentOptions; }
						if (vm.cartData[i].creditRange) { vm.cartData[i].details.creditRange = vm.cartData[i].creditRange; }

						if (stateService.selectedCartIds) {
							vm.cartData[i].details.cartItemChecked = stateService.selectedCartIds.indexOf(vm.cartData[i].id) != -1;
						} else {
							if ((vm.cart.length) && (vm.cart[i].cartItemChecked)) {
								vm.cartData[i].details.cartItemChecked = true;
							}
						}
						vm.getCourseStatusMessage(vm.cartData[i]);

						tempCart.push(vm.cartData[i].details);
					}
					vm.cart = angular.copy(tempCart);
				}
			}).catch(function(error) { console.error(error); });
		}

		function getEnrolledCourses(termCode) {
			return cartService.getEnrolled(termCode).then(function success(data) {
				vm.enrolledData = data;
				var currentSchedule;
				if (vm.DEBUG) console.log('enrolled data', data);

				if (vm.schedules) {
					if (!vm.hasConflicts) {
						currentSchedule = vm.schedules.feasibleSolutions[vm.scheduleSelectedIndex].schedulingEntities;
					} else {
						currentSchedule = vm.schedules.bestSolution.schedulingEntities;
					}
				}

				vm.enrolled = []; vm.waitlist = []; vm.waitlistData = [];
				// The cart data includes the course details on the course.details property. This is different than the search controller
				for (var i = vm.enrolledData.length - 1; i >= 0; i--) {
					vm.enrolledData[i].details.validationResults = vm.enrolledData[i].validationResults;
					vm.enrolledData[i].details.classMeetings = vm.enrolledData[i].classMeetings;
					vm.enrolledData[i].details.enrollmentResults = vm.enrolledData[i].enrollmentResults;
					vm.enrolledData[i].details.enrollmentClassNumber = vm.enrolledData[i].classNumber;
					vm.enrolledData[i].details.relatedClassNumber1 = vm.enrolledData[i].relatedClassNumber1;
					vm.enrolledData[i].details.classSection = vm.enrolledData[i].sections['0'].classSection;
					vm.enrolledData[i].details.credits = vm.enrolledData[i].sections['0'].numCreditsTaken;
					vm.enrolledData[i].details.creditRange = vm.enrolledData[i].creditRange;
					vm.enrolledData[i].details.studentEnrollmentStatus = vm.enrolledData[i].studentEnrollmentStatus;
					vm.enrolledData[i].details.sections = vm.enrolledData[i].sections;
					vm.enrolledData[i].details.scheduleColor = vm.colors[vm.cartData.length + i];

					if (vm.enrolledData[i].studentEnrollmentStatus == 'E') {
						vm.enrolled.push(vm.enrolledData[i].details);
					} else if (vm.enrolledData[i].studentEnrollmentStatus == 'W') {
						vm.waitlist.push(vm.enrolledData[i].details);
						vm.waitlistData.push(vm.enrolledData[i]);
					}
				}
				vm.gettingEnrolled = false;
			}).catch(function(error) { console.error(error); });
		}
        function getSessions() {
            return searchService.getSessionsByTerm(vm.terms[vm.activeTermIndex].termCode)
                .then(function success(data) {
                    var sessions = [];
                    angular.forEach(data, function(key) {
                        if (key.hasOwnProperty('beginDate')) {
                            key.beginDate = moment.unix(key.beginDate / 1000).format('MMM D');
                        }
                        if (key.hasOwnProperty('endDate')) {
                            key.endDate = moment.unix(key.endDate / 1000).format('MMM D, YYYY');
                        }
                        sessions.push(key);
                    });
                    vm.sessions = sessions;
                    sessions.unshift({ beginDate: 'All', endDate: 'All', sessionCode: '', sessionDescription: 'All' });
                    return vm.sessions;
                });
        }

		function syncMessageDialog(event) {
			$mdDialog.show({
				parent: angular.element(document.body),
				targetEvent: event,
				templateUrl: 'components/dialogs/sync-message-dialog.html',
				clickOutsideToClose: false,
				multiple: true,
				scope: $scope,
				preserveScope: true,
				fullscreen: true,
				controller: SyncMessageController
			});

			/* @ngInject */
			function SyncMessageController($scope, $mdDialog) {
				vm.closeDialog = function() {
					$mdDialog.hide();
				};
			}
		}

		function scheduledExamsDialog(event) {
			$mdDialog.show({
				parent: angular.element(document.body),
				targetEvent: event,
				templateUrl: 'components/scheduled-exams-dialog/scheduled-exams-dialog.html',
				clickOutsideToClose: false,
				multiple: true,
				scope: $scope,
				preserveScope: true,
				fullscreen: true,
				controller: ExamsController
			});

			/* @ngInject */
			function ExamsController($scope, $mdDialog) {
				if (!vm.hasConflicts) {
					$scope.currentSchedule = vm.schedules.feasibleSolutions[vm.scheduleSelectedIndex].schedulingEntities;
				} else {
					$scope.currentSchedule = vm.schedules.bestSolution.schedulingEntities;
				}

				vm.closeDialog = function() {
					$mdDialog.hide();
				};
			}
		}

		/* Since validation messages are a combination of both invalid and valid we check to see if any of the validation results have a 'validationState' of valid.
		 * That means the overall status is valid if there exists at least one valid status. Same is true for 'New' or pending courses. All other states are considered
		 * invalid. We are not displaying any messages on the front end if a course is valid so we do not set that property.
		 * @param course The course of the selected course
		 */
		function getCourseStatusMessage(course) {
			var failedEnrollment = course.enrollmentResults.some(function(message) { return message.enrollmentState === 'Failed'; });
			if (failedEnrollment) {
				course.details.status = {
					'state': 'Enrollment Failed',
					'icon': 'error',
					'class': 'closed'
				};
				return;
			}

			var enrollmentPending = course.enrollmentResults.some(function(message) { return message.enrollmentState === 'New' || message.enrollmentState === 'Added'; });
			if (enrollmentPending) {
				course.details.status = {
					'state': 'Enrollment Pending',
					'icon': 'sync',
					'class': 'error'
				};
				return;
			}

			var pending = course.validationResults.some(function(message) { return message.validationState === 'New' || message.validationState === 'Added'; }),
				invalid;

			if (pending) {
				course.details.status = {
					'state': 'Checking eligibility',
					'icon': 'sync',
					'class': 'error spin'
				};
				return;
			}

			invalid = course.validationResults.some(function(message) { return message.validationState === 'Invalid'; });

			if (invalid) {
				course.details.status = {
					'state': 'Problems with course',
					'icon': 'error',
					'class': 'error'
				};
			}

		}

		function toggleSidebar(name) {
			$mdSidenav(name).toggle();
			vm.sidebarActive = !vm.sidebarActive;
		}

		function closeSidebar(name) {
			$mdSidenav(name).close();
			vm.sidebarActive = false;
		}

		function addToCart() {
			var termCode = vm.courseDetails.termCode,
				subjectCode = vm.courseDetails.subject.subjectCode,
				courseId = vm.courseDetails.courseId,
				courseName = vm.courseDetails.subject.shortDescription + ' ' + vm.courseDetails.catalogNumber,
				classNumber = '',
				hasCheckedSection = false,
				data = {};

			// These properties are bound to the UI and the discussion for example gets checked.
			function setEnrollmentOptionsPostData(section) {
				if (section.classPermissionNumber) data.classPermissionNumber = section.classPermissionNumber;
				if (section.honorsChecked == true) data.honors = true;
				if (section.waitlistChecked == true) data.waitlist = true;
				data.credits = (section.creditRange.indexOf('-') > -1) ? section.credits : Number(section.creditRange);
			}

			// The lecture section of a package has the related class number property because we compare the lecture to the related class
			// number which is typically a lecture
			function setRelatedClassNumberPostData(section) {
				if (section.enrollmentOptions.relatedClassNumber) {
					data.relatedClassNumber1 = section.enrollmentOptions.relatedClasses[0];
					if (section.enrollmentOptions.relatedClasses.length == 2) data.relatedClassNumber2 = section.enrollmentOptions.relatedClasses[1];
				}
			}

			// Set the classNumber for enrollment only if they have selected a section
			if (vm.hasPackages) {
				vm.courseSections.filter(function(section) {
					for (var i = section.packages.length - 1; i >= 0; i--) {
						if (section.packages[i].checked === true) {
							hasCheckedSection = true;
							classNumber = section.packages[i][0].enrollmentClassNumber.toString();
							if (section.packages[i].length > 1) {
								setRelatedClassNumberPostData(section.packages[i][0]);
								setEnrollmentOptionsPostData(section.packages[i][1]);
							} else {
								setEnrollmentOptionsPostData(section.packages[i][0]);
								setRelatedClassNumberPostData(section.packages[i][0]);
							}
						}
					}
				});
			} else {
				if (vm.courseSections) {
					for (var i = vm.courseSections.length - 1; i >= 0; i--) {
						if (vm.courseSections[i].checked === true) {
							hasCheckedSection = true;
							classNumber = vm.courseSections[i].classUniqueId.classNumber.toString();
							setEnrollmentOptionsPostData(vm.courseSections[i]);
							break;
						}
					}
				}
			}

			if (!hasCheckedSection) {
				data.credits = 0;
			}

			if (vm.DEBUG) console.info('addToCart data:', termCode, subjectCode, courseId, classNumber, data);
			cartService.addToCart(termCode, subjectCode, courseId, classNumber, data)
				.then(function success(data) {
					if (data.error && data.error.status == 500) {
						toastService.infoMessage(courseName + ' Cannot add to cart server error');
					} else {
						$scope.$emit('cart-action', [vm.selectedTerm, 'add-to-cart']);
						var toast;
						if (!vm.searchPaneContent) {
							toast = $mdToast.simple()
									.textContent(courseName + ' Added to cart')
									.action('Go to cart')
									.position('bottom center')
									.hideDelay(5000)
									.highlightAction(false);
						} else {
							toast = $mdToast.simple()
									.textContent(courseName + ' Updating')
									.position('bottom center')
									.hideDelay(5000)
									.highlightAction(false);

							$mdToast.show(toast).then(function(response) {
								if (response == 'ok') {
									// SINCE TAB CONTROLLER ONLY FIRES ON PAGE LOAD I HAVE TO MANUALLY FIRE THE EVENT TO ADD THE ACTIVE STATE. NEED TO REFACTOR.
									$location.path('/my-courses');
									$timeout(function() {
										angular.element('.tab-courses').trigger('click');
									}, 100);
								}
							});

							pollValidationStatus();
							if (vm.runFindSchedules) {
								$mdSidenav('right').toggle();
								generate();
								vm.runFindSchedules = false;
							}
						}
					}
				}).catch(function(error) { console.log(error); });
		}

		function addToCartFromSchedule() {
			var selectedSchedule = (vm.schedules.feasibleSolutions.length) ?
				vm.schedules.feasibleSolutions[vm.scheduleSelectedIndex].schedulingEntities :
				vm.schedules.bestSolution.schedulingEntities;

			stateService.selectedCartIds = sharedService.getCheckedCartIds(vm.cart);

			var selectedCourses = selectedSchedule.filter(function(scheduledCourse) { return scheduledCourse.courseOrigin == 'ROADMAP'; });

			selectedCourses.forEach(function(course, i) {
				$timeout(function() {
					var termCode = vm.selectedTerm,
						subjectCode = course.subjectCode,
						courseId = course.courseId,
						classNumber = course.selectedEnrollmentPackage.classNumber,
						data = {};

					if (vm.cartData[course.cartIndex].classPermissionNumber) data.classPermissionNumber = vm.cart[course.cartIndex].classPermissionNumber;
					if (vm.cartData[course.cartIndex].honors == 'Y') data.honors = true;
					if (vm.cartData[course.cartIndex].waitlist == 'Y') data.waitlist = true;
					data.credits = (course.selectedEnrollmentPackage.creditRange.indexOf('-') != -1) ?
									vm.cart[course.cartIndex].credits :
									Number(course.selectedEnrollmentPackage.creditRange);

					if (course.selectedEnrollmentPackage.enrollmentOptions.relatedClassNumber) {
						data.relatedClassNumber1 = course.selectedEnrollmentPackage.enrollmentOptions.relatedClasses[0];
						if (course.selectedEnrollmentPackage.enrollmentOptions.relatedClasses.length == 2) {
							data.relatedClassNumber2 = course.selectedEnrollmentPackage.enrollmentOptions.relatedClasses[1];
						}
						if (vm.DEBUG) console.log('related class set to:', data.relatedClassNumber);
					}

					if (vm.DEBUG) console.log('add to cart data', termCode, subjectCode, courseId, classNumber, data);

					cartService.addToCart(termCode, subjectCode, courseId, classNumber, data)
						.then(function success(data) {
							if (!data.error) {
								if (selectedCourses.length - 1 == i) {
									pollValidationStatus();
								}
							} else {
								toastService.infoMessage('Cannot add to cart server error');
							}
						}).catch(function(error) { console.error(error); });
				}, 1500 * i);
			});
		}

		function removeFromCart() {
			var checkedCourses = sharedService.getCheckedCartItems(vm.cart),
				numberOfCourses = checkedCourses.length,
				courseNames = [],
				confirm;

			angular.forEach(checkedCourses, function(cartObj) {
				courseNames.push(cartObj.subject.shortDescription + ' ' + cartObj.catalogNumber);
			});

			if (numberOfCourses > 1) {
				confirm = $mdDialog.confirm()
					.title('Remove courses?')
					.textContent('Are you sure you want to remove ' + courseNames.toString() + ' from your cart?')
					.ariaLabel('remove selected courses')
					.ok('Remove')
					.cancel('Cancel');
			} else {
				confirm = $mdDialog.confirm()
					.title('Remove course?')
					.textContent('Are you sure you want to remove ' + courseNames.toString() + ' from your cart?')
					.ariaLabel('remove selected course')
					.ok('Remove')
					.cancel('Cancel');
			}

			$mdDialog.show(confirm).then(function() {
				checkedCourses.forEach(function(cartObj) {
					cartService.removeFromCart(vm.selectedTerm, cartObj.subject.subjectCode, cartObj.courseId).then(function() {
						getCart();
					}).catch(function(error) { console.error(error); });
				});
				toastService.infoMessage(courseNames.toString() + ' Removed from cart');
			}, function() { });
		}


		function getCourseData(result, $index) {
			vm.courseDetails = result;
			toggleSidenavSection(result, $index);
		}

		function toggleSidenavSection(result, cartIndex) {
			if ($mdSidenav('right').isOpen()) { $mdSidenav('right').toggle(); return;}

			vm.hideSections = true;
			vm.loadingSectionPane = true;
			vm.selectedSection = false;
			vm.updatingEnrolledErrors = [];
			if (!result.hasOwnProperty('studentEnrollmentStatus')) {
				vm.courseDetails = vm.cartData[cartIndex].details;
			} else if (result.studentEnrollmentStatus == 'E') {
				vm.courseDetails = vm.enrolled[cartIndex];
			} else if  (result.studentEnrollmentStatus == 'W') {
				vm.courseDetails = vm.waitlist[cartIndex];
			} else {
				vm.courseDetails = vm.dropped[cartIndex];
			}

			$mdSidenav('right', true).onClose(function() {
				// Only cart courses
				if (!vm.courseDetails.hasOwnProperty('studentEnrollmentStatus')) {
					var selectedCartId = sharedService.getCartIdFromCourse(vm.cartData, vm.courseDetails),
						selectedPackages = [],
						totalSections = 0,
						numSelectedPackages = 0;

					if (vm.hasPackages) {
						selectedPackages = sharedService.getSelectedPackages(vm.courseSections);
						vm.courseSections.forEach(function(coursePackages) { totalSections += coursePackages.packages.length; });
					} else {
						selectedPackages = sharedService.getSelectedSections(vm.courseSections);
						totalSections = vm.courseSections.length;
					}
					numSelectedPackages = selectedPackages.length;

					if (numSelectedPackages && numSelectedPackages != totalSections) {
						vm.schedulerSelectedPackages[selectedCartId] = selectedPackages;
					} else {
						if (vm.schedulerSelectedPackages.hasOwnProperty(selectedCartId))
							delete vm.schedulerSelectedPackages[selectedCartId];
					}

					if (vm.DEBUG) console.log('selected packs', vm.schedulerSelectedPackages);
				}

				vm.selectAllOrNothing = false;
				vm.sectionMulti = false;
				vm.courseSections = [];
				vm.sections = [];
				vm.hasPackages = false;
				vm.selectedSection = false;
				vm.selectedFromSchedule = false;
				vm.hideSections = true;
			});

			if ($mdSidenav('right').isOpen() == false) {
				$mdSidenav('right').toggle().then(function() {
					if (!result.hasOwnProperty('studentEnrollmentStatus')) {
						var query,
							isLocked = false,
							cartCourse = vm.cartData[cartIndex],
							matches = '',
							classMeetingsNoExam = [],
							classNumber = (vm.cartData[cartIndex].classNumber) ? vm.cartData[cartIndex].classNumber : vm.cartData[cartIndex].enrollmentClassNumber;
							vm.loadingSections = true;

						if (!result.enrollmentClassNumber && classNumber && !vm.noScheduleResults) {
							if (result.classMeetings && result.classMeetings.length) {
								classMeetingsNoExam = result.classMeetings.filter(function(meeting) { return meeting.meetingType != 'EXAM'; })
							}
							if (result.meetings && result.meetings.length) {
								classMeetingsNoExam = result.meetings.filter(function(meeting) { return meeting.meetingType != 'EXAM'; }); 
							}
							if (result.classMeetingsScheduler && result.classMeetingsScheduler.length) {
								classMeetingsNoExam = result.classMeetingsScheduler.filter(function(meeting) { return meeting.meetingType != 'EXAM'; }); 
							}

							classMeetingsNoExam.forEach(function(meeting){ matches += '{"match":{"sections.sectionNumber":"' + meeting.sectionNumber.toString() + '"}},'});
							matches = matches.slice(0,-1);
							query = '{"size":"1000","query":{"constant_score":{"filter":{"bool":{"must":[{"term":{"courseId":"' + cartCourse.courseId.toString() + '"}},' + matches + ']}}}}}';

							if (cartCourse.classNumber) isLocked = true;
						} else  {
							if (!result.enrollmentClassNumber && !vm.cart[cartIndex].classNumber && !result.classMeetingsScheduler) {
								return getCourseSections().then(function() { vm.loadingSectionPane = false; vm.hideSections = false; });
							}
							if (result.classMeetings && result.classMeetings.length) {
								classMeetingsNoExam = result.classMeetings.filter(function(meeting) { return meeting.meetingType != 'EXAM'; }) 
							}
							if (result.meetings && result.meetings.length) {
								classMeetingsNoExam = result.meetings.filter(function(meeting) { return meeting.meetingType != 'EXAM'; }); 
							}
							if (result.classMeetingsScheduler && result.classMeetingsScheduler.length) {
								classMeetingsNoExam = result.classMeetingsScheduler.filter(function(meeting) { return meeting.meetingType != 'EXAM'; }); 
							}

							classMeetingsNoExam.forEach(function(meeting){ matches += '{"match":{"sections.sectionNumber":"' + meeting.sectionNumber.toString() + '"}},'});
							matches = matches.slice(0,-1);
							query = '{"size":"1000","query":{"constant_score":{"filter":{"bool":{"must":[{"term":{"courseId":"' + cartCourse.courseId.toString() + '"}},' + matches + ']}}}}}';

							if (result.enrollmentClassNumber) {
								isLocked = true;
							}
						}

						// If this is true that means there are likely no class meetings
						if (!matches) {
							query = '{"size":"1000","query":{"term":{"enrollmentClassNumber":' + result.enrollmentClassNumber + '}}}';
						}

						getCourseSections().then(function() {
							vm.loadingSections = false;
						});

						searchService.getEnrollmentPackageByNumber(vm.selectedTerm, query).then(function(data) {
							if (data.error) {
								toastService.infoMessage(data.errorDescription);
								vm.loadingSectionPane = false;
								return;
							}
							var foundSection;

							if (data.hits.hits.length > 1) {
								foundSection = data.hits.hits.filter(function(section) {
									if (section._source.enrollmentOptions.relatedClassNumber) {
										return section._source.enrollmentOptions.relatedClasses["0"] == (Number(result.relatedClassNumber1));
									} else {
										if (result.hasOwnProperty('subject')) {
											return (section._source.courseId == result.courseId) && (section._source.subjectCode == result.subject.subjectCode);
										} else {
											return (section._source.courseId == result.courseId) && (section._source.subjectCode == result.subjectCode);
										}
									}
								})[0]._source;
							} else {
								foundSection = data.hits.hits[0]._source;
							}

							if (isLocked || result.enrollmentClassNumber) {
								foundSection.sections.map(function(section) { section.checked = true; });
							}

							var numSections = foundSection.sections.length;
							if (numSections > 1) {
								foundSection.classMeetings = foundSection.sections[0].classMeetings;
								var parent = angular.copy(foundSection.sections[0]);
								foundSection.sections[numSections-1].showPackage = true;
								if (cartCourse.enrollmentClassNumber) foundSection.checked = true;
								foundSection.sections[1].packageEnrollmentStatus = foundSection.packageEnrollmentStatus;
								foundSection.sections[1].enrollmentOptions = foundSection.enrollmentOptions;
								foundSection.sections[1].enrollmentRequirementGroups = foundSection.enrollmentRequirementGroups;
								foundSection.sections[1].creditRange = foundSection.creditRange;
								foundSection.sections[1].credits = cartCourse.credits;
								if (cartCourse.waitlist == 'Y') foundSection.sections[1].waitlistChecked = true;
								if (cartCourse.honors == 'Y') foundSection.sections[1].honorsChecked = true;
								if (cartCourse.classPermissionNumber) foundSection.sections[1].classPermissionNumber = cartCourse.classPermissionNumber;
								parent.packages = [foundSection.sections];
								vm.selectedSection = [Object.assign(parent, foundSection)];
								vm.parentPackageType = foundSection.sections[0].type;
								vm.hasPackages = true;

								if (vm.DEBUG)console.log('selected section', vm.selectedSection);
							} else {
								vm.hasPackages = false;
								foundSection.packages = foundSection.sections;
								foundSection.credits = cartCourse.credits;
								if (cartCourse.waitlist == 'Y') foundSection.waitlistChecked = true;
								if (cartCourse.honors == 'Y') foundSection.honorsChecked = true;
								if (cartCourse.classPermissionNumber) foundSection.classPermissionNumber = cartCourse.classPermissionNumber;
								if (cartCourse.enrollmentClassNumber) foundSection.checked = true;
								vm.selectedSection = [Object.assign(foundSection.sections[0], foundSection)];
								vm.selectedSection[0].show = true;
							}
							vm.loadingSectionPane = false;
						}).catch(function(error) { console.error(error); });
					} else {
						vm.loadingSections = false;
						vm.loadingSectionPane = false;
						vm.hideSections = false;

						var matches = '', query, foundSection;
						if (angular.isUndefined(result.sections)) {
							result.classMeetings.forEach(function(section){ matches += '{"match":{"sections.sectionNumber":"' + section.sectionNumber.toString() + '"}},'});
						} else {
							result.sections.forEach(function(section){ matches += '{"match":{"sections.sectionNumber":"' + section.classSection.toString() + '"}},'});
						}
						matches = matches.slice(0,-1);
						query = '{"size":"1000","query":{"constant_score":{"filter":{"bool":{"must":[{"term":{"courseId":"' + result.courseId.toString() + '"}},' + matches + ']}}}}}';

						searchService.getEnrollmentPackageByNumber(vm.selectedTerm, query).then(function(data) {
							if (data.error) {
								toastService.infoMessage(data.errorDescription);
								vm.loadingSectionPane = false;
								return;
							}

							if (data.hits.hits.length > 1) {
								foundSection = data.hits.hits.filter(function(section) {
									if (section._source.enrollmentOptions.relatedClassNumber) {
										return section._source.enrollmentOptions.relatedClasses["0"] == (Number(result.relatedClassNumber1));
									} else {
										return (section._source.courseId == result.courseId) && (section._source.subjectCode == result.subject.subjectCode);
									}
								})[0]._source;
							} else {
								foundSection = data.hits.hits[0]._source;
							}

							foundSection.sections.forEach(function(section, index) {
								section.creditRange = foundSection.creditRange;
								section.credits = result.sections[0].numCreditsTaken.toString();
								section.honors = result.honors;
								section.honorsChecked = (result.honors == 'Y' || result.honors) ? true : false;
								section.instructionMode = foundSection.sections[0].instructionMode;
								section.classMaterials = foundSection.sections[0].classMaterials;
								section.addConsent = foundSection.sections[0].addConsent;
								section.footnotes = foundSection.sections[0].footnotes;
								section.comB = foundSection.sections[0].comB;
								section.dropConsent = foundSection.sections[0].dropConsent;
								section.instructorProvidedClassDetails = foundSection.instructorProvidedClassDetails;
								section.enrollmentOptions = foundSection.enrollmentOptions;
								section.enrollmentRequirementGroups = foundSection.enrollmentRequirementGroups;
							});

							var numSections = foundSection.sections.length;
							foundSection.credits = result.sections[0].numCreditsTaken;
							if (numSections > 1) {
								vm.hasPackages = true;
								vm.parentPackageType = foundSection.sections[0].type;
								foundSection.sections[0].creditRange = '';
								foundSection.sections[1].enrolled = true;
								foundSection.sections[1].show = true;
								foundSection.sections[0].packages = [foundSection.sections];
								vm.courseSections = [foundSection.sections[0]];
							} else {
								vm.hasPackages = false;
								foundSection.sections[0].enrolled = true;
								foundSection.sections[0].show = true;
								vm.courseSections = foundSection.sections;
							}

							vm.loadingSectionPane = false;
						});

					

						// if (result.sections.length > 1) {
						// 	vm.hasPackages = true;
						// 	vm.parentPackageType = result.sections[0].type;
						// 	result.sections[0].packages = [angular.copy(result.sections)];
						// 	result.sections.splice(1, 1);
						// 	vm.courseSections = [result.sections[0]];
						// } else {
						// 	result.sections[0].show = true;
						// 	vm.courseSections = result.sections;
						// }

						if (vm.DEBUG) console.log('course sections enrolled', result);
					}
				}).catch(function(error) { console.error(error); });
			} else {
				$mdSidenav('right').toggle();
			}
		}

		function getCourseSections() {
			return searchService.getPackages(vm.selectedTerm, vm.courseDetails.courseId, vm.courseDetails.subject.subjectCode).then(function(data) {
				vm.loadingSections = false;
				vm.hasPackages = false;
				vm.sectionMulti = false;
				vm.isSearchingCourse = true;
				vm.hasOptionalSection = false;
				var checkedIndex;

				// If there is only 1 section (Lecture) in a package we assume there are no packages and hasPackages remains false.
				var allLectures = angular.copy(data.map(function(coursePackage) {
					var lecture = coursePackage.sections.filter(function(section) {
						// Set package enrollment status
						section.packageEnrollmentStatus = coursePackage.packageEnrollmentStatus;
						section.docId = coursePackage.docId;
						section.enrollmentRequirementGroups = coursePackage.enrollmentRequirementGroups;
						section.enrollmentOptions = coursePackage.enrollmentOptions;
						section.instructorProvidedClassDetails = coursePackage.instructorProvidedClassDetails;
						if (coursePackage.creditRange) {
							section.creditRange = coursePackage.creditRange;
						}
						if (!vm.hasPackages && coursePackage.sections.length > 1) {
							vm.hasPackages = true;
						}
						return true;
					});
					return lecture[0];
				}));

				// Set the name of the parent section
				if (allLectures[0]) {
					vm.parentPackageType = allLectures[0].type;
				}

				if (vm.hasPackages) {
					// Get the section numbers from allLectures array then filter out the dups
					var sectionNums = allLectures.map(function(e) { return e.sectionNumber; });
					sectionNums = sectionNums.filter(function(item, pos) { return sectionNums.indexOf(item) == pos; });

					// Reduce allLectures array to only 1 lecture per section number
					var lectures = sectionNums.map(function(section) {
						for (var i = allLectures.length - 1; i >= 0; i--) {
							if (allLectures[i].sectionNumber == section) {
								return angular.copy(allLectures[i]);
							}
						}
					});

					var optionalLecturePackages = [];
					// Search the 'data' for all packages and add a packages property to each lecture
					sectionNums.forEach(function(sectionNum) {
						var lectureWithPackages = [];
						for (var i = data.length - 1; i >= 0; i--) {
							for (var j = data[i].sections.length - 1; j >= 0; j--) {
								if ((data[i].sections[j].type == vm.parentPackageType) && (data[i].sections[j].sectionNumber == sectionNum)) {
									// Determine any checked section
									vm.cartData.map(function(cartDataItem) {
										if (data[i].enrollmentClassNumber.toString() === cartDataItem.classNumber) {
											// Packages always have the same length of classes until they don't, which means the lab for instance is optional
											// in this case we need to modify its display on the front end.
											if (data[i].sections.length > 1) {
												vm.sectionMaxLength = data[i].sections;
												// We have to check the right lecture with a related class number otherwise mutilple discussion get checked
												if ((cartDataItem.relatedClassNumber1 === null) && (!vm.hasOptionalSection)) {
													data[i].sections[j + 1].checked = true;
													data[i].sections.checked = true;
													checkedIndex = i;
													data[i].sections[j].instructorProvidedClassDetails = data[i].instructorProvidedClassDetails;
													data[i].sections[j + 1].packageEnrollmentStatus = data[i].packageEnrollmentStatus;
													if (cartDataItem.classPermissionNumber !== null) {
														data[i].sections[j + 1].classPermissionNumber = cartDataItem.classPermissionNumber;
														data[i].sections[j + 1].hasPermissionNumber = true;
													}
													if (cartDataItem.hasOwnProperty('credits')) {
														data[i].sections[j + 1].credits = cartDataItem.credits;
													}
													if (cartDataItem.waitlist == 'Y') {
														data[i].sections[j + 1].waitlistChecked = true;
													}
													if (cartDataItem.honors == 'Y') {
														data[i].sections[j + 1].honorsChecked = true;
													}
													// vm.selectedSection = { packages: [data[i].sections] };
												} else {
													// Most of the the time a related class number corresponses to a LAB section but sometimes it is a LEC section
													// this accounts for both cases
													if (data[i].sections.length != 1) {
														if (cartDataItem.relatedClassNumber1 === data[i].sections[1].classUniqueId.classNumber.toString() ||
															cartDataItem.relatedClassNumber1 === data[i].sections[0].classUniqueId.classNumber.toString()) {
															data[i].sections[j + 1].checked = true;
															data[i].sections.checked = true;
															checkedIndex = i;
															data[i].sections[j].instructorProvidedClassDetails = data[i].instructorProvidedClassDetails;
															data[i].sections[j + 1].packageEnrollmentStatus = data[i].packageEnrollmentStatus;
															if (cartDataItem.classPermissionNumber !== null) {
																data[i].sections[j + 1].classPermissionNumber = cartDataItem.classPermissionNumber;
																data[i].sections[j + 1].hasPermissionNumber = true;
															}
															if (cartDataItem.hasOwnProperty('credits')) {
																data[i].sections[j + 1].credits = cartDataItem.credits;
															}
															if (cartDataItem.waitlist == 'Y') {
																data[i].sections[j + 1].waitlistChecked = true;
															}
															if (cartDataItem.honors == 'Y') {
																data[i].sections[j + 1].honorsChecked = true;
															}
															// vm.selectedSection = { packages: [data[i].sections] };
														}
													} else if (cartDataItem.relatedClassNumber1 === data[i].sections[0].classUniqueId.classNumber.toString()) {
														data[i].sections[j].checked = true;
														data[i].sections.checked = true;
														checkedIndex = i;
														data[i].sections[j].instructorProvidedClassDetails = data[i].instructorProvidedClassDetails;
														if (cartDataItem.classPermissionNumber !== null) {
															data[i].sections[j].classPermissionNumber = cartDataItem.classPermissionNumber;
															data[i].sections[j].hasPermissionNumber = true;
														}
														if (cartDataItem.hasOwnProperty('credits')) {
															data[i].sections[j].credits = cartDataItem.credits;
														}
														if (cartDataItem.waitlist == 'Y') {
															data[i].sections[j].waitlistChecked = true;
														}
														if (cartDataItem.honors == 'Y') {
															data[i].sections[j].honorsChecked = true;
														}
														// vm.selectedSection = { packages: [data[i].sections] };
													}
												}
												//I would like this to work here but view is not updating.
											} else if (cartDataItem.relatedClassNumber1 === null) {
												data[i].sections[j].checked = true;
												data[i].sections.checked = true;
												checkedIndex = i;
												vm.hasOptionalSection = true;
												if (cartDataItem.classPermissionNumber !== null) {
													data[i].sections[j].classPermissionNumber = cartDataItem.classPermissionNumber;
													data[i].sections[j].hasPermissionNumber = true;
												}
												if (cartDataItem.hasOwnProperty('credits')) {
													data[i].sections[j].credits = cartDataItem.credits;
												}
												if (cartDataItem.waitlist == 'Y') {
													data[i].sections[j].waitlistChecked = true;
												}
												if (cartDataItem.honors == 'Y') {
													data[i].sections[j].honorsChecked = true;
												}
												// vm.selectedSection = { packages: [data[i].sections] };
											}
										}
									});
									//Set the classNumber for the package used for enrollment on the parentPackageType
									data[i].sections[j].enrollmentClassNumber = data[i].enrollmentClassNumber;
									data[i].sections[j].instructorProvidedClassDetails = data[i].instructorProvidedClassDetails;
									if (data[i].sections.length > 1) {
										data[i].sections[j + 1].packageEnrollmentStatus = data[i].packageEnrollmentStatus;
										vm.sectionMaxLength = data[i].sections;
										lectureWithPackages.push(data[i].sections);
									} else {
										data[i].sections[j].packageEnrollmentStatus = data[i].packageEnrollmentStatus;
										var tmpLecture = angular.copy(data[i].sections[0]);
										tmpLecture.packages = [data[i].sections];
										optionalLecturePackages.push(tmpLecture);
									}
								}
								if (data[i].sections.length == 1) {
									// Packages always have the same length of classes until they don't, which means the lab for instance is optional
									// in this case we need to modify its display on the front end.
									vm.hasOptionalSection = true;
								}
							}
						}
						for (var k = lectures.length - 1; k >= 0; k--) {
							if (lectures[k].sectionNumber == sectionNum) {
								lectures[k].packages = [];
								lectureWithPackages.map(function(section) {
									return lectures[k].packages.push(section);
								});
							}
						}
					});
					lectures = lectures.concat(optionalLecturePackages);
					lectures.map(function(lecture) { return lecture.packages.map(function(pack) { if(pack.checked) lecture.checked=true; }); });
					var selectedIndex = '';
					for (var x = lectures.length - 1; x >= 0; x--) {
						for (var y = lectures[x].packages.length - 1; y >= 0; y--) {
							if (!vm.hasOptionalSection) {
								if (lectures[x].packages[y][1].checked == true) {
									selectedIndex = { lecIndex: x, pacIndex: y};
								}
							} else {
								if (lectures[x].checked == true) {
									selectedIndex = { lecIndex: x, pacIndex: 0};
								}
							}
						}
					}
					if (selectedIndex) {
						vm.courseSections = lectures;

						vm.courseSectionsData = angular.copy(vm.courseSections);
						vm.courseSectionsData[selectedIndex.lecIndex].packages[selectedIndex.pacIndex].checked = true;
						vm.selectedSectionFilters = vm.appliedFilters;
						// vm.updateSectionFilters(selectedIndex);

						vm.loadingSections = false;
					} else {
						vm.courseSections = lectures;

						vm.courseSectionsData = angular.copy(vm.courseSections);
						vm.selectedSectionFilters = vm.appliedFilters;
						// vm.updateSectionFilters(selectedIndex);

						vm.sectionMulti = true;

						var cartId = sharedService.getCartIdFromCourse(vm.cartData, vm.courseDetails);
						var selectedPackagesFromCourse = vm.schedulerSelectedPackages[cartId];

						if (vm.schedulerSelectedPackages.hasOwnProperty(cartId)) {
							vm.courseSections.forEach(function(section) {
								section.packages.forEach(function(packages) {
									selectedPackagesFromCourse.forEach(function(docId) {
										if (packages.length > 1 && packages[1].docId == docId) {
											packages[1].multiChecked = true;
										} else if (packages[0].docId == docId) {
											packages[0].multiChecked = true;
										}
									});
								});
							});
							vm.loadingSections = false;
							return;
						}

						vm.courseSections.forEach(function(section) {
							section.packages.forEach(function(packages) {
								if (packages.length > 1) {
									packages[1].multiChecked = true;
								} else {
									packages[0].multiChecked = true;
								}
							});
						});

						vm.loadingSections = false;
					}

				} else {
					// Set the selected course in the UI
					for (var i = allLectures.length - 1; i >= 0; i--) {
						for (var j = vm.cartData.length - 1; j >= 0; j--) {
							if (allLectures[i].classUniqueId.classNumber.toString() === vm.cartData[j].classNumber) {
								allLectures[i].checked = true;
								checkedIndex = i;
								if (vm.cartData[j].classPermissionNumber !== null) {
									allLectures[i].classPermissionNumber = vm.cartData[j].classPermissionNumber;
									allLectures[i].hasPermissionNumber = true;
								}
								if (vm.cartData[j].hasOwnProperty('credits')) {
									allLectures[i].credits = vm.cartData[j].credits;
								}
								if (vm.cartData[j].waitlist == 'Y') {
									allLectures[i].waitlistChecked = true;
								}
								if (vm.cartData[j].honors == 'Y') {
									allLectures[i].honorsChecked = true;
								}
								// vm.selectedSection = [allLectures[i]];
							}
						}
					}

					if (checkedIndex != undefined) {
						vm.sectionMaxLength = [allLectures[0]];
						vm.courseSections = allLectures;

						vm.courseSectionsData = angular.copy(vm.courseSections);
						vm.selectedSectionFilters = vm.appliedFilters;
						// vm.updateSectionFilters();

						vm.loadingSections = false;
					} else {
						// Get enrollment options before we update the UI with sections
						vm.sectionMaxLength = [allLectures[0]];
						vm.courseSections = angular.copy(allLectures);

						vm.courseSectionsData = angular.copy(vm.courseSections);
						vm.selectedSectionFilters = vm.appliedFilters;
						// vm.updateSectionFilters();

						vm.sectionMulti = true;

						var cartId = sharedService.getCartIdFromCourse(vm.cartData, vm.courseDetails);
						var selectedPackagesFromCourse = vm.schedulerSelectedPackages[cartId];

						if (vm.schedulerSelectedPackages.hasOwnProperty(cartId)) {
							vm.courseSections.forEach(function(section) {
								selectedPackagesFromCourse.forEach(function(docId) {
									if (section.docId == docId) section.multiChecked = true;
								});
							});
							vm.loadingSections = false;
							return;
						}

						vm.courseSections.forEach(function(section) {
							section.multiChecked = true;
						});

						vm.loadingSections = false;
					}

				}
			}).catch(function(error) { console.error(error); });
		}

		function saveSchedule(event) {
			var cartItemIds = sharedService.getCheckedCartIds(vm.cart);
			if (cartItemIds.length) {
				$mdDialog.show({
					parent: angular.element(document.body),
					targetEvent: event,
					templateUrl: 'components/dialogs/save-schedule-dialog.html',
					clickOutsideToClose: false,
					multiple: true,
					scope: $scope,
					preserveScope: true,
					fullscreen: true,
					controller: SaveScheduleController
				}, function() { stateService.selectedCartIds = []; });
			} else {
				$mdDialog.show(
					$mdDialog.alert()
						.openFrom('#save-schedule')
						.clickOutsideToClose(true)
						.title('No selected courses in your cart')
						.textContent('Please select at least one course in your cart for schedule generation.')
						.ariaLabel('Please select at least one course in your cart for schedule generation.')
						.ok('Ok')
				);
			}

			/* @ngInject */
			function SaveScheduleController($location, sharedService, $scope) {
				vm.hasValidationErrors = false;
				vm.validationComplete = false;
				var selectedCourseIndex, scheduleRequest;

				if (!vm.hasConflicts) {
					vm.selectedSchedule = vm.schedules.feasibleSolutions[vm.scheduleSelectedIndex].schedulingEntities;
					scheduleRequest = vm.schedules.feasibleSolutions[vm.scheduleSelectedIndex].schedulingRequest;
				} else {
					vm.selectedSchedule = vm.schedules.bestSolution.schedulingEntities;
					scheduleRequest = vm.schedules.bestSolution.schedulingRequest;
				}
				scheduleRequest.cartItems.forEach(function(cartId) {
					vm.cart.forEach(function(cartObj, i) { if (cartObj.id == cartId) { selectedCourseIndex = i; } });

					vm.selectedSchedule.forEach(function(scheduledCourse) {
						if (scheduledCourse.courseId == vm.cart[selectedCourseIndex].courseId &&
							scheduledCourse.subjectCode == vm.cart[selectedCourseIndex].subject.subjectCode) {
							scheduledCourse.cartIndex = selectedCourseIndex;
						}
					});
				});

				if (vm.DEBUG) console.log('selected schedule', vm.selectedSchedule);

				vm.closeDialog = function() {
					stateService.selectedCartIds = [];
					$mdDialog.hide();
				};

				vm.goToCart = function() {
					// Save checked cart courses for return to my courses
					stateService.selectedCartIds = sharedService.getCheckedCartIds(vm.cart);
					$location.path('/my-courses');
				};

				vm.saveScheduleSave = function() {
					vm.hasPendingValidation = true;
					vm.addToCartFromSchedule();
				};

				vm.hasEnrollmentOptions = function(course) {
					return sharedService.hasEnrollmentOptions(course);
				};

				vm.createCreditRange = function(section) {
					function range(start, stop) {
						var a = [start], b = start;
						while (b < stop) { b += 1; a.push(b); }
						return a;
					}
					if (section.creditRange.indexOf('-') === -1) {
						return;
					} else {
						var min = Number(section.creditRange.split('-')[0]),
							max = Number(section.creditRange.split('-')[1]);

						return range(min, max);
					}
				};

				vm.sumCredits = function(cartObj) {
					var sum = 0, min = 0, max = 0, hasRange = false;

					angular.forEach(cartObj, function(cartObjItem) {
						if (cartObjItem.courseOrigin == 'ROADMAP') {
							if ((cartObjItem.selectedEnrollmentPackage.creditRange.indexOf('-') == -1)) {
								var credits = parseInt(cartObjItem.selectedEnrollmentPackage.creditRange, 10);
								sum += credits;
								min += credits;
								max += credits;
							} else {
								hasRange = true;
								min += parseInt(cartObjItem.selectedEnrollmentPackage.creditRange.split('')[0], 10);
								max += parseInt(cartObjItem.selectedEnrollmentPackage.creditRange.split('')[2], 10);
							}
						}
					});

					var enrolledCredits = vm.enrolledData.reduce(function(enrolledSum, course) {
						return enrolledSum + parseInt(course.creditRange);
					}, 0);

					if (!hasRange) {
						vm.enrolledCreditsSum = sum + enrolledCredits;
						return sum;
					} else {
						vm.enrolledCreditsSum = (min + enrolledCredits) + '-' + (max + enrolledCredits);
						return min + '-' + max;
					}
				};
			}
		}

		function clearSchedule() {
			uiCalendarConfig.calendars.scheduler.fullCalendar('removeEvents');
		}

		function prevSchedule() {
			clearSchedule();
			if (vm.scheduleSelectedIndex !== 0) vm.scheduleSelectedIndex--;
			var currentSchedule = vm.schedules.feasibleSolutions[vm.scheduleSelectedIndex].schedulingEntities;

			clearSchedule();
			plotCourses(vm.schedules.feasibleSolutions[vm.scheduleSelectedIndex]);
			createTempCart(currentSchedule);
		}

		function nextSchedule() {
			vm.nextScheduleClick = true;
			var num = vm.schedules.feasibleSolutions.length - 1;
			if (num != vm.scheduleSelectedIndex) vm.scheduleSelectedIndex++;
			var currentSchedule = vm.schedules.feasibleSolutions[vm.scheduleSelectedIndex].schedulingEntities;

			clearSchedule();
			plotCourses(vm.schedules.feasibleSolutions[vm.scheduleSelectedIndex]);
			createTempCart(currentSchedule);
		}

		function totalSelectedCredits() {
			var checkedCartCourses = (vm.cart.length) ? sharedService.getCheckedCartItems(vm.cart) : [],
				checkedEnrolledCourses = (vm.enrolled.length) ? sharedService.getCheckedCartItems(vm.enrolled) : [],
				checkedWaitlistCourses = (vm.waitlist.length) ? sharedService.getCheckedCartItems(vm.waitlist) : [],
				allChecked = checkedCartCourses.concat(checkedEnrolledCourses).concat(checkedWaitlistCourses);

			var sum = 0, min = 0, max = 0, hasRange = false;
			angular.forEach(allChecked, function(cartObjItem) {
				if ((cartObjItem.creditRange.indexOf('-') == -1)) {
					var credits = parseInt(cartObjItem.creditRange, 10);
					sum += credits;
					min += credits;
					max += credits;
				} else {
					hasRange = true;
					min += parseInt(cartObjItem.creditRange.split('')[0], 10);
					max += parseInt(cartObjItem.creditRange.split('')[2], 10);
				}
			});

			if (!hasRange) {
				vm.totalSelectedCredits = sum;
			} else {
				vm.totalSelectedCredits = min + '-' + max;
			}

		}

		function generate() {
			if (vm.hasPendingValidation) { return $timeout(function() { generate(); }, 1000); }

			var cartItemIds = sharedService.getCheckedCartIds(vm.cart),
				enrolledClassNumbers = sharedService.getCheckedEnrolledNumbers(vm.enrolled),
				waitListClassNumbers = sharedService.getCheckedEnrolledNumbers(vm.waitlist),
				schedulerClassNumbers = enrolledClassNumbers.concat(waitListClassNumbers);

			if (cartItemIds.length === 0 && schedulerClassNumbers.length === 0) {
				$mdDialog.show(
					$mdDialog.alert()
						.openFrom('#generate')
						.clickOutsideToClose(true)
						.title('No courses selected for schedule generation')
						.textContent('Please select at least one course for schedule generation.')
						.ariaLabel('Please select at least one course for schedule generation.')
						.ok('Ok')
				);
				return false;
			}

			clearSchedule();
			vm.paneNavClass = 'left';
			vm.paneScheduleClass = 'active';
			vm.scheduleSelectedIndex = 0;
			vm.loadTempCart = true;
			vm.isLoading = true;
			vm.hasConflicts = false;
			vm.noScheduleResults = false;
			vm.hasGeneratedResults = true;
			vm.expiredResults = false;
			vm.schedulerErrorMessage = false;
			vm.schedulerChangeWarning = false;
			// stateService.selectedCartIds = cartItemIds;

			// Ensure only checked courses are in the selected packages object. Prevents users from unchecking
			// a course with selected sections and have those packages get sent in.
			var filteredObj = {};
			cartItemIds.forEach(function(id) {
				if (vm.schedulerSelectedPackages.hasOwnProperty(id) && vm.schedulerSelectedPackages[id].length)
					filteredObj[id] = vm.schedulerSelectedPackages[id];
			});
			vm.schedulerSelectedPackages = filteredObj;

			var options = {
				'termCode': vm.selectedTerm,
				'cartItems': cartItemIds,
				'selectedPackages': vm.schedulerSelectedPackages,
				'enrolled': schedulerClassNumbers,
				'options': {
					'waitlistedPackages': vm.userPrefs.scheduler.options.waitlistedPackages,
					'specialGroupsPackages': vm.userPrefs.scheduler.options.specialGroupsPackages,
					'allowConflicts': vm.userPrefs.scheduler.options.allowConflicts
				}
			};
			if (vm.DEBUG)console.info('schedule post data', options);
			return scheduleService.schedule(vm.selectedTerm, options).then(function(data) {
				if (data) {
					pollScheduleStatus();
					vm.userPrefs.cachedDigest = courseDigestService.getDigest(vm.cartData);
					preferencesService.setPreferences(vm.userPrefs);
				} else {
					toastService.infoMessage('Unable to generate schedules due to server error');
					vm.isLoading = false;
					if (vm.DEBUG) console.error(data);
				}
			}).catch(function(error) { console.error(error); });
		}

		function pollValidationStatus() {
			return cartService.getEnrollmentStatus().then(function success(data) {
				if (data.status == 'Done') {
					if (vm.DEBUG) console.log('Polling tries:', vm.pollingSum);
					// Make sure cart is refreshed before stopping.
					vm.pollingSum = 0;
					$timeout(function() {
						return getCart().then(function() {
							vm.hasPendingValidation = false;
							vm.validationComplete = true;
							vm.disableLockButton = false;
							if (vm.loadTempCart && !vm.noScheduleResults) {
								if (vm.schedules.feasibleSolutions.length) {
									createTempCart(vm.schedules.feasibleSolutions[vm.scheduleSelectedIndex].schedulingEntities);
								} else {
									createTempCart(vm.schedules.bestSolution.schedulingEntities);
								}
							}
							if (vm.loadScheduleResults) {
								return getEnrolledCourses(vm.selectedTerm).then(function() {
									vm.loadScheduleResults = false;
									return pollScheduleStatus();
								});
							}
							return;
						}).catch(function(error) { console.error(error); });
					}, 2000);
				} else {
					if ($location.path() == '/scheduler') {
						vm.pollingSum++;
						vm.hasPendingValidation = true;

						if (vm.pollingSum < 300) {
							$timeout(function() {
								pollValidationStatus();
							}, 2000);
						} else {
							vm.hasPendingValidation = false;
							vm.disableLockButton = false;
							$mdDialog.show(
								$mdDialog.alert()
									.parent(angular.element(document.querySelector('#popupContainer')))
									.clickOutsideToClose(true)
									.title('Something went wrong!')
									.textContent('Unable to get cart courses. Please refresh your browser to try again.')
									.ariaLabel('unable to get courses')
									.ok('OK')
							);
						}
						getCart(true);

						if (vm.DEBUG)console.info('polling status:', data);
					}
				}
			}).catch(function(error) { console.error(error); });
		}

		function pollScheduleStatus() {
			// Fixes an issue where a validation is in progress and the schedule results are done.
			// The validation has to complete first so we know which courses are locked/unlocked
			if (vm.hasPendingValidation) {
				 return $timeout(function() { pollScheduleStatus(); }, 1000);
			}
			return scheduleService.status().then(function(data) {
				if (!data.error) {
					if ((data.status === 'New') || (data.status === 'InProgress')) {
						vm.isLoading = true;
						if ($location.path() == '/scheduler') {
							$timeout(function() {
								pollScheduleStatus();
							}, 2000);
						}
					} else if (data.status === 'Error') {
						vm.schedulerErrorMessage = data;

						if (data.reason == 'No Eligible Packages') {
							vm.schedulerErrorMessageEligibility = true;
						} else {
							vm.schedulerErrorMessageOther = true;
						}
						vm.isLoading = false;
						if (vm.DEBUG) console.info('status data', data);
					} else {
						scheduleService.getSchedules(vm.selectedTerm).then(function(data) {
							vm.isLoading = false;
							var currentSchedule;
							if (!data) {
								vm.noScheduleResults = true;
								if (hasGeneratedResultsForThisTerm() === false) {
									vm.newTerm = true;
								} else if (vm.hasGeneratedResults) {
									vm.expiredResults = false;
								} else {
									vm.expiredResults = true;
								}

								// Checks all courses if there are no schedule results
								vm.cart.forEach(function(cartObj) {
									cartObj.cartItemChecked = true;
								});
								vm.enrolled.forEach(function(cartObj) {
									cartObj.cartItemChecked = true;
								});
								return;
							}
							if (!data.error) {
								if (vm.DEBUG)console.info('schedule data', data);
								vm.schedules = data;
								vm.totalGenerated = data.feasibleSolutions.length;

								if (vm.schedules.feasibleSolutions.length) {
									// Ignore best solution since it's the first feasible
									plotCourses(vm.schedules.feasibleSolutions[0]);
									currentSchedule = vm.schedules.feasibleSolutions[vm.scheduleSelectedIndex].schedulingEntities;
									vm.schedulerSelectedPackages = vm.schedules.bestSolution.schedulingRequest.selectedPackages || {};
								} else {
									// Conflict solution only has 1 best result with no feasible
									vm.hasConflicts = true;
									plotCourses(vm.schedules.bestSolution);
									currentSchedule = vm.schedules.bestSolution.schedulingEntities;
								}

								createTempCart(currentSchedule);
								totalSelectedCredits();

								// Temp fix for cache sticking
								preferencesService.getPreferences().then(function() {
									vm.userPrefs = preferencesService.preferences;
								}).catch(function(error) { console.error(error); });

								var cachedDigest = vm.userPrefs.cachedDigest || null;
								var cartDigest = courseDigestService.getDigest(vm.cartData);

								if (vm.DEBUG) console.info('cached digest', cachedDigest);
								if (vm.DEBUG) console.info('cart digest', cartDigest);

								if (cachedDigest != null) {
									// Compare the cached digest (represents the cart state used to generate the
									// cached schedule data) against the current cart digest. If the digests are
									// different, indicate to the user that the cached schedule results do not
									// represent the schedules that could be generated from the current cart.
									if (courseDigestService.compareDigests(cachedDigest, cartDigest)) {
										if (vm.DEBUG) console.log('cached and cart digests match');
										vm.schedulerChangeWarning = false;
									} else {
										if (vm.DEBUG) console.log('different cached and cart digests');
										vm.schedulerChangeWarning = true;
										syncMessageDialog();
									}
								} else {
									if (vm.DEBUG) console.log('no cached digest');
									vm.schedulerChangeWarning = false;
								}
							} else {
								vm.noScheduleResults = true;
								console.error(data);
							}
						}).catch(function(error) { console.error(error); });
					}
				} else {
					toastService.infoMessage('Unable to get schedules due to server error');
					console.error(data);
				}
			}).catch(function(error) { console.error(error); });
		}

		function createTempCart(currentSchedule) {
			vm.cart.forEach(function(cartObj, i) {
				var classNumber = (cartObj.enrollmentClassNumber) ? cartObj.enrollmentClassNumber : cartObj.classNumber;
				var cartScheduleEntity = currentSchedule.filter(function(cartItem) {
						return (cartItem.courseId == cartObj.courseId && cartItem.subjectCode == cartObj.subject.subjectCode);
					})[0];

				if (cartScheduleEntity) {
					if (!cartObj.enrollmentClassNumber) {
						vm.cart[i] = {
							id: cartObj.id,
							catalogNumber: cartScheduleEntity.catalogNumber,
							subject: {	shortDescription: cartScheduleEntity.subjectShortDescription,
										description: cartScheduleEntity.subjectShortDescription,
										subjectCode: cartScheduleEntity.subjectCode },
							title: cartScheduleEntity.name,
							courseId: cartScheduleEntity.courseId,
							classMeetingsScheduler: cartScheduleEntity.selectedEnrollmentPackage.meetings,
							classNumber: cartScheduleEntity.selectedEnrollmentPackage.classNumber,
							creditRange: cartObj.creditRange,
							scheduleColor: cartObj.scheduleColor,
							enrollmentClassNumber: null,
							status: cartObj.status,
							validationResults: cartObj.validationResults,
							cartItemChecked: true,
							enrollmentOptions: cartScheduleEntity.selectedEnrollmentPackage.enrollmentOptions,
							packageEnrollmentStatus: cartScheduleEntity.selectedEnrollmentPackage.packageEnrollmentStatus
						};
					} else {
						vm.cart[i].cartItemChecked = true;
					}
				}
			});
			if (vm.DEBUG) console.log('temp cart', vm.cart);

			var enrolledOnSchedule = currentSchedule.filter(function(scheduleItem) {
				return scheduleItem.courseOrigin == 'ENROLLED';
			});
			if (enrolledOnSchedule.length) {
				enrolledOnSchedule.filter(function(scheduleItem) {
					return vm.enrolled.map(function(enrolledItem) {
						if (enrolledItem.courseId.indexOf(scheduleItem.courseId) > -1) {
							enrolledItem.cartItemChecked = true;
						}
					});
				});
			}

			var waitlistOnSchedule = currentSchedule.filter(function(scheduleItem) {
				return scheduleItem.courseOrigin == 'WAITLISTED';
			});
			if (waitlistOnSchedule.length) {
				waitlistOnSchedule.filter(function(scheduleItem) {
					return vm.waitlist.map(function(waitlistItem) {
						if (waitlistItem.courseId.indexOf(scheduleItem.courseId) > -1) {
							waitlistItem.cartItemChecked = true;
						}
					});
				});
			}
		}

		function plotCourses(data) {
			var calendarWeekendSetting = uiCalendarConfig.calendars.scheduler.fullCalendar('option', 'weekends');
			// Check for weekend courses
			var hasWeekends = data.schedulingEntities.some(function(course) {
				if (course.name !== 'SCHEDULE-BLOCKS') {
					return course.selectedEnrollmentPackage.meetings.some(function(meeting) {
						return meeting.daysOfWeek.some(function(day) { return (day == 'SATURDAY' || day == 'SUNDAY'); });
					});
				} else {
					return vm.allBlocks.some(function(block) {
						return ((block.storedDaysOfWeek.indexOf('S') > -1) || (block.storedDaysOfWeek.indexOf('N') > -1));
					});
				}
			});

			if (hasWeekends) {
				uiCalendarConfig.calendars.scheduler.fullCalendar('option', 'weekends', true);
			}

			// Edge case where user used to have a weekend calendar and hasn't refreshed their browser.
			// would show weekends on the grid by mistake
			if (calendarWeekendSetting && !hasWeekends) {
				uiCalendarConfig.calendars.scheduler.fullCalendar('option', 'weekends', false);
			}

			data.schedulingEntities.forEach(function(course) {
				if (course.name !== 'SCHEDULE-BLOCKS') {
					course.selectedEnrollmentPackage.meetings.forEach(function(meeting, j) {
						var cartObj = {}, enrolledObj = {}, cartIndex = 0, eventData = {};
						if (course.courseOrigin != 'ENROLLED') {
							cartObj = vm.cart.filter(function(cartObj) { return cartObj.courseId == meeting.courseId; })[0];
							if (cartObj) {
								cartIndex = vm.cart.map(function(x) { return x.courseId; }).indexOf(cartObj.courseId);
							} else {
								return;
							}
							eventData = {
								daysOfWeek: meeting.daysOfWeek,
								section: meeting.sectionType + ' ' + meeting.sectionNumber,
								instructors: meeting.instructors,
								eventColor: cartObj.scheduleColor,
								courseOrigin: (course.courseOrigin) ? course.courseOrigin : false,
								startTime: meeting.startTime,
								endTime: meeting.endTime,
								cartIndex: cartIndex,
								seatsStatus: course.selectedEnrollmentPackage.packageEnrollmentStatus.status
							};
						} else {
							enrolledObj = vm.enrolledData.filter(function(enrolledObj) { return enrolledObj.courseId == meeting.courseId; })[0];
							vm.enrolled.forEach(function(course) {
								if (enrolledObj.courseId == course.courseId) {
									course.cartItemChecked = true;
								}
							});
							vm.waitlist.forEach(function(course) {
								if (enrolledObj.courseId == course.courseId) {
									course.cartItemChecked = true;
								}
							});
							eventData = {
								daysOfWeek: meeting.daysOfWeek,
								section: meeting.sectionType + ' ' + meeting.sectionNumber,
								instructors: meeting.instructors,
								eventColor: enrolledObj.details.scheduleColor,
								courseOrigin: (course.courseOrigin) ? course.courseOrigin : false,
								startTime: meeting.startTime,
								endTime: meeting.endTime
							};
						}

						angular.forEach(eventData.daysOfWeek, function(day) {
							switch (day) {
								case 'SUNDAY':
									addCourseToSchedule(course, eventData, moment(currentDateString).add(0, 'day'), j);
									break;
								case 'MONDAY':
									addCourseToSchedule(course, eventData, moment(currentDateString).add(1, 'day'), j);
									break;
								case 'TUESDAY':
									addCourseToSchedule(course, eventData, moment(currentDateString).add(2, 'day'), j);
									break;
								case 'WEDNESDAY':
									addCourseToSchedule(course, eventData, moment(currentDateString).add(3, 'day'), j);
									break;
								case 'THURSDAY':
									addCourseToSchedule(course, eventData, moment(currentDateString).add(4, 'day'), j);
									break;
								case 'FRIDAY':
									addCourseToSchedule(course, eventData, moment(currentDateString).add(5, 'day'), j);
									break;
								case 'SATURDAY':
									addCourseToSchedule(course, eventData, moment(currentDateString).add(6, 'day'), j);
									break;
							}
						});
					});
				} else {
					course.scheduleBlocks.forEach(function(block) {
						var eventData = {
							storedDaysOfWeek: block.storedDaysOfWeek,
							startTime: block.startTime,
							endTime: block.endTime,
							name: block.name
						};

						angular.forEach(eventData.storedDaysOfWeek, function(day) {
							switch (day) {
								case 'N':
									addBlockToSchedule(eventData, moment(currentDateString).add(0, 'day'));
									break;
								case 'M':
									addBlockToSchedule(eventData, moment(currentDateString).add(1, 'day'));
									break;
								case 'T':
									addBlockToSchedule(eventData, moment(currentDateString).add(2, 'day'));
									break;
								case 'W':
									addBlockToSchedule(eventData, moment(currentDateString).add(3, 'day'));
									break;
								case 'R':
									addBlockToSchedule(eventData, moment(currentDateString).add(4, 'day'));
									break;
								case 'F':
									addBlockToSchedule(eventData, moment(currentDateString).add(5, 'day'));
									break;
								case 'S':
									addBlockToSchedule(eventData, moment(currentDateString).add(6, 'day'));
									break;
							}
						});
					});
				}
			});

			if (vm.DEBUG) console.info('events', uiCalendarConfig.calendars.scheduler.fullCalendar('clientEvents'));
		}

		function addBlockToSchedule(eventData, calendarDate) {
			var d = calendarDate.date(),
				m = calendarDate.month(),
				y = calendarDate.year(),
				courseStartTime = { hours: moment(eventData.startTime).format('HH'), mins: moment(eventData.startTime).format('mm') },
				courseEndTime = { hours: moment(eventData.endTime).format('HH'), mins: moment(eventData.endTime).format('mm') },
				firstHour = uiCalendarConfig.calendars.scheduler.fullCalendar('option', 'minTime');

			// Modify the start time of the calendar to avoid extra scrolling
			if (moment(eventData.startTime).isBefore('1970-01-01 ' + firstHour)) {
				uiCalendarConfig.calendars.scheduler.fullCalendar('option', 'minTime', moment(eventData.startTime).format('HH:00'));
			}

			uiCalendarConfig.calendars.scheduler.fullCalendar('renderEvent', {
				title: eventData.name,
				instructors: [],
				start: moment({ year:y, month:m, day:d, hour:courseStartTime.hours, minutes:courseStartTime.mins }).toDate(),
				end: moment({ year:y, month:m, day:d, hour:courseEndTime.hours, minutes:courseEndTime.mins }).toDate(),
				backgroundColor: '#333'
			});
		}

		function addCourseToSchedule(course, eventData, calendarDate, j) {
			var d = calendarDate.date(),
				m = calendarDate.month(),
				y = calendarDate.year(),
				courseStartTime = { hours: moment(eventData.startTime).format('HH'), mins: moment(eventData.startTime).format('mm') },
				courseEndTime = { hours: moment(eventData.endTime).format('HH'), mins: moment(eventData.endTime).format('mm') },
				status = (eventData.courseOrigin == 'ROADMAP') ? vm.cart[eventData.cartIndex].status : false,
				firstHour = uiCalendarConfig.calendars.scheduler.fullCalendar('option', 'minTime');

			// Modify the start time of the calendar to avoid extra scrolling
			if (moment(eventData.startTime).isBefore('1970-01-01 ' + firstHour)) {
				uiCalendarConfig.calendars.scheduler.fullCalendar('option', 'minTime', moment(eventData.startTime).format('HH:00'));
			}
			uiCalendarConfig.calendars.scheduler.fullCalendar('renderEvent', {
				title: course.subjectShortDescription + ' ' + course.catalogNumber,
				courseOrigin: course.courseOrigin,
				subjectCode: course.subjectCode,
				creditRange: course.selectedEnrollmentPackage.creditRange,
				enrollmentOptions: course.selectedEnrollmentPackage.enrollmentOptions,
				courseId: course.courseId,
				meetings: course.selectedEnrollmentPackage.meetings,
				classNumber: course.selectedEnrollmentPackage.classNumber,
				section: course.selectedEnrollmentPackage.meetings[j].sectionType + ' ' + course.selectedEnrollmentPackage.meetings[j].sectionNumber,
				instructors: course.selectedEnrollmentPackage.meetings[j].instructors,
				room: course.selectedEnrollmentPackage.meetings[j].room,
				buildingName: course.selectedEnrollmentPackage.meetings[j].buildingName,
				start: moment({ year:y, month:m, day:d, hour:courseStartTime.hours, minutes:courseStartTime.mins }).format(),
				end: moment({ year:y, month:m, day:d, hour:courseEndTime.hours, minutes:courseEndTime.mins }).format(),
				backgroundColor: eventData.eventColor,
				cartIndex: eventData.cartIndex,
				longDesc: course.name,
				validationStatus: status,
				seatsStatus: eventData.seatsStatus
			});
		}

		function scheduleSectionClickFromList(result, $event) {
			var scheduledEvents = uiCalendarConfig.calendars.scheduler.fullCalendar('clientEvents') || [];

			var clickedCourse = scheduledEvents.filter(function(course) {
				return course.courseId == result.courseId;
			})[0];

			if (clickedCourse) { scheduleSectionClick(clickedCourse, $event); }
		}

		function scheduleSectionClick(eventData, jsEvent, view) {
			vm.sidebarActive = false;
			vm.selectedClass = { eventData: eventData, jsEvent: jsEvent, view: view };
			var xPos = (jsEvent.originalEvent.screenX > 1800) ? $mdPanel.xPosition.OFFSET_START : $mdPanel.xPosition.OFFSET_END;

			var position = $mdPanel.newPanelPosition()
				.relativeTo(jsEvent.target)
				.addPanelPosition(xPos, $mdPanel.yPosition.ALIGN_TOPS);

			var config = {
				attachTo: angular.element(document.body),
				controller: DialogCtrl,
				controllerAs: 'vm',
				locals: {
					eventData: eventData,
					cart: vm.cart
				},
				templateUrl: 'components/scheduled-course-event-dialog/scheduled-course-event-dialog.html',
				panelClass: 'course-event-dialog',
				position: position,
				clickOutsideToClose: true,
				escapeToClose: true,
				hasBackdrop: true,
				focusOnOpen: true
			};

			// Do not show dialog for schedule blocks
			if (eventData.courseId) {
				$mdPanel.open(config);
			}

			// Show block dialog when clicking a block on the grid
			if (eventData.backgroundColor == '#333') {
				vm.scheduleBlocksDialog();
			}

			/* @ngInject */
			function DialogCtrl(mdPanelRef, $scope) {
				$scope.closeDialog = function() {
					mdPanelRef.close();
				};

				$scope.showSections = function(section, cartIndex) {
					mdPanelRef.close();
					vm.toggleSidenavSection(section, cartIndex);
				};

				$scope.setSection = function($event, unlock) {
					mdPanelRef.close();
					vm.selectedEnrollmentOptions = $scope.vm.selectedClassEnrollmentOptions;
					vm.setSection($event, unlock);
				};
			}
			if (vm.DEBUG) console.log('clicked class', vm.selectedClass);
		}

		function setSection($event, unlock) {
			var confirm;
			if (!unlock) {
				confirm = $mdDialog.confirm()
					.title('Lock this section?')
					.textContent('Locking this option will update your schedule choices')
					.ariaLabel('Locking this option will update your schedule choices')
					.targetEvent($event)
					.ok('Ok')
					.cancel('Cancel');
			} else {
				confirm = $mdDialog.confirm()
					.title('Unlock this section?')
					.textContent('Unlocking this option will update your schedule choices')
					.ariaLabel('Unlocking this option will update your schedule choices')
					.targetEvent($event)
					.ok('Ok')
					.cancel('Cancel');
			}

			$mdDialog.show(confirm).then(function() {
				if (angular.isUndefined(vm.selectedClass)) {
					vm.selectedClass = { eventData: vm.schedulerCurrentCourse };
				}
				if ($mdSidenav('right').isOpen()) {
					$mdSidenav('right').toggle()
				}
				var termCode = vm.selectedTerm,
					subjectCode = vm.selectedClass.eventData.subjectCode || vm.selectedClass.eventData.subject.subjectCode,
					courseId = vm.selectedClass.eventData.courseId,
					classNumber = (!unlock) ? vm.selectedClass.eventData.classNumber : '',
					data = {};

				if (!unlock && vm.selectedClass.eventData.enrollmentOptions.relatedClassNumber) {
					data.relatedClassNumber1 = vm.selectedClass.eventData.enrollmentOptions.relatedClasses[0];
					if (vm.selectedClass.eventData.enrollmentOptions.relatedClasses.length == 2)
						data.relatedClassNumber2 = vm.selectedClass.eventData.enrollmentOptions.relatedClasses[1];
				}

				if (vm.DEBUG) console.log('set section data', termCode, subjectCode, courseId, classNumber, data);

				cartService.addToCart(termCode, subjectCode, courseId, classNumber, data)
					.then(function success(data) {
						if (!data.error) {
							pollValidationStatus();
							generate();
						} else {
							toastService.infoMessage('Cannot add to cart server error');
							console.error(data);
						}
					});

				if (!unlock) {
					toastService.infoMessage(vm.selectedClass.eventData.title + ' section selected.');
				} else {
					toastService.infoMessage(vm.selectedClass.eventData.title + ' section updated.');
				}
				$scope.$emit('cart-action', [vm.selectedTerm, 'add-to-cart']);
			}, function() {}).catch(function(error) { console.error(error); });
		}

		function schedulePreferences(event) {
			$mdDialog.show({
				parent: angular.element(document.body),
				targetEvent: event,
				templateUrl: 'components/schedule-preferences-dialog/schedule-preferences-dialog.html',
				clickOutsideToClose: false,
				multiple: true,
				scope: $scope,
				preserveScope: true,
				fullscreen: true,
				controller: PreferencesController
			});

			/* @ngInject */
			function PreferencesController($scope, $mdDialog, preferencesService) {
				vm.closeDialog = function() {
					vm.setFirstStep = true;
					$mdDialog.hide();
				};

				vm.saveSettings = function() {
					preferencesService.setPreferences(vm.userPrefs);
					vm.closeDialog();
					generate();
				};
			}
		}

		function scheduleBlocksDialog(event) {
			$mdDialog.show({
				parent: angular.element(document.body),
				targetEvent: event,
				templateUrl: 'components/schedule-blocks-dialog/schedule-blocks-dialog.html',
				clickOutsideToClose: false,
				multiple: true,
				scope: $scope,
				preserveScope: true,
				fullscreen: true,
				controller: BlocksController
			});

			/* @ngInject */
			function BlocksController($scope, $mdDialog, toastService) {
				vm.addBlock = addBlock;
				vm.deleteBlock = deleteBlock;
				vm.saveSettings = saveSettings;
				vm.resetSettings = resetSettings;

				vm.closeDialog = function() {
					vm.setFirstStep = true;
					$mdDialog.hide();
				};

				function addBlock() {
					var blockDaysChecked = Object.keys(vm.blockDays);

					if ((blockDaysChecked.length === 0) || (vm.blockLabel === undefined) || (vm.selectedEndItem === null) || (vm.selectedStartItem === null)) {
						vm.blockError = true;
					} else {
						vm.blockError = false;
						vm.dayTouched = false;
						// Start Block Time
						var startHour = moment.utc(vm.selectedStartItem.time, 'h:mmA').format('HH:mm');
						var formattedStartHour = moment.duration(startHour).asMilliseconds() + 21600000;

						// End Block Time
						var endHour = moment(vm.selectedEndItem.time, 'h:mmA').format('HH:mm');
						var formattedEndHour = moment.duration(endHour).asMilliseconds() + 21600000;

						var addBlocks = {
							'name': vm.blockLabel,
							'startTime': formattedStartHour,
							'endTime': formattedEndHour,
							'storedDaysOfWeek': blockDaysChecked.join(''),
							'termCode': vm.selectedTerm
						};

						scheduleService.addBlock(addBlocks).then(function(data) {
							if (!data.error) {
								getBlocksList(vm.selectedTerm);
							} else {
								toastService.infoMessage('Cannot add block due to server error');
							}
						}).catch(function(error) { console.error(error); });

						vm.blockName.$setUntouched();
						vm.blockLabel = undefined;
						vm.timeAutocomplete.$setUntouched();
						vm.selectedEndItem = null;
						vm.selectedStartItem = null;
						vm.blockDayCheck.$setUntouched();
						vm.blockDays = {};
					}
				}

				vm.someSelected = function(days) {
					if (!days) {
						return false;
					}
					return Object.keys(days).some(function(key) {
						return days[key];
					});
				};

				vm.doTouched = function() {
					vm.dayTouched = true;
					vm.blockDayCheck.anyDay.$setTouched();
				};

				function deleteBlock(block) {
					var blockIndex = vm.allBlocks.indexOf(block);

					scheduleService.deleteBlock(block.id).then(function success(data) {
						vm.allBlocks.splice(blockIndex, 1);
						if (data.error) {
							toastService.infoMessage('Cannot delete block due to server error');
						}
					}).catch(function(error) { console.error(error); });
				}

				function saveSettings() {
					vm.closeDialog();
					generate();
				}

				function resetSettings() {
					vm.events = [];
					vm.blockLabel = null;
					vm.selectedEndItem = null;
					vm.selectedStartItem = null;
					vm.blockDays = false;
					vm.waitlistedPackages = false;
					vm.specialGroupsPackages = false;
					vm.allowConflicts = false;
				}
			}
		}

		function printPage() {
			$window.print();
		}

		function updatePanePositionClasses() {
			if (vm.paneNavClass === 'left') {
				vm.paneScheduleClass = 'right';
				vm.paneNavClass = 'active';
			} else if (vm.paneNavClass === 'active') {
				vm.paneScheduleClass = 'active';
				vm.paneNavClass = 'left';
			}
		}
		/* Change View */
		function changeView() {
			uiCalendarConfig.calendars.scheduler.fullCalendar('changeView', vm.viewSwitch);
		}
		/* Change View */
		function renderCalendar(calendar) {
			$timeout(function() {
				if (uiCalendarConfig.calendars[calendar]) {
					uiCalendarConfig.calendars[calendar].fullCalendar('render');
				}
			});
		}

		function hasGeneratedResultsForThisTerm() {
			var terms = vm.userPrefs.termsWithGeneratedSchedules || [];
			return (terms.indexOf(vm.selectedTerm) > -1);
		}
	}

})();
