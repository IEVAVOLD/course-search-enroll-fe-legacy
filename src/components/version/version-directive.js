(function() {
'use strict';

angular.module('app.version', [])
	.directive('appVersion', ['VERSION', function(VERSION) {
		return {
			restrict: 'C',
			link: function(scope, elm) {
				if (elm[0].src) {
					elm[0].src = elm[0].src  + '?v=' + VERSION;
				}
			}
		};
	}]);
})();
