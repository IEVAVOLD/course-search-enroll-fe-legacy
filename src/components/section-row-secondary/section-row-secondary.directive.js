(function() {
	'use strict';
	/**
	@desc directive for displaying a child section (LAB,DIS etc.). Does not have an isolate scope.
	@example class="section-row-secondary"
	*/
	angular
		.module('app.core')
		.directive('sectionRowSecondary', sectionRowSecondary);

	function sectionRowSecondary() {
		return {
			templateUrl: 'components/section-row-secondary/section-row-secondary.html',
			restrict: 'C',
			transclude: true,
		};
	}
})();