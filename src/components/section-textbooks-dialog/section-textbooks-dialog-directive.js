(function() {
	'use strict';
	/**
	* @desc directive for loading a placeholder image if the /book endpoint does not return an image.
	*/
	angular
		.module('app.core')
		.directive('notFoundSrc', function() {
			return {
				link: function(scope, element, attrs) {
					element.bind('error', function() {
						if (attrs.src != attrs.notFoundSrc) {
							attrs.$set('src', attrs.notFoundSrc);
						}
					});
				}
			};
		});
})();


