(function() {
	'use strict';
	/**
	 @desc directive for displaying footer of the enrollment tools app
	 @example <enrollment-footer></enrollment-footer>
	 */
	angular
		.module('app.core')
		.directive('enrollmentFooter', enrollmentFooter);

	function enrollmentFooter() {
		return {
			templateUrl: '../components/footer/footer.html',
			restrict: 'E',
			controller: FooterController,
			controllerAs: 'vm'
		}
	}

	function FooterController() {
		var vm = this;

		vm.version = version();

		// TODO: Get a real version once a /version endpoint and service are available
		function version() {
			return '0.0.1';
		}
	}
})();