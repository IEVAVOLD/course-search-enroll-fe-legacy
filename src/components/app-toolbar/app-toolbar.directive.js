(function() {
	'use strict';
	/**
	@desc directive for displaying the top bar of the enrollment tools app
	@example <app-toolbar></app-toolbar>
	*/
	angular
		.module('app.core')
		.directive('appToolbar', appToolbar);

	function appToolbar() {
		return {
			templateUrl: 'components/app-toolbar/app-toolbar.html',
			restrict: 'E',
			controller: ToolbarController,
			controllerAs: 'vm'
		};
	}

	/* @ngInject */
	function ToolbarController($scope, $location, $window, $timeout, constants, cartService, searchService, profileService, $mdDialog) {
		var vm = this;

		// BINDABLE MEMBERS
		vm.term							= '';
		vm.cartCount					= 0;
		vm.cartErrorExists				= false;
		vm.DEBUG						= constants.DEBUG;
		vm.public						= window.config && window.config.public || constants.public;

		// EXPOSED METHODS
		vm.goToCart = goToCart;
		vm.logout = logout;
		vm.helpDialog = helpDialog;
		vm.getProfile = getProfile;

		activate();

		function activate() {
			if (!vm.public) {
				getProfile().then(function(){
					var customEvent = new CustomEvent('myuw-login', {
						bubbles: true, // optional
						detail: { // required always
							person: { // required for generic session display
								"firstName": vm.profile.firstName // required for full session display
							}
						}
					});
					document.dispatchEvent(customEvent)
				});
			} else {
				var customEventPublic = new CustomEvent('myuw-login', {
					bubbles: true, // optional
					detail: {}
				});
				document.dispatchEvent(customEventPublic);
			}
			return;
		}

		/////////////
		// GETTERS //
		/////////////

		function goToCart() {
			$location.path('/my-courses');
			$timeout(function() {
				angular.element('.tab-courses').trigger('click');
			}, 100);
		}

		function logout() {
			$window.location.href = vm.profile.customLogoutUrl || '/logout';
		}

		function getProfile() {
			return profileService.getProfile()
				.then(function success(data) {
					vm.profile = data;
					if (vm.DEBUG)console.info('profile:', data);
				});
		}

		function helpDialog(event) {
			$mdDialog.show({
				parent: angular.element(document.body),
				targetEvent: event,
				template: 
					'<md-dialog class="help-dialog">' +
						'<md-content class="md-padding">' +
							'<div class="md-toolbar-tools">' +
								'<h2>Need more help?</h2>' +
								'<span flex></span>' +
								'<md-button class="md-button" ng-click="vm.closeDialog()">' +
									'<md-icon class="material-icon">close</md-icon>' +
								'</md-button>' +
							'</div>' +
							'<span flex/></span>' +
							'<md-list>' +
								'<md-list-item><a href="/help" target="_self">Support</a></md-list-item>' +
								'<md-list-item><a href="https://kb.wisc.edu/helpdesk/search.php?q=&cat=8136" target="_blank">FAQs</a></md-list-item>' +
								'<md-list-item><a href="https://kb.wisc.edu/enrollment/page.php?id=86702" target="_blank">Recent Changes</a></md-list-item>' +
							'</md-list>' +
						'</md-content>' +
					'</md-dialog>',
				clickOutsideToClose: false,
				multiple: true,
				scope: $scope,
				preserveScope: true,
				controller: HelpController
			});

			/* @ngInject */
			function HelpController($scope, $mdDialog) {

				vm.closeDialog = function() {
					$mdDialog.hide();
				};
			}
		}

		//////////////////
		// SCOPE EVENTS //
		//////////////////
		// TODO: Add some error handling
		// $scope.$on('cart-action', function(event, data) {
			// if(vm.DEBUG)console.info('cart event detected: ' + data[1]);
			// getCartTotal(data[0]);
		// });

	}
})();
