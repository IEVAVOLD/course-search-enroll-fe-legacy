(function() {
	'use strict';

	angular
		.module('app.core')
		.filter('removeAmpersand', function() {
			return function(subject) {
				if (subject) {
					return subject.replace('&', '').toLowerCase();
				}
			};
		})
		.directive('slideIn', function() {
			return {
				restrict: 'A',
				link: function(scope, element) {
					element.addClass('animate-repeat');
				}
			};
		})
		.directive('searchResultsListItem', searchResultsListItem);

	function searchResultsListItem() {
		var directive = {
			templateUrl: 'components/search-results-list-item/search-results-list-item.html',
			restrict: 'EA',
			scope: {
				result: '=searchResultsListItem'
			}
		};
		return directive;
	}

})();
