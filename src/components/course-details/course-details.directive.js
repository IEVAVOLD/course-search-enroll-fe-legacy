(function() {
	'use strict';
	/**
	@desc directive for displaying course details, including instructor, meeting times, etc. This is used by
			both the search view and the my-courses view.
	@example <course-details course=""></course-details>
	*/
	angular
		.module('app.core')
		.directive('courseDetails', courseDetails)
		.filter('filterAsHtml', function($sce) {
			return function(val) {
				return $sce.trustAsHtml(val);
			};
		})
		.filter('termDescriptionShort', function() {
			return function(termId) {
				var termDescription = '';
				var splitId = termId.split('');
				var century = (19 + Number(splitId[0])).toString();
				var year = Number(splitId.splice(1, 2).join(''));
				var term = Number(splitId[1]);

				switch (term) {
					case 2:
						termDescription = 'Fall ' + century;
						year = year - 1;
						break;
					case 3:
						//Replacing winter with more standard display of Fall
						termDescription = 'Fall ' + century;
						year = year - 1;
						break;
					case 4:
						termDescription = 'Spring ' + century;
						break;
					case 6:
						termDescription = 'Summer ' + century;
						break;
				}
				if (year > 9) {
					termDescription += (year).toString();
				} else {
					termDescription += '0' + (year).toString();
				}
				return termDescription;
			};
		})
		.filter('parseMessageDataEnroll', function() {
			return function(messageData) {
				if (angular.isArray(messageData)) {
					var lastMessageIndex = messageData.length - 1;
					var message = (messageData[lastMessageIndex].enrollmentMessages !== null) ? $.parseJSON(messageData[lastMessageIndex].enrollmentMessages) : false;
					if (angular.isArray(message) && message.length) {
						return message[0].description;
					} else
						return 'Error retrieving message (A1)';
				}
				if (messageData === null) return 'Error retrieving message (A2)';
			};
		})
		.filter('parseMessageDataValidation', function() {
			return function(messageData) {
				if (messageData && angular.isArray(messageData)) {
					if (messageData.length && messageData[0].validationMessages !== '[]') {
						var messages = $.parseJSON(messageData[0].validationMessages);
						var combinedMessages = '';

						if ((messages !== null) && (messages.length > 1)) {
							angular.forEach(messages, function(message, i) {
								combinedMessages = '#' + (i + 1) + ' ' + message.description + '\n' + combinedMessages;
							});
							combinedMessages = 'You have multiple messages. ' + combinedMessages;
						} else {
							combinedMessages = (messages) ? messages[0].description : '';
						}
						return combinedMessages;
					}
				}
				if (messageData === null) return 'Error retrieving message (A3)';
			};
		});

	function courseDetails() {
		return {
			templateUrl: 'components/course-details/course-details.html',
			restrict: 'E',
			scope: {
				term: '='
			},
			controller: DetailsController,
			controllerAs: 'vm',
			bindToController: {
				course: '=',
				sections: '='
			}
		};
	}

	/* @ngInject */
	function DetailsController(constants, $scope, $timeout, formatService, cartService, $q, $mdDialog) {
		var vm = this;
		// BINDABLE MEMBERS
		vm.courseDataExists				= courseDataExists;
		vm.getCode						= getCode;
		vm.getCleanSubject				= getCleanSubject;
		vm.getBreadths					= getBreadths;
		vm.getLevels					= getLevels;
		vm.getGeneralEd					= getGeneralEd;
		vm.getDepartment				= getDepartment;
		vm.getNotes						= getNotes;
		vm.hasExamDate					= hasExamDate;
		vm.hasInstructorContent			= hasInstructorContent;
		vm.instructorContent			= instructorContent;

		vm.DEBUG						= constants.DEBUG;
		vm.public						= window.config && window.config.public || constants.public;

		/**
		 * Check to make sure the course object is not empty
		 * @returns {boolean} True if the object has data, false if it is empty
		 */
		function courseDataExists() {
			for (var property in vm.course) {
				if (vm.course.hasOwnProperty(property)) {
					return true;
				}
			}
			return false;
		}

		/**
		 * Get a string representation of the levels array
		 * @returns {string} The level(s) of the course
		 */
		function getLevels() {
			if (courseDataExists()) {
				var levels = [];
				if (vm.course.hasOwnProperty('levels')) {
					angular.forEach(vm.course.levels, function(level) {
						levels.push(level.description);
					});
				}
				return levels.toString();
			}
		}

		/**
		 * Get a string representation of the generalEd array
		 * @returns {string} The general education attribute(s) of the course
		 */
		function getGeneralEd() {
			if (courseDataExists()) {
				var genEd = [];
				if (vm.course.hasOwnProperty('generalEd')) {
					angular.forEach(vm.course.generalEd, function(ge) {
						genEd.push(ge.description);
					});
				}
				return genEd.toString();
			}
		}

		/**
		 * Get a string representation of the breadths array
		 * @returns {string} The breadth(s) of the course
		 */
		function getBreadths() {
			if (courseDataExists()) {
				var breadths = [];
				if (vm.course.hasOwnProperty('breadths')) {
					angular.forEach(vm.course.breadths, function(breadth) {
						breadths.push(breadth.description);
					});
				}
				return breadths.toString();
			}
		}


		/**
		 * Get a lower case version of the course's department
		 * @returns {string} The department
		 */
		function getDepartment() {
			if (courseDataExists()) {
				if (vm.course.subject.hasOwnProperty('formalDescription') && (vm.course.subject.formalDescription)) {
					return vm.course.subject.formalDescription.toLowerCase();
				}
			}
		}

		/**
		 * Get the instructor's note for the course
		 * @returns {*} Response object containing subject notes
		 */
		function getNotes() {
			if (courseDataExists()) {
				if (vm.course.subject.hasOwnProperty('footnotes')) {
					var footnotes = [];
					angular.forEach(vm.course.subject.footnotes, function(key) {
						var note = formatService.formatFootnotes(key);
						footnotes.push(note);
					});
					return footnotes;
				}
			}
		}

		/**
		 * Returns the first letter of the course description for use by the course-badge directive
		 * @returns {string}
		 */
		function getCode() {
			return vm.course.subject.shortDescription.substr(0,1);
		}

		/**
		 * Remove special characters from vm.subject and change it to lower case (necessary because
		 * vm.subject is used in HTML class attributes)
		 * @returns {String} The "cleaned" version of the subject
		 */
		function getCleanSubject() {
			var groupCode = vm.course.subject.schoolCollege.academicGroupCode;
			if (groupCode) {
				if (groupCode.indexOf('&') > -1) {
					groupCode = groupCode.replace('&', '').toLowerCase();
					return groupCode;
				} else {
					groupCode = groupCode.toLowerCase();
					return groupCode;
				}
			}
		}

		function hasClassMaterials() {
			var materialsDefined = false;
			var hasSections = (vm.sections[0] !== undefined) ? true : false;
			if (hasSections) {
				for (var i = hasSections - 1; i >= 0; i--) {
					materialsDefined = vm.sections[i].classMaterials.some(function(value) { return value.materialsDefined === true; });
					if (materialsDefined) {
						break;
					}
				}
			}
			return materialsDefined;
		}

		function hasExamDate(section) {
			return section.classMeetings.some(function(value) { return value.meetingType == 'EXAM'; });
		}

		function hasInstructorContent() {

			if (vm.course) {
				return vm.course.instructorProvidedContent;
			}

			if (vm.sections) {
				return vm.sections.some(function(section) { return section.instructorProvidedClassDetails; });
			}
		}

		function instructorContent(event) {
			var courseName = vm.course.subject.shortDescription + ' ' + vm.course.catalogNumber;

			$mdDialog.show({
				parent: angular.element(document.body),
				targetEvent: event,
				templateUrl: 'components/course-instructor-content-dialog/course-instructor-content-dialog.html',
				locals: {
					courseName: courseName,
					courseTitle: vm.course.title,
					course: vm.course
				},
				clickOutsideToClose: true,
				multiple: true,
				controller: InstructorController
			});

			function InstructorController($scope, $mdDialog, course, courseName, courseTitle) {
				$scope.courseName = courseName;
				$scope.course = course;
				$scope.courseTitle = courseTitle;

				$scope.closeDialog = function() {
					$mdDialog.hide();
				};
			}
		}
	}
})();
