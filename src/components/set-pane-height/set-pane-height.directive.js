(function() {
	'use strict';
	/**
	* directive for calculating the height of panes based on the device.
	* @example <set-pane-height></set-pane-height>
	*/
	angular
		.module('app.core')
		.directive('setPaneHeight', setPaneHeight);

	function setPaneHeight($window) {
		function setHeight(element) {
			// Get the height of the viewport and subtract the panel header
			var viewport = document.querySelectorAll('.app-view')[0];

			// Set the height of the panes (54 = pane header, 15 = spacing)
			element.height(viewport.offsetHeight - 54 - 15);
		}

		return function(scope, element) {
			setHeight(element);

			angular.element($window).bind('resize', function() {
				setHeight(element);
				scope.$digest();
			});
		};
	}

	angular
		.module('app.core')
		.directive('setPaneHeightResults', setPaneHeightResults);

	function setPaneHeightResults($window) {
		function setHeight(element) {
			// Get the height of the viewport and subtract the panel header
			var viewport = document.querySelectorAll('.app-view')[0];

			// Set the height of the panes (54 = pane header, 15 = spacing, 64 = sort dropdown)
			element.height(viewport.offsetHeight - 54 - 15 - 64);
		}

		return function(scope, element) {
			setHeight(element);

			angular.element($window).bind('resize', function() {
				setHeight(element);
				scope.$digest();
			});
		};
	}

	/**
	* directive for calculating the height of panes based on the device.
	* @example <set-pane-height></set-pane-height>
	*/
	angular
		.module('app.core')
		.directive('setPaneHeightScheduler', setPaneHeightScheduler);

	function setPaneHeightScheduler($window) {
		function setHeight(element) {
			// Get the height of the viewport and subtract the panel header
			var viewport = document.querySelectorAll('.app-view')[0];

			// Set the height of the panes (54 = pane header, 15 = spacing)
			element.height(viewport.offsetHeight - 54 - 15);
		}

		return function(scope, element) {
			setHeight(element);

			angular.element($window).bind('resize', function() {
				setHeight(element);
				scope.$digest();
			});
		};
	}

	/**
	* directive for calculating the height of panes based on the device.
	* @example <set-pane-height-scheduler-first></set-pane-height-scheduler-first>
	*/
	angular
		.module('app.core')
		.directive('setPaneHeightSchedulerFirst', setPaneHeightSchedulerFirst);

	function setPaneHeightSchedulerFirst($window) {
		function setHeight(element) {
			// Get the height of the viewport and subtract the panel header
			var viewport = document.querySelectorAll('.app-view')[0];

			// Set the height of the panes (54 = pane header, 54 = controls header, 15 = spacing, 48 = use schedule button)
			element.height(viewport.offsetHeight - 54 - 54 - 15 - 48);
		}

		return function(scope, element) {
			setHeight(element);

			angular.element($window).bind('resize', function() {
				setHeight(element);
				scope.$digest();
			});
		};
	}

	/**
	* directive for calculating the height of panes based on the device.
	* Sets the height of the cart pane to take allow room for the multi select toolbar.
	* @example <set-pane-height></set-pane-height>
	*/
	angular
		.module('app.core')
		.directive('setPaneHeightCart', setPaneHeightCart);

	function setPaneHeightCart($window) {
		function setHeight(element) {
			// Get the height of the viewport and subtract the panel header
			var viewport = document.querySelectorAll('.app-view')[0];

			// Set the height of the panes (15 = spacing)
			element.height(viewport.offsetHeight - 15);
		}
		return function(scope, element) {
			setHeight(element);

			angular.element($window).bind('resize', function() {
				setHeight(element);
				scope.$digest();
			});
		};
	}

})();
