(function() {
	'use strict';
	/**
	@desc directive for displaying enrollment options for sections
	@example class="section-enrollment-options"
	*/
	angular
		.module('app.core')
		.directive('stringToNumber', function() {
			return {
				require: 'ngModel',
				link: function(scope, element, attrs, ngModel) {
					ngModel.$parsers.push(function(value) {
					return '' + value;
					});
					ngModel.$formatters.push(function(value) {
					return parseFloat(value);
					});
				}
			};
		})
		.filter('unique', function unique(){
			return function(arry){
					Array.prototype.getUnique = function(){
					var u = {}, a = [];
					for(var i = 0, l = this.length; i < l; ++i){
						if(u.hasOwnProperty(this[i])) {
							continue;
						}
						a.push(this[i]);
						u[this[i]] = 1;
					}
					return a;
				};
				if(arry === undefined || arry.length === 0){
					return '';
				}
				else {
					return arry.getUnique(); 
				}

				};
		})
		.directive('sectionEnrollmentOptions', function sectionEnrollmentOptions() {
			return {
				templateUrl: 'components/section-enrollment-options/section-enrollment-options.html',
				restrict: 'C',
				scope: true,
				link: function(scope, element, attr, constants) {
					scope.package = scope.$parent.$eval(attr.package);
					scope.public = window.config && window.config.public || constants.public;
				},
				controller: function(constants) {
					var vm = this;

					vm.addToCart = addToCart;
					vm.public = window.config && window.config.public || constants.public;

					function addToCart() {
						console.log('works');
					}
				}
			};
		});

})();