'use strict';

describe('search controller', function() {

	beforeEach(module('app.core'));
  	beforeEach(module('app.search'));

  	describe('SearchController Unit Tests:', function(){

		////////////////////
		//    CRITICAL    //
		////////////////////
		it('should have searchController', inject(function($controller) {
			//spec body
			var searchController = $controller('SearchController');
			expect(searchController).toBeDefined();
		}));

		it('should have empty arrays all bindable arrays upon initialization', inject(function($controller) {
			var searchController = $controller('SearchController');

			// ensure all bindable arrays are empty
			expect(searchController.subjects.length).toBe(0);
			expect(searchController.terms.length).toBe(0);
			expect(searchController.sessions.length).toBe(0);
			expect(searchController.searchResults.length).toBe(0);
		}));

		it('should have object to capture changes to the model', inject(function($controller) {
			var searchController = $controller('SearchController');
			var model = searchController.formData;

			// ensure formData model object is defined
			expect(model).toBeDefined();

			// query, subjectCode, selectedTerm, and session should be equal to empty string
			expect(model.query).toBe('');
			expect(model.subjectCode).toBe('');
			expect(model.selectedTerm).toBe('');
			expect(model.session).toBe('');

			// credits should be an object with lowerBound and upperBound both equal to empty string
			expect(model.credits.lowerBound).toBe('');
			expect(model.credits.upperBound).toBe('');

			// ethnicStudies, onlineOnly, gradCourseWork should be equal to false
			expect(model.ethnicStudies).toBeFalsy();
			expect(model.onlineOnly).toBeFalsy();
			expect(model.gradCourseWork).toBeFalsy();

		}));

		it('should have searchService with all necessary methods', inject(['searchService', function(searchService) {
			// ensure all methods are available
			expect(searchService.getSubjects).toBeDefined();
			expect(searchService.getTerms).toBeDefined();
			expect(searchService.getSessionsByTerm).toBeDefined();
			expect(searchService.searchWithFilters).toBeDefined();
		}]));

		it('should have formatService with all necessary methods', inject(['formatService', function(formatService) {
			// ensure all methods are available
			expect(formatService.formatSubject).toBeDefined();
			expect(formatService.alphabetizeObjects).toBeDefined();
		}]));

		it('should have properly-initialized filterService with all necessary methods', inject(['filterService', function(filterService) {
			// ensure that the filterList is empty
			var filters = [];
			for (var key in filterService.filterList) {
				if (filterService.filterList.hasOwnProperty(key)) {
					filters.push(filterService.filterList[key]);
				}
			}
			expect(filters.length).toBe(0);

			// ensure all methods are available
			expect(filterService.buildFilterList).toBeDefined();
			expect(filterService.addFilter).toBeDefined();
			expect(filterService.removeFilter).toBeDefined();
			expect(filterService.clearFilters).toBeDefined();
			expect(filterService.addSimpleString).toBeDefined();
			expect(filterService.addSimpleArray).toBeDefined();
			expect(filterService.addSpecialGroups).toBeDefined();
			expect(filterService.addOpenSeats).toBeDefined();
			expect(filterService.addMinCredits).toBeDefined();
			expect(filterService.addMaxCredits).toBeDefined();
			expect(filterService.addSession).toBeDefined();
			expect(filterService.addDayTime).toBeDefined();
		}]));

		it('should add a filter to the filterList when a filter is created', inject(['filterService', function(filterService) {
			// create a filter
			filterService.addSimpleString('TEST-STRING-VALUE', 100, 'TEST-STRING-NAME');
			filterService.addSimpleArray('TEST-ARRAY-VALUE', [1, 2, 3, 4], 'TEST-ARRAY-NAME');

			// trigger buildFilterList
			var filters = filterService.buildFilterList();

			// ensure filters exists
			expect(filters.length).toBe(2);

			// ensure they are the filters we added
			expect(filters[0].term['TEST-STRING-VALUE']).toBe(100);
			expect(filters[1].terms['TEST-ARRAY-VALUE'].length).toBe(4);
		}]));

		////////////////////////
		//    NICE-TO-HAVE    //
		////////////////////////

		it('should format search results to make them more readable', inject(function($controller) {
			var searchController = $controller('SearchController');
			var mockSearchResults = [
				{
					"courseId": "t3st4bl3",
					"subject": {
						"shortDescription": "TEST",
						"schoolCollege": {
							"academicGroupCode": "TEST"
						}
					},
					"catalogNumber": "322",
					"title": "Test Results"
				}
			];

			// trigger formatting function
			var formattedSearchResults = searchController.formatSearchResults(mockSearchResults);

			// ensure results are formatted as expected
			expect(formattedSearchResults.length).toBe(1);
			expect(formattedSearchResults[0].title).toBe('Test Results');
			expect(formattedSearchResults[0].shortDescription).toBe('TEST 322');
			expect(formattedSearchResults[0].courseId).toBe('t3st4bl3');
			expect(formattedSearchResults[0].code).toBe('T');
			expect(formattedSearchResults[0].subject).toBe('TEST');


		}));

		it('should format subject descriptions and list them in alphabetical order', inject(['formatService', function(formatService) {
			var mockSubjects = [
				{'formalDescription': 'SC - TEST SCIENCE'},
				{'formalDescription': 'A TEST OF THE WORDS TO LEAVE LOWERCASE'},
				{'formalDescription': 'INTRODUCTION TO TESTING'}
			];
			var formattedSubjects = [];

			// trigger the format function on each subject
			for(var i in mockSubjects) {
				var subject = {};
				subject['formalDescription'] = formatService.formatSubject(mockSubjects[i].formalDescription);
				formattedSubjects.push(subject);
			}

			// trigger the alphabetize function on the formatted subjects
			formattedSubjects = formatService.alphabetizeObjects(formattedSubjects, 'formalDescription');

			// ensure subjects were formatted and re-ordered as expected
			expect(formattedSubjects[0].formalDescription).toBe('A Test of the Words to Leave Lowercase');
			expect(formattedSubjects[1].formalDescription).toBe('Introduction to Testing');
			expect(formattedSubjects[2].formalDescription).toBe('SC - Test Science');
		}]));

		it('should build filters and execute a search upon triggering the search function', function() {});
		// Note: The above will require using $httpBackend to mock-up a response, and may not be worth the time.
  	});
});