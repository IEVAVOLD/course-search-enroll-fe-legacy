angular
	.module('app.search')
	.directive('paneAnimation', paneAnimation);

	function paneAnimation($animate) {
	'use strict';
		return {
			scope: {
				'paneAnimation': '=',
				'afterShow': '&',
				'afterHide': '&'
			},
			link: function(scope, element) {
				scope.$watch('paneAnimation', function(show, oldShow) {
					if (show) {
						$animate.removeClass(element, 'ng-hide').then(scope.afterShow);
					}
					if (!show) {
						$animate.addClass(element, 'ng-hide').then(scope.afterHide);
					}
				});
			}
		};
	}
