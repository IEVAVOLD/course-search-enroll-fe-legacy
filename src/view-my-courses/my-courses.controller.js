(function() {
	'use strict';

	angular
		.module('app.courses')
		.controller('CoursesController', CoursesController);

	/* @ngInject */
	function CoursesController(constants, VERSION, stateService, scheduleService, searchService, cartService, sharedService, preferencesService, courseDigestService,
								toastService, $mdToast, $scope, $timeout, $mdDialog, $interval, $filter, $location, enrollmentService, $mdSidenav, $mdMedia, $q) {
		var vm = this;

		vm.DEBUG				= constants.DEBUG;
		vm.VERSION				= VERSION;
		vm.userPrefs			= preferencesService.preferences;
		vm.initialization		= true;

		//////////////////////
		// BINDABLE MEMBERS //
		//////////////////////
		vm.terms				= [];
		vm.cart					= [];			// Cart data after getCourse() is called. used for the cart list
		vm.cartData				= [];			// Original cart data used for enrollment
		vm.enrolled				= false;
		vm.enrolledData			= [];
		vm.gettingEnrolled		= false;
		vm.gettingWaitlist		= false;
		vm.savedLater			= [];
		vm.savedLaterData		= [];
		vm.waitlist				= false;
		vm.dropped				= false;
		vm.hasPackages			= false;
		vm.selectedIndex		= undefined;
		vm.showSearchPane		= false;
		vm.showCoursePane		= false;
		vm.pollingSum			= 0;
		vm.paneNavClass			= 'active';
		vm.paneSearchClass		= 'right';
		vm.paneSchedulerClass	= 'right';
		vm.paneCourseClass		= 'right';
		vm.searchPaneContent	= '';
		vm.checkSections		= [];
		vm.courseSections		= [];
		vm.timerInterval		= '';
		vm.activeTermIndex		= 0;
		vm.hackFix				= [];
		vm.checkedCartItems		= [];
		vm.appliedFilters		= [];
		vm.onMycourses			= true;
		vm.hideSections			= true;
		vm.selectedSectionFilters		= '';
		vm.totalCoursePackages			= 0;
		vm.filteredTotalCoursePackages	= 0;
		vm.enrollmentEventTimestamp		= undefined;

		/////////////////////
		// EXPOSED METHODS //
		/////////////////////
		vm.getCart						= getCart;
		vm.getCourseData				= getCourseData;
		vm.getCourseStatusMessage		= getCourseStatusMessage;
		vm.addToSavedLater				= addToSavedLater;
		vm.addToCart					= addToCart;
		vm.addSavedLaterToCart			= addSavedLaterToCart;
		vm.removeFromCart				= removeFromCart;
		vm.drop							= drop;
		vm.swap							= swap;
		vm.getFavorites					= getFavorites;
		vm.removeFromSavedLater			= removeFromSavedLater;
		vm.enroll						= enroll;
		vm.getEnrolledCourses			= getEnrolledCourses;
		vm.getTermDescription			= getTermDescription;
		vm.myCoursesClickEvent			= myCoursesClickEvent;
		vm.updatePanePositionClasses	= updatePanePositionClasses;
		vm.createScheduleForCourses		= createScheduleForCourses;
		vm.afterSearchPaneClosed		= afterSearchPaneClosed;
		vm.afterSearchPaneOpen			= afterSearchPaneOpen;
		vm.afterCoursePaneOpen			= afterCoursePaneOpen;
		vm.afterSectionPaneClosed		= afterSectionPaneClosed;
		vm.confirmCartAction			= confirmCartAction;
		vm.enrollSingleCourse			= enrollSingleCourse;
		vm.sumCredits					= sumCredits;
		vm.updateCartChecked			= updateCartChecked;
		vm.getCourseSections			= getCourseSections;
		vm.clearCartSection				= clearCartSection;
		vm.updateSectionFilters			= updateSectionFilters;
		vm.updateAppliedFilters			= updateAppliedFilters;
		vm.enrollDialog					= enrollDialog;
		vm.toggleSidenavSection			= toggleSidenavSection;
		vm.getEnrollmentEventDate		= getEnrollmentEventDate;
		vm.getSessions					= getSessions;
		// shared functions
		vm.hasEnrollmentOptions			= hasEnrollmentOptions;

		// Initialize the view
		activate();

		function activate() {
			if (Object.keys(vm.userPrefs).length === 0) {
				// Get user preferences
				preferencesService.getPreferences().then(function() {
					vm.userPrefs = preferencesService.preferences;
				}).catch(function(error) { console.error(error); });
			}

			getTerms().then(function() {
				if ((localStorage.selectedTerm) && (vm.terms.length)) {
					if (localStorage.selectedTerm === 'undefined') {
						localStorage.setItem('selectedTerm', vm.terms[0].termCode);
					}
					var selectedTerm = localStorage.selectedTerm.toString();
					vm.terms.forEach(function(termObj, i) {
						if (termObj.termCode === selectedTerm) {
							vm.activeTermIndex = i;
							vm.selectedTerm = selectedTerm;
						}
					});
				} else {
					localStorage.setItem('selectedTerm', vm.terms[0].termCode);
					vm.selectedTerm = vm.terms[0].termCode;
				}
				vm.gettingEnrolled = true;
                getSessions();
                getEnrolledCourses(vm.selectedTerm);
				pollValidationStatus();
			});

			$scope.$watch('vm.selectedTerm', function(newTerm, oldTerm) {
				if ((oldTerm !== undefined) && (newTerm != oldTerm)) {
					vm.selectedTerm = newTerm;
					localStorage.setItem('selectedTerm', newTerm);
					pollValidationStatus();
					vm.gettingEnrolled = true;
					getEnrolledCourses(vm.selectedTerm);
					vm.showCoursePane = false;
					getEnrollmentEventDate();
				}
			}, true);

			getEnrollmentEventDate();

			return initializeViewByDeviceWidth();
		}

		function getEnrollmentEventDate() {
			return enrollmentService.getEnrollmentEventDate()
				.then(function success(data) {

					var enrollDate = [];
					if (data.enrollmentImpacts) {
						enrollDate = data.enrollmentImpacts.registrationAppointments.filter(function(appt) {
							return appt.termCode == vm.selectedTerm;
						});
					}

					vm.enrollmentEventTimestamp = enrollDate.length ? enrollDate[0].registrationDateTime : false;
					if (vm.DEBUG)console.info('studentInfoData:', data);
				}).catch(function(error) {
					console.log('error obj:', error);
				});
		}


		/**
		 * If the screen is wider than 960px, show both the first and second panes on initialization (shows the cart by default)
		 */
		function initializeViewByDeviceWidth() {
			// If device width is greater than 959px, show cart
			var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
			if (width > 959) {
				vm.myCoursesClickEvent('Cart');
			}
		}

		/////////////
		// GETTERS //
		/////////////
		/**
		 * Get the searchable terms
		 *
		 * @returns {*} Objects containing information about terms (a.k.a. semesters)
		 */
		function getTerms() {
			return searchService.getTerms()
				.then(function success(data) {
					var terms = [];

					// Add the rest of the terms
					for (var i in data) {
						var term = {
							longDescription: getTermDescription(data[i].termCode, true),
							termCode: data[i].termCode
						};
						terms.push(term);
					}
					vm.terms = terms;
					if (vm.DEBUG)console.info('terms:', terms);
					return vm.terms;
				});
		}

		/**
		* Takes termCode id and determine a short or long description of the code i.e. fall 2016 or fall 2016-2017
		* termCode logic is a follows:
		*    First digit is century, counting from 20 = 1, second and third are year, fourth is ‘semester'
		*    1172 = (1 (century), 17 (2016-2017 academic year), 2 (fall)
		*    Last digit: 2 (fall), 3 (winter), 4 (spring), 6 (summer)
		* All terms are displayed with the year before to account for fall 2016 in the above example
		* This is true for all terms except summer.
		*
		* @param {String} id - for the termCode
		* @param {bool} isShortDescription - Boolean flag for when a term description with a single year is desired
		* @returns {String} - string Either the short or long term description
		*/
		function getTermDescription(id, isShortDescription) {
			if (id) {
				var termDescription = '';
				var splitId = id.split('');
				var century = (19 + Number(splitId[0])).toString();
				var year = Number(splitId.splice(1, 2).join(''));
				var term = Number(splitId[1]);

				switch (term) {
					case 2:
						termDescription = 'Fall ' + century;
						if (isShortDescription) termDescription += (year - 1).toString();
						break;
					case 3:
						//Replacing winter with more standard display of Fall
						termDescription = 'Fall ' + century;
						if (isShortDescription) termDescription += (year - 1).toString();
						break;
					case 4:
						termDescription = 'Spring ' + century;
						if (isShortDescription) termDescription += (year).toString();
						break;
					case 6:
						termDescription = 'Summer ' + century;
						if (isShortDescription) termDescription += (year).toString();
						break;
				}
				if (!isShortDescription) {
					termDescription += (term === 6) ? year.toString() : (year - 1).toString() + '-20' + year.toString();
				}
				return termDescription;
			}
		}
        /**
         * Get the searchable sessions (no results unless a term is selected)
         *
         * @returns {*} Objects containing information about sessions
         */
        function getSessions() {
            return searchService.getSessionsByTerm(vm.terms[vm.activeTermIndex].termCode)
                .then(function success(data) {
                    var sessions = [];
                    angular.forEach(data, function(key) {
                        if (key.hasOwnProperty('beginDate')) {
                            key.beginDate = moment.unix(key.beginDate / 1000).format('MMM D');
                        }
                        if (key.hasOwnProperty('endDate')) {
                            key.endDate = moment.unix(key.endDate / 1000).format('MMM D, YYYY');
                        }
                        sessions.push(key);
                    });
                    vm.sessions = sessions;
                    sessions.unshift({ beginDate: 'All', endDate: 'All', sessionCode: '', sessionDescription: 'All' });
                    return vm.sessions;
                });
        }
		function getCart(refreshState) {
			// TODO: Split this into 2 functions getCart() and refreshCart()
			return cartService.getCart(vm.selectedTerm).then(function success(data) {
				if (vm.DEBUG)console.info('cartData:', data);

				if (!refreshState) { $scope.$emit('cart-action', [vm.selectedTerm, 'get-cart']); }
				vm.cartData = data;

				if ((refreshState) && (vm.cartData.length == vm.cart.length)) {
					vm.cartData.map(function(cartDataItem) {
						for (var i = 0; i < vm.cart.length; i++) {
							cartDataItem.details.validationResults = cartDataItem.validationResults;
							cartDataItem.details.classMeetings = cartDataItem.classMeetings;
							cartDataItem.details.enrollmentResults = cartDataItem.enrollmentResults;
							cartDataItem.details.enrollmentClassNumber = cartDataItem.classNumber;
							cartDataItem.details.docId = cartDataItem.docId;
							cartDataItem.details.relatedClassNumber1 = cartDataItem.relatedClassNumber1;
							cartDataItem.details.relatedClassNumber2 = cartDataItem.relatedClassNumber2;
							cartDataItem.details.id = cartDataItem.id;
							cartDataItem.details.cartItemChecked = (stateService.selectedCartIds) ? stateService.selectedCartIds.indexOf(cartDataItem.id) > -1 : false;
							cartDataItem.details.credits = cartDataItem.credits;
							cartDataItem.details.classPermissionNumber = cartDataItem.classPermissionNumber;
							vm.getCourseStatusMessage(cartDataItem);

							if ((cartDataItem.classNumber !== null) && (vm.cart[i].enrollmentClassNumber == cartDataItem.classNumber)) {
								vm.cart[i] = angular.extend({}, cartDataItem.details);
							} else if ((vm.cart[i].courseId == cartDataItem.courseId) && (vm.cart[i].subject.subjectCode == cartDataItem.subjectCode)) {
								vm.cart[i] = angular.extend({}, cartDataItem.details);
							}
						}
					});
				} else {
					if ((vm.cart.length !== 0) && (vm.cartData.length != vm.cart.length)) {
						pulseCart();
					}
					// Unset any cart items, this is needed if a user selects a different cart term
					vm.cart = [];
					// The cart data includes the course details on the course.details property. This is different than the search controller
					for (var i = 0; i < vm.cartData.length; i++) {
						vm.cartData[i].details.validationResults = vm.cartData[i].validationResults;
						vm.cartData[i].details.classMeetings = vm.cartData[i].classMeetings;
						vm.cartData[i].details.enrollmentResults = vm.cartData[i].enrollmentResults;
						vm.cartData[i].details.enrollmentClassNumber = vm.cartData[i].classNumber;
						vm.cartData[i].details.docId = vm.cartData[i].docId;
						vm.cartData[i].details.relatedClassNumber1 = vm.cartData[i].relatedClassNumber1;
						vm.cartData[i].details.relatedClassNumber2 = vm.cartData[i].relatedClassNumber2;
						vm.cartData[i].details.id = vm.cartData[i].id;
						vm.cartData[i].details.cartItemChecked = (stateService.selectedCartIds) ? stateService.selectedCartIds.indexOf(vm.cartData[i].id) > -1 : false;
						vm.cartData[i].details.credits = vm.cartData[i].credits;
						vm.cartData[i].details.classPermissionNumber = vm.cartData[i].classPermissionNumber;
						if (vm.cartData[i].enrollmentOptions) { vm.cartData[i].details.enrollmentOptions = vm.cartData[i].enrollmentOptions; }
						if (vm.cartData[i].creditRange) { vm.cartData[i].details.creditRange = vm.cartData[i].creditRange; }
						vm.getCourseStatusMessage(vm.cartData[i]);

						vm.cart.push(vm.cartData[i].details);
					}

					getFavorites();

					// My courses multiselect uses the index of the cart item for various actions. This recreates the index array from ids.
					if ((stateService.selectedCartIds) && (stateService.selectedCartIds.length)) {
						stateService.selectedCartIds.forEach(function(cartId) {
							vm.cart.forEach(function(cartObj, i) { if (cartObj.id == cartId) vm.checkedCartItems.push(i); vm.toggleCartCheckedControls = true; });
						});
					}
					if (vm.DEBUG) console.log('refact cart', vm.cart);
				}
			});
		}

		function getFavorites() {
			return cartService.getFavorites().then(function success(data) {
				if (vm.DEBUG)console.info('savedLaterData:', data);

				vm.savedLaterData = data;
				vm.savedLater = [];

				angular.forEach(vm.savedLaterData, function(savedLaterCourse) {
					return cartService.getCourse('0000', savedLaterCourse.subjectCode, savedLaterCourse.courseId)
						.then(function success(courseData) {
							if (!courseData.error) {
								vm.savedLater.push(courseData);
							} else {
								return searchService.getSubjectsMap().then(function(data) {
									// If course wasn't found, course is no longer offered
									vm.subjectsMap = data;
									savedLaterCourse.subject = {
										shortDescription: vm.subjectsMap[savedLaterCourse.subjectCode],
										formalDescription: vm.subjectsMap[savedLaterCourse.subjectCode],
										subjectCode: savedLaterCourse.subjectCode
									};
									savedLaterCourse.status = { state: 'No longer offered',	icon: 'error', class: 'closed' };
									vm.savedLater.push(savedLaterCourse);
								}).catch(function(error) { console.log(error); });
							}
						}).catch(function(error) { console.error(error); });
				});
				return vm.savedLater;
			});
		};

		function removeFromSavedLater(index) {
			if (vm.checkedCartItems.length) {
				if (vm.checkedCartItems.length > 1) { vm.checkedCartItems.sort(function(a, b) { return b - a; }); }

				var confirm = $mdDialog.confirm()
					.title('Are you sure?')
					.textContent('Remove selected courses?')
					.ariaLabel('remove selected courses')
					.ok('Remove')
					.cancel('Cancel');

				$mdDialog.show(confirm).then(function() {
					angular.forEach(vm.checkedCartItems, function(cartItemIndex) {
						var selectedCourse = vm.savedLater[cartItemIndex],
							courseName = selectedCourse.subject.shortDescription + ' ' + selectedCourse.catalogNumber;

						cartService.removeFromFavorites(selectedCourse.subject.subjectCode, selectedCourse.courseId)
							.then(function success(data) {
								vm.showSectionPane = false;
								vm.showCoursePane = false;
							}).catch(function(error) { 
								if (error.error.status == 404) {
									toastService.infoMessage('Unable to remove ' + courseName + ' from saved for later due to error: ' + data.error.statusText);
								}
							});
					});
					clearCartSection();
					pollValidationStatus();
				}, function() {});
			} else {
				// Don't open course details if course is being removed
				if (index !== undefined) {
					var selectedCourse = vm.savedLater[index],
						courseName = selectedCourse.subject.shortDescription + ' ' + selectedCourse.catalogNumber;

					cartService.removeFromFavorites(selectedCourse.subject.subjectCode, selectedCourse.courseId)
						.then(function success(data) {
							vm.showSectionPane = false;
							vm.showCoursePane = false;

							if (data && data.error.status == 404) {
								toastService.infoMessage('Unable to remove course from saved for later due to error: ' + data.error.statusText);
							} else {
								vm.savedLater.splice(index, 1);
								toastService.infoMessage(courseName + ' removed from saved for later');
							}

						});
				}
				pollValidationStatus();
			}

		}

		function pollValidationStatus() {
			vm.hasPendingValidation = true;

			cartService.getEnrollmentStatus().then(function success(data) {
				if (data.status == 'Done') {
					if (vm.DEBUG) console.log('Polling tries:', vm.pollingSum);
					// Make sure cart is refreshed before stopping.
					vm.pollingSum = 0;
					$timeout(function() {
						getCart().then(function() {
							vm.hasPendingValidation = false;
							vm.disableLockButton = false;
							$scope.$broadcast('polling-done');
							// Updates error messages
							if (vm.searchPaneContent == 'Cart' && vm.showCoursePane) {
								vm.courseDetails = vm.cart[vm.selectedIndex];
							}
						}).catch(function(error) { console.log(error); });

					}, 2000);

					if (vm.hasPendingEnrollment) {
						return enrollmentService.lastEnrollment().then(function(data) {
							vm.enrollmentComplete = true;
							vm.hasPendingEnrollment = false;
							if (!angular.isArray(data)) {
								data = [];
								vm.enrollmentProcessError = true;
							}
							var lastEnrollmentData = data;
							vm.lastEnrollment = lastEnrollmentData.filter(function(course) { return course.enrollmentState == 'Completed'; });

							if (vm.lastEnrollment.length > 1) {
								vm.totalCreditsEnrolled = vm.lastEnrollment.reduce(function(a, b) { return a.credits + b.credits; });
							} else if (vm.lastEnrollment.length === 1) {
								vm.totalCreditsEnrolled = vm.lastEnrollment.credits;
							} else {
								vm.totalCreditsEnrolled = 0;
							}

							lastEnrollmentData.forEach(function(courseEnrolled) {
								vm.checkedToEnroll.forEach(function(courseToEnroll) {
									if (courseEnrolled.courseId == courseToEnroll.courseId && courseEnrolled.subjectCode == courseToEnroll.subject.subjectCode) {
										if ((courseEnrolled.enrollmentState === 'Completed')) {
											courseToEnroll.enrolled = true;
											courseToEnroll.status = false;
										} else {
											courseToEnroll.enrollmentResults = [courseEnrolled];
											courseToEnroll.status = false;
											courseToEnroll.enrolled = false;
										}
									}
								});
							});
							clearCartSection();
							getEnrolledCourses(vm.selectedTerm);
						}).catch(function(error) { console.error(error); });
					}
				} else {
					if ($location.path() == '/my-courses') {
						vm.pollingSum++;
						if (vm.pollingSum < 300) {
							$timeout(function() {
								pollValidationStatus();
							}, 1000);
						} else {
							vm.hasPendingValidation = false;
							vm.disableLockButton = false;
							$mdDialog.show(
								$mdDialog.alert()
									.parent(angular.element(document.querySelector('#popupContainer')))
									.clickOutsideToClose(true)
									.title('Something went wrong!')
									.textContent('Unable to get cart courses. Please refresh your browser to try again.')
									.ariaLabel('unable to get courses')
									.ok('OK')
							);
						}
						getCart(true);

						if (vm.DEBUG)console.info('polling status:', data);
					}
				}
			}).catch(function(error) { console.error(error); });
		}

		function pulseCart() {
			vm.cartTotalChanged = true;
			$timeout(function() {
				vm.cartTotalChanged = false;
			}, 2000);
		}
		function pulseEnroll() {
			vm.enrolledTotalChanged = true;
			$timeout(function() {
				vm.enrolledTotalChanged = false;
			}, 2000);
		}
		function pulseWaitlist() {
			vm.waitlistTotalChanged = true;
			$timeout(function() {
				vm.waitlistTotalChanged = false;
			}, 2000);
		}
		function pulseDropped() {
			vm.droppedTotalChanged = true;
			$timeout(function() {
				vm.droppedTotalChanged = false;
			}, 2000);
		}

		function getEnrolledCourses(termCode) {
			return cartService.getEnrolled(termCode).then(function success(data) {
				if (vm.DEBUG)console.info('cartEnrolledData:', data);

				vm.enrolledData = data;
				// Don't want the numbers to pulse the first time we load.
				var waitlistLength  = (typeof(vm.waitlist) == 'boolean') ? false : vm.waitlist.length;
				var enrolledLength = (typeof(vm.enrolled) == 'boolean') ? false : vm.enrolled.length;
				var droppedLength = (typeof(vm.dropped) == 'boolean') ? false : vm.dropped.length;

				vm.enrolled = []; vm.waitlist = []; vm.waitlistData = []; vm.dropped = [];

				// The cart data includes the course details on the course.details property. This is different than the search controller
				for (var i = vm.enrolledData.length - 1; i >= 0; i--) {
					vm.enrolledData[i].details.validationResults = vm.enrolledData[i].validationResults;
					vm.enrolledData[i].details.classMeetings = vm.enrolledData[i].classMeetings;
					vm.enrolledData[i].details.enrollmentResults = vm.enrolledData[i].enrollmentResults;
					vm.enrolledData[i].details.enrollmentClassNumber = vm.enrolledData[i].classNumber;
					vm.enrolledData[i].details.relatedClassNumber1 = vm.enrolledData[i].relatedClassNumber1;
					vm.enrolledData[i].details.classSection = vm.enrolledData[i].sections['0'].classSection;
					vm.enrolledData[i].details.credits = vm.enrolledData[i].sections['0'].numCreditsTaken;
					vm.enrolledData[i].details.creditRange = vm.enrolledData[i].creditRange;
					vm.enrolledData[i].details.sections = vm.enrolledData[i].sections;
					vm.enrolledData[i].details.honors = vm.enrolledData[i].honors;
					vm.enrolledData[i].details.studentEnrollmentStatus = vm.enrolledData[i].studentEnrollmentStatus;
					vm.enrolledData[i].details.footnotes = vm.enrolledData[i].footnotes;
					vm.enrolledData[i].details.instructionMode = vm.enrolledData[i].instructionMode;
					vm.enrolledData[i].details.classMaterials = vm.enrolledData[i].classMaterials;
					vm.enrolledData[i].details.addConsent = vm.enrolledData[i].addConsent;
					vm.enrolledData[i].details.comB = vm.enrolledData[i].comB;
					vm.enrolledData[i].details.enrollmentOptions = vm.enrolledData[i].enrollmentOptions;
					vm.enrolledData[i].details.dropConsent = vm.enrolledData[i].dropConsent;
					vm.enrolledData[i].details.instructorProvidedClassDetails = vm.enrolledData[i].instructorProvidedClassDetails;
					vm.enrolledData[i].details.enrollmentRequirementGroups = vm.enrolledData[i].enrollmentRequirementGroups;

					if (vm.enrolledData[i].studentEnrollmentStatus == 'E') {
						vm.enrolled.push(vm.enrolledData[i].details);
					} else if (vm.enrolledData[i].studentEnrollmentStatus == 'D') {
						vm.dropped.push(vm.enrolledData[i].details);
					} else {
						vm.waitlist.push(vm.enrolledData[i].details);
						vm.waitlistData.push(vm.enrolledData[i]);
					}
				}

				if ((enrolledLength) && (enrolledLength != vm.enrolled.length)) {
					pulseEnroll();
				}
				if ((waitlistLength) && (waitlistLength != vm.waitlist.length)) {
					pulseWaitlist();
				}
				if ((droppedLength) && (droppedLength != vm.dropped.length)) {
					pulseDropped();
				}
				vm.gettingEnrolled = false;
				vm.gettingWaitlist = false;
			}).catch(function(error) {
				console.log('error obj:', error);
			});
		}

		/*
		 * Get the details for a specific course to pass on to the course-details directive and get
		 * displayable data for the 'last taught' property of the course
		 * @param course The course of the selected course
		 */
		function getCourseData(course, cartIndex) {
			if (vm.searchPaneContent != 'Saved for later') {
				vm.selectedSection = false;
				vm.sectionMaxLength = [];
				getCourseDetails(course.courseId, course.subject.subjectCode, course.subject.termCode, course.enrollmentClassNumber);

				if (vm.coursePaneOpen && $mdMedia('xs') || vm.coursePaneOpen && $mdMedia('sm')) {
					vm.loadingSections = true;
					getCourseSections(course.courseId, course.subject.subjectCode, course.subject.termCode);
					$timeout(function() {vm.loadingSections = false; }, 500);
					if ($mdMedia('gt-md')) focus('.pane__course-details');
				}
			} else {
				getCourseDetails(course.courseId, course.subject.subjectCode, course.subject.termCode);
			}
			vm.appliedFilters = [];
		}

		function getCourseDetails(courseId, subjectCode, termCode, classNumber) {
			vm.showSchedulerPane = false;
			vm.showCoursePane = true;
			vm.updatePanePositionClasses();
			var i = 0;

			if (vm.searchPaneContent == 'Cart') {
				// Get the course that was selected
				for (i = vm.cart.length - 1; i >= 0; i--) {
					if (vm.cart[i].courseId == courseId && vm.cart[i].subject.subjectCode == subjectCode && vm.cart[i].subject.termCode == termCode) {
						vm.courseDetails = vm.cart[i];
						vm.selectedIndex = i;
						if (vm.DEBUG)console.info('courseDetails:', vm.courseDetails);
					}
				}
			}

			if (vm.searchPaneContent == 'Enrolled') {
				// Get the course that was selected
				for (i = vm.enrolled.length - 1; i >= 0; i--) {
					if (vm.enrolled[i].enrollmentClassNumber == classNumber) {
						vm.courseDetails = vm.enrolled[i];
						vm.selectedIndex = i;
						if (vm.DEBUG)console.info('courseDetails:', vm.courseDetails);
					}
				}
			}

			if (vm.searchPaneContent == 'Wait list') {
				// Get the course that was selected
				for (i = vm.waitlist.length - 1; i >= 0; i--) {
					if (vm.waitlist[i].enrollmentClassNumber == classNumber) {
						vm.courseDetails = vm.waitlist[i];
						vm.selectedIndex = i;
						if (vm.DEBUG)console.info('courseDetails:', vm.courseDetails);
					}
				}
			}

			if (vm.searchPaneContent == 'Dropped') {
				// Get the course that was selected
				for (i = vm.dropped.length - 1; i >= 0; i--) {
					if (vm.dropped[i].enrollmentClassNumber == classNumber) {
						vm.courseDetails = vm.dropped[i];
						vm.selectedIndex = i;
						if (vm.DEBUG)console.info('courseDetails:', vm.courseDetails);
					}
				}
			}

			if (vm.searchPaneContent == 'Saved for later') {
				// Get the course that was selected
				for (i = vm.savedLater.length - 1; i >= 0; i--) {
					if (vm.savedLater[i].courseId == courseId && vm.savedLater[i].subject.subjectCode == subjectCode) {
						vm.courseDetails = vm.savedLater[i];
						vm.selectedIndex = i;
						if (vm.DEBUG)console.info('courseDetails:', vm.courseDetails);
					}
				}
			}

		}

		function getCourseSections() {
			// Variables that need to get reset each time this function is called.
			// vm.hasPackages = false;
			vm.isSearchingCourse = true;
			vm.hasOptionalSection = false;
			if (vm.searchPaneContent == 'Cart') {
				return searchService.getPackages(vm.selectedTerm, vm.courseDetails.courseId, vm.courseDetails.subject.subjectCode)
					.then(function success(data) {

						/* We need to refactor the data to treat a lecture as a parent and any labs, discussions, etc. as children since this
						 * is how we are grouping the lectures in the UI. There is an edge case where there are no lectures but a package that
						 * consists of a lab and discussion. In this case, we want the lab to be the parent.
						 * The first part of this code is designed to find out what the parent properties are, then we create a packages property
						 * on the parent object.
						 */

						// If there is only 1 section (Lecture) in a package we assume there are no packages and hasPackages remains false.
						var allLectures = angular.copy(data.map(function(coursePackage) {
							var lecture = coursePackage.sections.filter(function(section) {
								// Set package values on the sections since we display the sections in the UI
								section.packageEnrollmentStatus = coursePackage.packageEnrollmentStatus;
								section.enrollmentRequirementGroups = coursePackage.enrollmentRequirementGroups;
								section.enrollmentOptions = coursePackage.enrollmentOptions;
								section.instructorProvidedClassDetails = coursePackage.instructorProvidedClassDetails;
								if (coursePackage.creditRange) {
									section.creditRange = coursePackage.creditRange;
								}
								if (!vm.hasPackages && coursePackage.sections.length > 1) {
									vm.hasPackages = true;
								}
								return true;
							});
							return lecture[0];
						}));

						// Set the name of the parent section
						if (allLectures[0]) {
							vm.parentPackageType = allLectures[0].type;
						}

						if (vm.hasPackages) {
							// Get the section numbers from allLectures array then filter out the dups
							var sectionNums = allLectures.map(function(e) { return e.sectionNumber; });
							sectionNums = sectionNums.filter(function(item, pos) { return sectionNums.indexOf(item) == pos; });

							// Reduce allLectures array to only 1 lecture per section number
							var lectures = sectionNums.map(function(section) {
								for (var i = allLectures.length - 1; i >= 0; i--) {
									if (allLectures[i].sectionNumber == section) {
										return allLectures[i];
									}
								}
							});

							var OptionalLecturePackages = [];
							// Search the 'data' for all packages and add a packages property to each lecture
							sectionNums.forEach(function(sectionNum) {
								var lectureWithPackages = [];
								for (var i = data.length - 1; i >= 0; i--) {
									for (var j = data[i].sections.length - 1; j >= 0; j--) {
										if ((data[i].sections[j].type == vm.parentPackageType) && (data[i].sections[j].sectionNumber == sectionNum)) {
											// Determine any checked section
											vm.cartData.map(function(cartDataItem) {
												if (data[i].enrollmentClassNumber.toString() === cartDataItem.classNumber) {
													// Packages always have the same length of classes until they don't, which means the lab for instance is optional
													// in this case we need to modify its display on the front end.
													if (data[i].sections.length > 1) {
														vm.sectionMaxLength = data[i].sections;
														// We have to check the right lecture with a related class number otherwise mutilple discussion get checked
														if ((cartDataItem.relatedClassNumber1 === null) && (!vm.hasOptionalSection)) {
															data[i].sections.map(function(section) { section.checked = true; });
															data[i].sections.checked = true;
															data[i].sections[j].instructorProvidedClassDetails = data[i].instructorProvidedClassDetails;
															// getEnrollmentOptions(data[i].sections);
															data[i].sections[j + 1].packageEnrollmentStatus = data[i].packageEnrollmentStatus;
															if (cartDataItem.classPermissionNumber !== null) {
																data[i].sections[j + 1].classPermissionNumber = cartDataItem.classPermissionNumber;
																data[i].sections[j + 1].hasPermissionNumber = true;
															}
															if (cartDataItem.hasOwnProperty('credits')) {
																data[i].sections[j + 1].credits = cartDataItem.credits;
															}
															if (cartDataItem.waitlist == 'Y') {
																data[i].sections[j + 1].waitlistChecked = true;
															}
															if (cartDataItem.honors == 'Y') {
																data[i].sections[j + 1].honorsChecked = true;
															}
															// var parent = angular.copy(foundSection.sections[0]);
															// foundSection.sections[numSections-1].showPackage = true;
															// foundSection.sections[numSections-1].packageEnrollmentStatus = foundSection.packageEnrollmentStatus;
															// foundSection.sections[numSections-1].creditRange = foundSection.creditRange;
															// parent.packages = [foundSection.sections];
															// vm.selectedSection = [Object.assign(parent, foundSection)];

															// var numSections = data[i].sections.length;
															// data[i].sections[0].checked = true;
															// var parent = data[i].sections[0];
															// parent.packages = [data[i].sections];
															// vm.selectedSection = [angular.copy(parent)];
															// vm.selectedSection[0].packages[0][numSections-1].showPackage = true;
															// console.log('selected section', vm.selectedSection);
														} else {
															// Most of the the time a related class number corresponses to a LAB section but sometimes it is a LEC section
															// this accounts for both cases
															if (data[i].sections.length != 1) {
																if (cartDataItem.relatedClassNumber1 === data[i].sections[1].classUniqueId.classNumber.toString() ||
																	cartDataItem.relatedClassNumber1 === data[i].sections[0].classUniqueId.classNumber.toString()) {
																	data[i].sections.map(function(section) { section.checked = true; });
																	data[i].sections.checked = true;
																	data[i].sections[j].instructorProvidedClassDetails = data[i].instructorProvidedClassDetails;
																	data[i].sections[j + 1].packageEnrollmentStatus = data[i].packageEnrollmentStatus;
																	if (cartDataItem.classPermissionNumber !== null) {
																		data[i].sections[j + 1].classPermissionNumber = cartDataItem.classPermissionNumber;
																		data[i].sections[j + 1].hasPermissionNumber = true;
																	}
																	if (cartDataItem.hasOwnProperty('credits')) {
																		data[i].sections[j + 1].credits = cartDataItem.credits;
																	}
																	if (cartDataItem.waitlist == 'Y') {
																		data[i].sections[j + 1].waitlistChecked = true;
																	}
																	if (cartDataItem.honors == 'Y') {
																		data[i].sections[j + 1].honorsChecked = true;
																	}
																	// var parent = angular.copy(data[i].sections[0]);
																	// vm.selectedSection = Object.assign(parent, { packages: [angular.copy(data[i].sections)]});
																}
															} else if (cartDataItem.relatedClassNumber1 === data[i].sections[0].classUniqueId.classNumber.toString()) {
																data[i].sections[j].checked = true;
																data[i].sections.checked = true;
																data[i].sections[j].instructorProvidedClassDetails = data[i].instructorProvidedClassDetails;
																if (cartDataItem.classPermissionNumber !== null) {
																	data[i].sections[j].classPermissionNumber = cartDataItem.classPermissionNumber;
																	data[i].sections[j].hasPermissionNumber = true;
																}
																if (cartDataItem.hasOwnProperty('credits')) {
																	data[i].sections[j].credits = cartDataItem.credits;
																}
																if (cartDataItem.waitlist == 'Y') {
																	data[i].sections[j].waitlistChecked = true;
																}
																if (cartDataItem.honors == 'Y') {
																	data[i].sections[j].honorsChecked = true;
																}
																// vm.selectedSection = { packages: [angular.copy(data[i].sections)] };
															}
														}
													} else if (cartDataItem.relatedClassNumber1 === null) {
														data[i].sections[j].checked = true;
														data[i].sections.checked = true;
														vm.hasOptionalSection = true;
														if (cartDataItem.classPermissionNumber !== null) {
															data[i].sections[j].classPermissionNumber = cartDataItem.classPermissionNumber;
															data[i].sections[j].hasPermissionNumber = true;
														}
														if (cartDataItem.hasOwnProperty('credits')) {
															data[i].sections[j].credits = cartDataItem.credits;
														}
														if (cartDataItem.waitlist == 'Y') {
															data[i].sections[j].waitlistChecked = true;
														}
														if (cartDataItem.honors == 'Y') {
															data[i].sections[j].honorsChecked = true;
														}
														// vm.selectedSection = { packages: [angular.copy(data[i].sections)] };
													}
												}
											});
											//Set the classNumber and enrollment status for the package used for enrollment on the parentPackageType
											data[i].sections[j].enrollmentClassNumber = data[i].enrollmentClassNumber;
											if (data[i].sections.length > 1) {
												data[i].sections[j + 1].packageEnrollmentStatus = data[i].packageEnrollmentStatus;
												vm.sectionMaxLength = data[i].sections;
												lectureWithPackages.push(data[i].sections);
											} else {
												data[i].sections[j].packageEnrollmentStatus = data[i].packageEnrollmentStatus;
												var tmpLecture = angular.copy(data[i].sections[0]);
												tmpLecture.packages = [data[i].sections];
												OptionalLecturePackages.push(tmpLecture);
											}
										}
										if (data[i].sections.length == 1) {
											// Packages always have the same length of classes until they don't, which means the lab for instance is optional
											// in this case we need to modify its display on the front end.
											vm.hasOptionalSection = true;
										}
									}
								}
								for (var k = lectures.length - 1; k >= 0; k--) {
									if (lectures[k].sectionNumber == sectionNum) {
										lectures[k].packages = angular.extend([], lectureWithPackages);
									}
								}
							});
							lectures = lectures.concat(OptionalLecturePackages);
							lectures.map(function(lecture) { return lecture.packages.map(function(pack) { if(pack.checked) lecture.checked=true; }); });
							vm.courseSections = lectures;
						} else {
							// Set the selected course in the UI
							for (var i = allLectures.length - 1; i >= 0; i--) {
								for (var j = vm.cartData.length - 1; j >= 0; j--) {
									if (allLectures[i].classUniqueId.classNumber.toString() === vm.cartData[j].classNumber) {
										allLectures[i].checked = true;
										// getEnrollmentOptions(allLectures[i]);
										if (vm.cartData[j].classPermissionNumber !== null) {
											allLectures[i].classPermissionNumber = vm.cartData[j].classPermissionNumber;
											allLectures[i].hasPermissionNumber = true;
										}
										if (vm.cartData[j].hasOwnProperty('credits')) {
											allLectures[i].credits = vm.cartData[j].credits;
										}
										if (vm.cartData[j].waitlist == 'Y') {
											allLectures[i].waitlistChecked = true;
										}
										if (vm.cartData[j].honors == 'Y') {
											allLectures[i].honorsChecked = true;
										}
										// vm.selectedSection = angular.copy([allLectures[i]]);
										break;
									}
								}
							}
							vm.sectionMaxLength = [allLectures[0]];
							vm.courseSections = allLectures;
						}

						vm.courseSectionsData = angular.copy(vm.courseSections);
						vm.selectedSectionFilters = vm.appliedFilters;
						// updateSectionFilters();

						vm.isSearchingCourse = false;
						vm.loadingSections = false;
						if (vm.DEBUG)console.info('courseSectionsData:', data);
						if (vm.DEBUG)console.info('Refactored courseSections:', vm.courseSections);

						return vm.courseSections;
					});
			} else {
				if ((vm.searchPaneContent == 'Enrolled') || (vm.searchPaneContent == 'Wait list') || (vm.searchPaneContent == 'Dropped')) {
					var activeCourse = vm.enrolled[vm.selectedIndex];
					if (vm.searchPaneContent == 'Wait list') {
						activeCourse = vm.waitlist[vm.selectedIndex];
					}
					if (vm.searchPaneContent == 'Dropped') {
						activeCourse = vm.dropped[vm.selectedIndex];
					}

					activeCourse.classMeetings = activeCourse.classMeetings.filter(function(meeting) { return meeting.meetingType != 'EXAM'; });

					activeCourse.sections.forEach(function(section, index) {
						section.classMeetings = (activeCourse.classMeetings.length) ? [ activeCourse.classMeetings[index] ] : '';
						section.type = (activeCourse.classMeetings.length) ? activeCourse.classMeetings[index].sectionType : section.classSSRComponent;
						section.sectionNumber = section.classSection;
						section.classUniqueId = { classNumber: section.classNumber };
						section.creditRange = activeCourse.credits;
						section.honors = activeCourse.honors;
						section.checked = true;
						section.enrolled = true;
					});

					vm.courseSections = activeCourse.sections;

					var deferred = $q.defer();
					deferred.resolve();
					return deferred.promise;
				}
				if (vm.searchPaneContent == 'Saved for later') {
					vm.courseSections = '';
				}
			}
		}

		/* Since validation messages are a combination of both invalid and valid we check to see if any of the validation results have a 'validationState' of valid.
		 * That means the overall status is valid if there exists at least one valid status. Same is true for 'New' or pending courses. All other states are considered
		 * invalid. We are not displaying any messages on the front end if a course is valid so we do not set that property.
		 * @param course The course of the selected course
		 */
		function getCourseStatusMessage(course) {
			var failedEnrollment = course.enrollmentResults.some(function(message) { return message.enrollmentState === 'Failed'; });

			if (failedEnrollment) {
				course.details.status = {
					'state': 'Enrollment Failed',
					'icon': 'error',
					'class': 'closed'
				};
				return;
			}

			var enrollmentPending = course.enrollmentResults.some(function(message) { return message.enrollmentState === 'New' || message.enrollmentState === 'Added'; });
			if (enrollmentPending) {
				course.details.status = {
					'state': 'Enrollment Pending',
					'icon': 'sync',
					'class': 'error'
				};
				return;
			}

			var pending = course.validationResults.some(function(message) { return message.validationState === 'New' || message.validationState === 'Added'; }),
				invalid;

			if (pending) {
				course.details.status = {
					'state': 'Checking eligibility',
					'icon': 'sync',
					'class': 'error spin'
				};
				return;
			}

			invalid = course.validationResults.some(function(message) { return message.validationState === 'Invalid'; });

			if (invalid) {
				course.details.status = {
					'state': 'Problems with course',
					'icon': 'error',
					'class': 'error'
				};
			}

		}

		function addToSavedLater() {
			var subjectCode = vm.courseDetails.subject.subjectCode,
				courseName = vm.courseDetails.subject.shortDescription + ' ' + vm.courseDetails.catalogNumber,
				courseId = vm.courseDetails.courseId;

			cartService.addToFavorites(subjectCode, courseId)
				.then(function success(data) {
					if (data && data.error.status == 500) {
						toastService.infoMessage(courseName + ' Cannot add to saved for later server error');
					} else {
						var toast;
						if (!vm.cart.length) {
							toast = $mdToast.simple()
								.textContent(courseName + ' saved for later')
								.action('Go to cart')
								.position('bottom center')
								.hideDelay(5000)
								.highlightAction(false);
						} else {
							toast = $mdToast.simple()
								.textContent(courseName + ' saved for later')
								.position('bottom center')
								.hideDelay(5000)
								.highlightAction(false);
						}

						$mdToast.show(toast).then(function(response) {
							if (response == 'ok') {
								// SINCE TAB CONTROLLER ONLY FIRES ON PAGE LOAD I HAVE TO MANUALLY FIRE THE EVENT TO ADD THE ACTIVE STATE. NEED TO REFACTOR.
								$location.path('/my-courses');
								$timeout(function() {
									angular.element('.tab-courses').trigger('click');
								}, 100);
							}
						});
						getFavorites();
					}
				});
		}

		function addSavedLaterToCart(event) {
			var coursesNotInTerm = [],
			promises = [],
			courseNames = [];
			vm.checkedCartItems.forEach(function(checkedCartItem, index) {
				var course = vm.savedLater[checkedCartItem],
				termCode = course.termCode,
				subjectCode = course.subject.subjectCode,
				courseId = course.courseId,
				courseName = course.subject.shortDescription + ' ' + course.catalogNumber,
				credits = (course.maximumCredits === course.minimumCredits) ? course.minimumCredits : 0,
				classNumber = '',
				hasCheckedSection = false,
				data = {};
				var numberOfCourses = vm.checkedCartItems.length;

				if (numberOfCourses) {
					courseNames.push(courseName);
				}
				if (vm.searchPaneContent === 'Saved for later') {
					termCode = vm.selectedTerm;
				}

				var promise = cartService.addToCart(termCode, subjectCode, courseId, classNumber, data)
					.then(function success(data) {
						if (data.error && data.error.status == 500) {
							coursesNotInTerm.push(courseName);
						} else {
							return cartService.removeFromFavorites(subjectCode, courseId)
							.then(function success(data) {
								if (data && data.error.status == 404) {
									toastService.infoMessage('Unable to remove course from saved for later due to error: ' + data.error.statusText);
								} else {
									vm.savedLater.splice(checkedCartItem, 1);
								}
							});
						}
					}).catch(function(error){ console.error(error)});
				promises.push(promise);
			});

			$q.all(promises).then(function(){
				var selectedTerm = vm.getTermDescription(vm.selectedTerm, true);
				if (coursesNotInTerm.length) {
					$mdDialog.show(
						$mdDialog.alert()
							.parent(angular.element(document.body))
							.clickOutsideToClose(false)
							.title('Notice')
							.textContent('Looks like the following courses aren\'t available in ' + selectedTerm + ': ' + coursesNotInTerm)
							.ariaLabel('course not available in term')
							.ok('Ok')
					);
				}
				clearCartSection();
			});

			$mdToast.show(
				$mdToast.simple()
					.textContent('Adding ' + courseNames + ' to the cart')
					.position('bottom center')
					.hideDelay(5000)
					.highlightAction(false)
			);
		}

		function addToCart() {
			clearCartSection();
			stateService.selectedCartIds = false;

			var termCode = vm.courseDetails.termCode,
				subjectCode = vm.courseDetails.subject.subjectCode,
				courseId = vm.courseDetails.courseId,
				courseName = vm.courseDetails.subject.shortDescription + ' ' + vm.courseDetails.catalogNumber,
				classNumber = '',
				hasCheckedSection = false,
				data = {};
				if (vm.searchPaneContent === 'Saved for later') {
					termCode = vm.selectedTerm;
				}
			// These properties are bound to the UI and the discussion for example gets checked.
			function setEnrollmentOptionsPostData(section) {
				if (section.classPermissionNumber) data.classPermissionNumber = section.classPermissionNumber;
				if (section.honorsChecked == true) data.honors = true;
				if (section.waitlistChecked == true) data.waitlist = true;
				data.credits = (section.creditRange.indexOf('-') > -1) ? section.credits : Number(section.creditRange);
			}

			// The lecture section of a package has the related class number property because we compare the lecture to the related class
			// number which is typically a lecture
			function setRelatedClassNumberPostData(section) {
				if (section.enrollmentOptions.relatedClassNumber) {
					data.relatedClassNumber1 = section.enrollmentOptions.relatedClasses[0];
					if (section.enrollmentOptions.relatedClasses.length == 2) data.relatedClassNumber2 = section.enrollmentOptions.relatedClasses[1];
				}
			}

			// Set the classNumber for enrollment only if they have selected a section
			if (vm.hasPackages) {
				vm.courseSections.filter(function(section) {
					for (var i = section.packages.length - 1; i >= 0; i--) {
						if (section.packages[i].checked === true) {
							hasCheckedSection = true;
							classNumber = section.packages[i][0].enrollmentClassNumber.toString();
							if (section.packages[i].length > 1) {
								setRelatedClassNumberPostData(section.packages[i][0]);
								setEnrollmentOptionsPostData(section.packages[i][1]);
							} else {
								setEnrollmentOptionsPostData(section.packages[i][0]);
								setRelatedClassNumberPostData(section.packages[i][0]);
							}
						}
					}
				});
			} else {
				if (vm.courseSections) {
					for (var i = vm.courseSections.length - 1; i >= 0; i--) {
						if (vm.courseSections[i].checked === true) {
							hasCheckedSection = true;
							classNumber = vm.courseSections[i].classUniqueId.classNumber.toString();
							setEnrollmentOptionsPostData(vm.courseSections[i]);
							break;
						}
					}
				}
			}

			if (!hasCheckedSection) {
				data.credits = 0;
			}
			if (vm.DEBUG)console.info('addToCart data:', termCode, subjectCode, courseId, classNumber, data);
			cartService.addToCart(termCode, subjectCode, courseId, classNumber, data)
				.then(function success(data) {
					if (data.error && data.error.status == 500) {
						toastService.infoMessage(courseName + ' Cannot add to cart server error');
					} else {
						$scope.$emit('cart-action', [vm.selectedTerm, 'add-to-cart']);
						var toast;
						if (!vm.searchPaneContent) {
							toast = $mdToast.simple()
									.textContent(courseName + ' Added to cart')
									.action('Go to cart')
									.position('bottom center')
									.hideDelay(5000)
									.highlightAction(false);
						} else {
							toast = $mdToast.simple()
									.textContent(courseName + ' Updating')
									.position('bottom center')
									.hideDelay(5000)
									.highlightAction(false);

							$mdToast.show(toast).then(function(response) {
								if (response == 'ok') {
									// SINCE TAB CONTROLLER ONLY FIRES ON PAGE LOAD I HAVE TO MANUALLY FIRE THE EVENT TO ADD THE ACTIVE STATE. NEED TO REFACTOR.
									$location.path('/my-courses');
									$timeout(function() {
										angular.element('.tab-courses').trigger('click');
									}, 100);
								}
							});

							pollValidationStatus();
						}
					}
				});
		}

		function confirmCartAction(event, action) {
			if (vm.selectedIndex !== undefined) {
				var selectedCourse = vm.cart[vm.selectedIndex];

				if (vm.searchPaneContent === 'Saved for later') {
					selectedCourse = vm.savedLater[vm.selectedIndex];
				}

				if (vm.searchPaneContent === 'Wait list') {
					selectedCourse = vm.waitlist[vm.selectedIndex];
				}

				if (vm.searchPaneContent === 'Enrolled') {
					selectedCourse = vm.enrolled[vm.selectedIndex];
				}

				if (vm.searchPaneContent === 'Dropped') {
					selectedCourse = vm.dropped[vm.selectedIndex];
				}

				var courseName = selectedCourse.subject.shortDescription + ' ' + selectedCourse.catalogNumber;

				$mdDialog.show({
					parent: angular.element(document.body),
					targetEvent: event,
					templateUrl: 'components/confirm-cart-action/confirm-cart-action.html',
					fullscreen: true,
					locals: {
						course: courseName,
						action: action
					},
					clickOutsideToClose: true,
					controller: ConfirmController
				});
			}

			function ConfirmController($scope, $mdDialog, course, action) {
				$scope.course = course;
				$scope.action = action;
				$scope.term = vm.getTermDescription(vm.selectedTerm, true);
				$scope.closeDialog = function() {
					$mdDialog.hide();
				};
				$scope.remove = function() {
					clearCartSection();
					removeFromCart();
					toggleSidenavSection();
					$mdDialog.hide();
				};
				$scope.enroll = function() {
					enrollSingleCourse();
					toggleSidenavSection();
					$mdDialog.hide();
				};
				$scope.drop = function() {
					clearCartSection();
					drop();
					toggleSidenavSection();
					$mdDialog.hide();
				};
				$scope.removeSavedLater = function() {
					clearCartSection();
					removeFromSavedLater(vm.selectedIndex);
					$mdDialog.hide();
				};
				$scope.addSavedLater = function() {
					addToSavedLater();
					if (vm.searchPaneContent != 'Dropped') { removeFromCart(); }
					$mdDialog.hide();
				};
				$scope.addToCart = function() {
					clearCartSection();
					addToCart();
					$mdDialog.hide();
				};
				$scope.updateCart = function() {
					addToCart();
					$mdDialog.hide();
				};
				$scope.addSavedLaterToCart = function() {
					addSavedLaterToCart();
					$mdDialog.hide();
				};
			}
		}

		function removeFromCart() {
			var numberOfCourses = vm.checkedCartItems.length,
				courseNames = [],
				confirm;

			if (numberOfCourses) {

				angular.forEach(vm.checkedCartItems, function(cartItemIndex) {
					courseNames.push(vm.cartData[cartItemIndex].details.subject.shortDescription + ' ' + vm.cartData[cartItemIndex].catalogNumber);
				});

				if (numberOfCourses > 1) {
					vm.checkedCartItems.sort(function(a, b) { return b - a; });

					confirm = $mdDialog.confirm()
						.title('Are you sure?')
						.textContent('Remove selected courses? ' + courseNames.toString())
						.ariaLabel('remove selected courses')
						.ok('Remove')
						.cancel('Cancel');
				} else {
					confirm = $mdDialog.confirm()
						.title('Are you sure?')
						.textContent('Remove ' + courseNames.toString() + ' from cart?')
						.ariaLabel('remove selected course')
						.ok('Remove')
						.cancel('Cancel');
				}

				$mdDialog.show(confirm).then(function() {
					angular.forEach(vm.checkedCartItems, function(cartItemIndex) {
						var selectedCourse = vm.cart[cartItemIndex];
						cartService.removeFromCart(selectedCourse.termCode, selectedCourse.subject.subjectCode, selectedCourse.courseId)
							.then(function() {
								vm.showSectionPane = false;
								vm.showCoursePane = false;
								getCart();
							});
					});
					toastService.infoMessage(courseNames.toString() + ' Removed from cart');
					clearCartSection();
				}, function() {});
			} else {
				if (vm.selectedIndex !== undefined) {
					var selectedCourse = vm.cart[vm.selectedIndex],
						courseName = selectedCourse.subject.shortDescription + ' ' + selectedCourse.catalogNumber;
					return cartService.removeFromCart(selectedCourse.termCode, selectedCourse.subject.subjectCode, selectedCourse.courseId)
						.then(function() {
							vm.showSectionPane = false;
							vm.showCoursePane = false;
							toastService.infoMessage(courseName + ' Removed from cart');
							getCart();
						});
				}
			}
		}

		function drop() {
			var selectedDrop = [],
				dropPostData = [],
				confirm,
				selectedCourse = '',
				dataSet = [],
				courseNames = {},
				courseNamesList = '';

			if (vm.checkedCartItems.length) {
				switch (vm.searchPaneContent) {
					case 'Enrolled':
						dataSet = vm.enrolled;
						break;
					case 'Wait list':
						dataSet = vm.waitlist;
						break;
				}

				angular.forEach(vm.checkedCartItems, function(cartItemIndex) {
					selectedCourse = dataSet[cartItemIndex];
					var classNumber = selectedCourse.enrollmentClassNumber;
					courseNames[classNumber.toString()] = selectedCourse.subject.shortDescription + ' ' + selectedCourse.catalogNumber;
					selectedDrop.push(selectedCourse);
				});

				if (Object.keys(courseNames).length === 1) {
					for (var key in courseNames) courseNamesList += courseNames[key];
				} else {
					courseNamesList = Object.keys(courseNames).map(function(i) {return courseNames[i];}).join(', ');
				}

				confirm = $mdDialog.confirm()
					.title('Drop selected course?')
					.textContent('Drop selected courses? ' + courseNamesList)
					.ariaLabel('Drop selected courses')
					.ok('Drop')
					.cancel('Cancel');

				$mdDialog.show(confirm).then(function() {
					vm.gettingEnrolled = true;
					clearCartSection();

					angular.forEach(selectedDrop, function(singleCourse) {
						dropPostData.push(singleCourse.enrollmentClassNumber);
					});

					return cartService.drop(selectedCourse.termCode, dropPostData)
						.then(function success(data) {
							var description = '';
							vm.showSectionPane = false;
							vm.showCoursePane = false;

							data.forEach(function(errorResponse) {
								if (errorResponse.messageSeverity != 'E') {
									toastService.infoMessage('Dropped ' + courseNames[errorResponse.instanceId]);
								} else {
									description += courseNames[errorResponse.instanceId] + ' ' + errorResponse.description + ' ';
								}
							});

							if (description !== '') {
								$mdDialog.show(
									$mdDialog.alert()
										.parent(angular.element(document.body))
										.clickOutsideToClose(false)
										.title('Drop failed')
										.textContent(description)
										.ariaLabel('drop failed')
										.ok('Ok')
								);
								vm.gettingEnrolled = false;
							} else {
								vm.showCoursePane = false;
								vm.showSectionPane = false;
								getEnrolledCourses(vm.selectedTerm);
							}

						}).catch(function(error) { console.log('error obj:', error);});
				}, function cancel() {
					return;
				});
			} else {

				if (vm.searchPaneContent === 'Wait list') {
					selectedCourse = vm.waitlist[vm.selectedIndex];
					vm.gettingWaitlist = true;
				}

				if (vm.searchPaneContent === 'Enrolled') {
					selectedCourse = vm.enrolled[vm.selectedIndex];
					vm.gettingEnrolled = true;
				}

				dropPostData.push(selectedCourse.enrollmentClassNumber);
				courseNames = selectedCourse.subject.shortDescription + ' ' + selectedCourse.catalogNumber;

				cartService.drop(selectedCourse.termCode, dropPostData)
					.then(function success(data) {
						if (data[0].messageSeverity != 'E') {
							vm.showCoursePane = false;
							vm.showSectionPane = false;
							toastService.infoMessage('Dropped ' + courseNames);
							getEnrolledCourses(vm.selectedTerm);
						} else {
							vm.gettingEnrolled = false;
							$mdDialog.show(
								$mdDialog.alert()
									.parent(angular.element(document.body))
									.clickOutsideToClose(false)
									.title('Drop failed')
									.textContent(data[0].description)
									.ariaLabel('drop failed')
									.ok('Ok')
							);
						}
					}).catch(function(error) { console.log('error obj:', error); });
			}
		}

		function swap() {
			$mdDialog.show({
				parent: angular.element(document.body),
				templateUrl: 'components/swap/swap.html',
				preserveScope: true,
				scope: $scope,
				fullscreen: true,
				clickOutsideToClose: true,
				controller: SwapController
			});

			function SwapController($mdDialog, toastService, cartService) {
				if ((vm.searchPaneContent === 'Enrolled') || (vm.searchPaneContent === 'Wait list')) {
					vm.swapDropCourseIndex = (!vm.checkedCartItems.length) ? vm.selectedIndex : vm.checkedCartItems[0];
				}
				if (vm.searchPaneContent === 'Cart') {
					vm.swapEnrollCourseIndex = (!vm.checkedCartItems.length) ? vm.selectedIndex : vm.checkedCartItems[0];
				}
				vm.hackFix = [{}];
				vm.swapComplete = false;
				vm.processingSwap = false;
				vm.swapCompleteError = false;
				vm.dataSet = (vm.searchPaneContent !== 'Wait list') ? vm.enrolled : vm.waitlist;

				vm.closeDialog = function() {
					vm.swapEnrollCourseIndex = -1;
					vm.swapDropCourseIndex = -1;
					vm.hackFix = [];
					$mdDialog.hide();
				};

				vm.swapHasEnrollOptions = function() {
					if (vm.cartData[vm.swapEnrollCourseIndex].waitlist == 'Y') {
						return true;
					} else if (vm.cartData[vm.swapEnrollCourseIndex].honors === 'Y') {
						return true;
					} else if (vm.cartData[vm.swapEnrollCourseIndex].creditMax != vm.cartData[vm.swapEnrollCourseIndex].creditMin) {
						return true;
					} else if (vm.cartData[vm.swapEnrollCourseIndex].classPermissionNumber) {
						return true;
					} else {
						return false;
					}

				};

				vm.processSwap = function() {
					var swapEnrollCourse = vm.cart[vm.swapEnrollCourseIndex];
					var swapDropCourse = vm.enrolled[vm.swapDropCourseIndex];

					if (vm.searchPaneContent === 'Wait list') {
						swapDropCourse = vm.waitlist[vm.swapDropCourseIndex];
					}

					if (swapEnrollCourse.enrollmentClassNumber !== null) {
						var data = { };
						vm.swapEnrollCourseName = swapEnrollCourse.subject.shortDescription + ' ' + swapEnrollCourse.catalogNumber;
						vm.swapDropCourseName = swapDropCourse.subject.shortDescription + ' ' + swapDropCourse.catalogNumber;
						vm.processingSwap = false;

						data.dropCourseId = swapDropCourse.courseId;
						data.dropPackageNumber = swapDropCourse.enrollmentClassNumber;

						if (swapEnrollCourse.credits !== 0) {
							vm.processingSwap = true;
							cartService.swap(vm.selectedTerm, swapEnrollCourse.courseId, swapEnrollCourse.enrollmentClassNumber, data)
								.then(function(data) {
									if (data[0].messageSeverity != 'E') {
										vm.showCoursePane = false;
										getEnrolledCourses(vm.selectedTerm);
										getCart();
										vm.processingSwap = false;
										vm.swapComplete = true;
										clearCartSection();
									} else {
										vm.hackFix = [];
										vm.swapCompleteError = true;
										vm.processingSwap = false;
										vm.errorMsg = data[0].description;
									}
								}).catch(function(err) { console.error(err); });
						} else {
							$mdDialog.show(
								$mdDialog.alert()
									.parent(angular.element(document.body))
									.clickOutsideToClose(false)
									.title('Credits needed')
									.textContent('You must select credits for ' + vm.swapEnrollCourseName + ' and try again.')
									.ariaLabel('you need credits selected')
									.ok('Ok')
							);
						}
					}
				};
			}
		}

		function enrollDialog($event, allCourses) {
			$mdDialog.show({
				parent: angular.element(document.body),
				targetEvent: $event,
				templateUrl: 'components/dialogs/enrollment-dialog.html',
				scope: $scope,
				preserveScope: true,
				fullscreen: true,
				controller: function() {
					vm.enrollmentComplete = false;
					vm.totalCreditsEnrolled = false;
					vm.checkedToEnroll = (!allCourses) ? sharedService.getCheckedCartItems(vm.cart) : angular.copy(vm.cart);

					if (vm.searchPaneContent == 'Wait list') {
						vm.checkedToEnroll = sharedService.getCheckedCartItems(vm.waitlist);
					}

					// loop and set enrollment options
					vm.checkedToEnroll.forEach(function(course) {
						vm.cartData.forEach(function(cartObj) {
							if (course.id == cartObj.id) {
								if (cartObj.waitlist == 'Y') course.waitlist = true;
								if (cartObj.honors == 'Y') course.honors = true;
								if (cartObj.classPermissionNumber) course.classPermissionNumber = cartObj.classPermissionNumber;
								if (cartObj.creditRange) course.creditRange = cartObj.creditRange;
							}
						});
					});

					var calcCredits = function() {
						var sum = 0, min = 0, max = 0, hasRange = false, enrolledCredits = vm.sumCredits(vm.enrolled);
						angular.forEach(vm.checkedToEnroll, function(cartObjItem) {
							if (cartObjItem.classNumber || cartObjItem.enrollmentClassNumber) {
								if (cartObjItem.credits != 0) {
									sum += Number(cartObjItem.credits);
									min += Number(cartObjItem.credits);
									max += Number(cartObjItem.credits);
									return;
								} else {
									if (cartObjItem.minimumCredits != cartObjItem.maximumCredits) {
										hasRange = true;
									}
								}
								min += cartObjItem.minimumCredits;
								max += cartObjItem.maximumCredits;
							} else {
								if (cartObjItem.maximumCredits === cartObjItem.minimumCredits) {
									sum += cartObjItem.maximumCredits;
								} else {
									hasRange = true;
								}
								min += cartObjItem.minimumCredits;
								max += cartObjItem.maximumCredits;
							}
						});

						if (!hasRange) {
							vm.enrolledCreditsSum = sum + enrolledCredits;
							return sum;
						} else {
							vm.enrolledCreditsSum = (min + enrolledCredits) + '-' + (max + enrolledCredits);
							return min + '-' + max;
						}
					};

					vm.creditsSum = calcCredits();

					vm.closeDialog = function() { $mdDialog.hide(); vm.enrollmentProcessError = false; };

					vm.createCreditRange = function(section) {
						function range(start, stop) {
							var a = [start], b = start;
							while (b < stop) { b += 1; a.push(b); }
							return a;
						}
						if (section.creditRange.indexOf('-') === -1) {
							return;
						} else {
							var min = Number(section.creditRange.split('-')[0]),
								max = Number(section.creditRange.split('-')[1]);

							return range(min, max);
						}
					};

					vm.processEnrollment = function() {
						vm.hasPendingEnrollment = true;
						vm.gettingEnrolled = true;
						var validToEnroll = [];

						// Collapse all courses before enrollment
						vm.checkedToEnroll.forEach(function(course) { course.show = false; });

						// checkedToEnroll is bound to the UI and we need to send the cart object in to the enrollment service.
						// this copies the needed properties to the cartData object for enrollment processing
						var checkedCartIds = (!allCourses) ? sharedService.getCheckedCartIds(vm.checkedToEnroll) : vm.cart.map(function(course) { return course.id; });

						if (vm.searchPaneContent != 'Wait list') {
							validToEnroll = vm.cartData.filter(function(cartDataCourse) {
								if (cartDataCourse.classNumber) {
									var foundId = checkedCartIds.filter(function(checkedCartId) { return cartDataCourse.id == checkedCartId; });

									if (cartDataCourse.id == foundId) {
										return vm.checkedToEnroll.map(function(courseChecked) {
											if (courseChecked.id == foundId) {
												cartDataCourse.waitlist = (courseChecked.waitlist) ? true : false;
												cartDataCourse.honors = (courseChecked.honors) ? true : false;
												cartDataCourse.credits = (courseChecked.credits) ? Number(courseChecked.credits) : cartDataCourse.credits;
												if (courseChecked.classPermissionNumber) cartDataCourse.classPermissionNumber = courseChecked.classPermissionNumber;
												return true;
											}
										});
									}
								}
							});
						} else {
							validToEnroll = vm.enrolledData.filter(function(waitlistDataCourse) {
								return vm.checkedToEnroll.filter(function(checkedCourse) {
									return waitlistDataCourse.enrollmentClassNumber == checkedCourse.enrollmentClassNumber;
								}).length != 0;
							});
						}

						if (validToEnroll.length) {
							if (vm.DEBUG) console.log('validToEnroll', validToEnroll);

							enrollmentService.enroll(validToEnroll, vm.selectedTerm)
								.then(function() {
									pollValidationStatus();
								}).catch(function(error) { console.log('error obj:', error); });
						} else {
							vm.hasPendingEnrollment = false;
							vm.gettingEnrolled = false;
							vm.lastEnrollment = [];
							vm.enrollmentComplete = true;
						}

					};
				}
			}, function() { });
		}

		function enrollSingleCourse() {
			clearCartSection();
			vm.checkedCartItems.push(vm.selectedIndex);
			vm.checkedToEnroll = sharedService.getCheckedCartItems(vm.cart);
			var single = true;
			enroll(single);
		}

		function enroll(single) {
			var selectedEnroll = [],
				confirm,
				zeroCreditCourse = [],
				dataSet = (vm.searchPaneContent == 'Cart') ? vm.cartData : vm.waitlistData,
				numberOfCourses = vm.checkedCartItems.length,
				courseNames = [];

			if (numberOfCourses) {
				angular.forEach(vm.checkedCartItems, function(cartItemIndex) {
					var course = dataSet[cartItemIndex];
					selectedEnroll.push(course);
					courseNames.push(course.details.subject.shortDescription + ' ' + course.catalogNumber);
					if (course.credits === 0 && course.details.enrollmentClassNumber !== null) zeroCreditCourse.push(course.details.subject.shortDescription + ' ' + course.catalogNumber);
				});

				if (numberOfCourses > 1) {
					confirm = $mdDialog.confirm()
						.title('Enroll in selected courses?')
						.textContent('Enroll in selected courses? ' + courseNames.toString())
						.ariaLabel('enroll selected courses')
						.ok('Enroll')
						.cancel('Cancel');
				} else {
					confirm = $mdDialog.confirm()
						.title('Enroll in selected course?')
						.textContent('Enroll in ' + courseNames.toString() + '?')
						.ariaLabel('enroll course')
						.ok('Enroll')
						.cancel('Cancel');
				}
			} else {
				// Enroll in all courses action
				angular.forEach(dataSet, function(cartItem) {
					if (cartItem.credits === 0 && cartItem.details.enrollmentClassNumber !== null) zeroCreditCourse.push(cartItem.details.subject.shortDescription + ' ' + cartItem.catalogNumber);
				});

				if (zeroCreditCourse.length) {
					confirm = $mdDialog.confirm()
						.title('Credits needed')
						.textContent(zeroCreditCourse.toString() +
						' has a credits value of 0. Please select credits on your selected section or if this is a 0 credit course you can continue with enrollment.')
						.ariaLabel('you need credits selected')
						.ok('Continue')
						.cancel('Cancel');
				} else {
					confirm = $mdDialog.confirm()
						.title('Are you sure?')
						.textContent('Enroll in all courses?')
						.ariaLabel('enroll in all')
						.ok('Ok')
						.cancel('Cancel');
				}
			}

			if (zeroCreditCourse.length) {
				confirm = $mdDialog.confirm()
					.title('Credits needed')
					.textContent(zeroCreditCourse.toString() +
					' has a credits value of 0. Please select credits on your selected section or if this is a 0 credit course you can continue with enrollment.')
					.ariaLabel('you need credits selected')
					.ok('Continue')
					.cancel('Cancel');
			}

			$mdDialog.show(confirm).then(function() {
				vm.showSectionPane = false;
				vm.showCoursePane = false;
				var cancelEnrollment = false;
				var enrollmentPostData = [];
				var enrollDataSet = (selectedEnroll.length) ? selectedEnroll : vm.cartData;

				angular.forEach(enrollDataSet, function(cartDataItem) {
					if (cartDataItem.details.enrollmentClassNumber !== null) {
						var data = { };

						if (cartDataItem.waitlist === 'Y') { data.waitlist = true; }
						if (cartDataItem.honors === 'Y') { data.honors = true; }
						if (cartDataItem.classPermissionNumber && cartDataItem.classPermissionNumber !== null) { data.classPermissionNumber = cartDataItem.classPermissionNumber; }
						if (cartDataItem.relatedClassNumber1 && cartDataItem.relatedClassNumber1 !== null) { data.relatedClassNumber = cartDataItem.relatedClassNumber1; }
						// Don't think this is needed but may catch edge case
						if (cartDataItem.relatedClassNumber2 && cartDataItem.relatedClassNumber2 !== null) {
							data.relatedClassNumber = cartDataItem.relatedClassNumber2; console.log('relatedClassNumber2 is true!');
						}

						data.credits = cartDataItem.credits;

						var tempObj = {
							courseId: cartDataItem.courseId,
							subjectCode: cartDataItem.details.subject.subjectCode,
							classNumber: cartDataItem.classNumber,
							options: data
						};

						enrollmentPostData.push(tempObj);
					} else {
						toastService.infoMessage(cartDataItem.details.subject.shortDescription + ' ' + cartDataItem.catalogNumber + ' Needs a section selected');
						cancelEnrollment = true;
					}
				});

				if (enrollmentPostData.length && !cancelEnrollment) {
					vm.hasPendingEnrollment = true;

					if (vm.DEBUG) console.log('Enrollment post data:', enrollmentPostData);
					// Note: there are mild differences in data from this endpoint and the cart data
					cartService.enroll(vm.selectedTerm, enrollmentPostData)
						.then(function() {
							pollValidationStatus();
						})
						.catch(function(error) {
							console.log('error obj:', error);
						});

					vm.gettingEnrolled = true;
					clearCartSection();
				}

			}, function() {
				// cancel function. we unset the selected course. This only happens if the click to enroll happens on the section pane.
				if (single) { clearCartSection(); }
			});
		}

		function sumCredits(cartObj) {
			if (cartObj.length == 0) return 0;

			var sum = 0, min = 0, max = 0, hasRange = false;

			angular.forEach(cartObj, function(cartObjItem) {
				if (cartObjItem.classNumber && cartObjItem.credits > 0) {
					min += cartObjItem.credits;
					max += cartObjItem.credits;
					sum += cartObjItem.credits;
				} else {
					if (cartObjItem.creditMin != cartObjItem.creditMax) {
						hasRange = true;
					}
					if (cartObjItem.credits == 0 && cartObjItem.classNumber == null) {
						sum += cartObjItem.creditMax;
					}
					min += cartObjItem.creditMin;
					max += cartObjItem.creditMax;
					sum += cartObjItem.credits;
				}
			});

			if (!hasRange) {
				return sum;
			} else {
				return min + '-' + max;
			}
		}

		function updateCartChecked(result, $index) {
			if (result.cartItemChecked === true) {
				vm.checkedCartItems.push($index);
			} else {
				var index = vm.checkedCartItems.indexOf($index);
				if (index > -1) {
					vm.checkedCartItems.splice(index, 1);
				}
			}

			vm.toggleCartCheckedControls = (vm.checkedCartItems.length) ? true : false;
		}

		function clearCartSection() {
			vm.checkedCartItems = [];
			vm.toggleCartCheckedControls = false;
			var dataSet = [];

			switch (vm.searchPaneContent) {
				case 'Cart':
					dataSet = vm.cart;
					break;
				case 'Enrolled':
					dataSet = vm.enrolled;
					break;
				case 'Wait list':
					dataSet = vm.waitlist;
					break;
				case 'Dropped':
					dataSet = vm.dropped;
					break;
				case 'Saved for later':
					dataSet = vm.savedLater;
					break;
			}
			dataSet.map(function(cartItem) { cartItem.cartItemChecked = false; });
		}

		////////////////////
		// EVENT HANDLERS //
		////////////////////
		/**
		 * Triggers the search after the opening pane animation is complete. This was the only way to avoid the animation stutter that happens during an XHR
		 */
		function afterSearchPaneOpen() {
			// Make sure course pane is closed
			vm.showCoursePane = false;

			if (vm.searchPaneOpen === false) {
				vm.searchPaneOpen = true;
			}
		}

		/**
		 * When the search pane closes remove any existing search results for any subsequent searches
		 */
		function afterSearchPaneClosed() {
			vm.searchPaneOpen = false;
			vm.courseDetails = {};
			vm.courseSections = [];
		}

		function afterCoursePaneOpen() {
			vm.coursePaneOpen = true;
			if ($mdMedia('sm') || $mdMedia('xs')) {
				getCourseSections(vm.courseDetails.courseId, vm.courseDetails.subject.subjectCode, vm.selectedTerm);
				$timeout(function() {vm.loadingSections = false; }, 500);
			}
		}

		function afterSectionPaneClosed() {
			if (vm.sectionPaneBack === false) {
				vm.showCoursePane = false;
			}
			vm.sectionPaneBack = false;
		}

		function myCoursesClickEvent(content) {
			// Check if something other than the current content should be displayed
			if (content != vm.searchPaneContent) {
				clearCartSection();

				// Switch searchPaneContent flag
				switch (content) {
					case 'Cart':
						vm.showSectionPane = false;
						vm.showCoursePane = false;
						vm.searchPaneContent = 'Cart';
						vm.selectedIndex = undefined;
						break;
					case 'Enrolled':
						vm.showSectionPane = false;
						vm.showCoursePane = false;
						vm.searchPaneContent = 'Enrolled';
						vm.selectedIndex = undefined;
						break;
					case 'Wait list':
						vm.showSectionPane = false;
						vm.showCoursePane = false;
						vm.searchPaneContent = 'Wait list';
						vm.selectedIndex = undefined;
						break;
					case 'Dropped':
						vm.showSectionPane = false;
						vm.showCoursePane = false;
						vm.searchPaneContent = 'Dropped';
						vm.selectedIndex = undefined;
						break;
					case 'Saved for later':
						vm.showSectionPane = false;
						vm.showCoursePane = false;
						vm.searchPaneContent = 'Saved for later';
						vm.selectedIndex = undefined;
						break;
				}
			}
			vm.showSearchPane = true;
			vm.updatePanePositionClasses();
		}

		/**
		 * Sets classes to the panes for the scrolling effect. Primarily works on mobile.
		 */
		function updatePanePositionClasses() {
			if ((!vm.showSearchPane) && (!vm.showCoursePane)) {
				vm.paneNavClass = 'active';
				vm.paneSearchClass = 'right';
				vm.paneCourseClass = 'right';
				vm.paneSectionClass = 'right';
			} else if (vm.showSchedulerPane) {
				vm.paneNavClass = 'left';
				vm.paneSearchClass = 'left';
				vm.paneSchedulerClass = 'active';
				vm.paneCourseClass = 'right';
				vm.paneSectionClass = 'right';
			} else if ((vm.showSearchPane) && (!vm.showCoursePane)) {
				vm.paneNavClass = 'left';
				vm.paneSearchClass = 'active';
				vm.paneCourseClass = 'right';
				vm.paneSectionClass = 'right';
			} else if ((vm.showSearchPane) && (vm.showCoursePane) && (!vm.showSectionPane)) {
				vm.paneNavClass = 'left';
				vm.paneSearchClass = 'left';
				vm.paneCourseClass = 'active';
				vm.paneSectionClass = 'right';
			} else if ((vm.showSearchPane) && (vm.showCoursePane) && (vm.showSectionPane)) {
				vm.paneNavClass = 'left';
				vm.paneSearchClass = 'left';
				vm.paneCourseClass = 'left';
				vm.paneSectionClass = 'active';
			} else if (vm.showSchedulerPane) {
				vm.paneNavClass = 'left';
				vm.paneSearchClass = 'left';
				vm.paneSchedulerClass = 'active';
				vm.paneCourseClass = 'right';
				vm.paneSectionClass = 'right';
			}
		}

		function createScheduleForCourses() {
			// Get the IDs of every item selected in the user's cart.
			var cartItemIds = sharedService.getCheckedCartIds(vm.cart);

			// If no cart items were selected, schedule everything in the cart.
			if (cartItemIds.length === 0) {
				cartItemIds = vm.cart.map(function(item) {
					return item.id;
				});
			}

			// Add IDs of selected items to the state service so that those items will be displayed as
			//selected in the scheduler view.
			// stateService.selectedCartIds = cartItemIds;

			// Options passed to the scheduling service. Taken from the method:
			// SchedulerController.generate()
			var options = {
				'termCode': vm.selectedTerm,
				'cartItems': cartItemIds,
				'enrolled': [],
				'options': {}
			};

			if (vm.DEBUG) console.info('schedule post data', options);

			scheduleService.status().then(function(data) {
				if (!data.error) {
					if ((data.status === 'New') || (data.status === 'InProgress')) {
						toastService.infoMessage('You are currently finding schedules. Please wait and try again later.');
					} else {
						return scheduleService.schedule(vm.selectedTerm, options).then(function(data) {
							if (data) {
								vm.userPrefs.cachedDigest = courseDigestService.getDigest(vm.cartData);
								preferencesService.setPreferences(vm.userPrefs);

								// If there wasn't an error, navigate to the scheduler view.
								$location.url('/scheduler');
							} else {
								toastService.infoMessage('Unable to generate schedules due to server error');
								if (vm.DEBUG) console.error(data);
							}
						}).catch(function(error) { console.error(error); });
					}
				}
			}).catch(function(error) { console.error(error); });
		}

		function updateAppliedFilters() {
			vm.appliedFilters = vm.selectedSectionFilters;
			//updateSectionFilters();
		}

		function updateSectionFilters() {
			var numFilters = vm.appliedFilters.length;
			vm.totalCoursePackages = 0;
			vm.filteredTotalCoursePackages = 0;
			vm.courseSections = (numFilters) ? angular.copy(vm.courseSectionsData) : vm.courseSections;

			if (vm.hasPackages) {
				vm.courseSectionsData.forEach(function(coursePackages) { vm.totalCoursePackages += coursePackages.packages.length; });
			} else {
				vm.totalCoursePackages = vm.courseSectionsData.length;
			}

			if ((vm.showCoursePane === true) && (!numFilters)) {
				vm.courseSections = angular.copy(vm.courseSectionsData);
				vm.filteredTotalCoursePackages = vm.totalCoursePackages;
				return;
			}

			var t0 = performance.now();
			vm.courseSections = $filter('sectionFilter')(vm.courseSections, vm.appliedFilters);
			var t1 = performance.now();
			if (vm.DEBUG) console.log('section filtering took ' + (t1 - t0) + ' milliseconds.');

			if (vm.hasPackages) {
				vm.courseSections.forEach(function(coursePackages) { vm.filteredTotalCoursePackages += coursePackages.packages.length; });
			} else {
				vm.filteredTotalCoursePackages = vm.courseSections.length;
			}
		}

		function hasEnrollmentOptions(course) {
			return sharedService.hasEnrollmentOptions(course);
		}

		function toggleSidenavSection(result, cartIndex) {
			$mdSidenav('right', true).onClose(function() {
				vm.courseSections = [];
				vm.hasPackages = false;
				vm.hideSections = true;
				vm.updatingEnrolledErrors = [];
			});

			vm.hideSections = true;
			vm.loadingSectionPane = true;
			vm.selectedSection = false;
			var cartCourse = vm.cartData[cartIndex];

			if ($mdSidenav('right').isOpen() == false) {
				$mdSidenav('right').toggle().then(function() {
					if (!result.hasOwnProperty('studentEnrollmentStatus')) {
						var classNumber, query, matches = '', isLocked = false;
						vm.loadingSections = true;
						if (!result.enrollmentClassNumber && !angular.isUndefined(cartIndex)) {
							return getCourseSections().then(function() {
								vm.loadingSectionPane = false;
								vm.hideSections = false;
							});
						} else  {
							var classMeetingsNoExam = result.classMeetings.filter(function(meeting) { return meeting.meetingType != 'EXAM'; });
							classMeetingsNoExam.forEach(function(meeting){ matches += '{"match":{"sections.sectionNumber":"' + meeting.sectionNumber.toString() + '"}},'});
							matches = matches.slice(0,-1);
							query = '{"size":"1000","query":{"constant_score":{"filter":{"bool":{"must":[{"term":{"courseId":"' + cartCourse.courseId.toString() + '"}},' + matches + ']}}}}}';

							isLocked = true;
						}

						// If this is true that means there are likely no class meetings
						if (!matches) {
							query = '{"size":"1000","query":{"term":{"enrollmentClassNumber":' + result.enrollmentClassNumber + '}}}';
						}

						getCourseSections().then(function() {
							vm.loadingSections = false;
						});

						searchService.getEnrollmentPackageByNumber(vm.selectedTerm, query).then(function(data) {
							vm.loadingSectionPane = false;
							var foundSection;

							if (data.hits.hits.length > 1) {
								foundSection = data.hits.hits.filter(function(section) {
									if (section._source.enrollmentOptions.relatedClassNumber) {
										return section._source.enrollmentOptions.relatedClasses["0"] == (Number(result.relatedClassNumber1));
									} else {
										return (section._source.courseId == result.courseId) && (section._source.subjectCode == result.subject.subjectCode);
									}
								})[0]._source;
							} else {
								foundSection = data.hits.hits[0]._source;
							}

							if (isLocked || result.enrollmentClassNumber) {
								foundSection.sections.map(function(section) { section.checked = true; });
							}

							var numSections = foundSection.sections.length;
							if (numSections > 1) {
								foundSection.classMeetings = foundSection.sections[0].classMeetings;
								var parent = angular.copy(foundSection.sections[0]);
								foundSection.sections[numSections-1].showPackage = true;
								foundSection.sections[1].packageEnrollmentStatus = foundSection.packageEnrollmentStatus;
								foundSection.sections[1].enrollmentOptions = foundSection.enrollmentOptions;
								foundSection.sections[1].enrollmentRequirementGroups = foundSection.enrollmentRequirementGroups;
								foundSection.sections[1].creditRange = foundSection.creditRange;
								foundSection.sections[1].credits = cartCourse.credits;
								if (cartCourse.waitlist == 'Y') foundSection.sections[1].waitlistChecked = true;
								if (cartCourse.honors == 'Y') foundSection.sections[1].honorsChecked = true;
								if (cartCourse.classPermissionNumber) foundSection.sections[1].classPermissionNumber = cartCourse.classPermissionNumber;
								parent.packages = [foundSection.sections];
								vm.selectedSection = [Object.assign(parent, foundSection)];
								vm.hideSelectedSection = false;
								vm.parentPackageType = foundSection.sections[0].type;
								vm.hasPackages = true;

								if (vm.DEBUG) console.log('selected section', vm.selectedSection);
							} else {
								vm.hasPackages = false;
								foundSection.packages = foundSection.sections;
								foundSection.credits = cartCourse.credits;
								if (cartCourse.waitlist == 'Y') foundSection.waitlistChecked = true;
								if (cartCourse.honors == 'Y') foundSection.honorsChecked = true;
								if (cartCourse.classPermissionNumber) foundSection.classPermissionNumber = cartCourse.classPermissionNumber;
								vm.selectedSection = [Object.assign(foundSection.sections[0], foundSection)];
								vm.selectedSection[0].show = true;
								vm.hideSelectedSection = false;
							}
						}).catch(function(error) { console.error(error); });
					} else {
						vm.loadingSections = false;
						vm.loadingSectionPane = false;
						vm.hideSections = false;
						var matches = '', query, foundSection;

						if (angular.isUndefined(result.sections)) {
							result.classMeetings.forEach(function(section){ matches += '{"match":{"sections.sectionNumber":"' + section.sectionNumber.toString() + '"}},'});
						} else {
							result.sections.forEach(function(section){ matches += '{"match":{"sections.sectionNumber":"' + section.classSection.toString() + '"}},'});
						}
						matches = matches.slice(0,-1);
						query = '{"size":"1000","query":{"constant_score":{"filter":{"bool":{"must":[{"term":{"courseId":"' + result.courseId.toString() + '"}},' + matches + ']}}}}}';

						searchService.getEnrollmentPackageByNumber(vm.selectedTerm, query).then(function(data) {
							if (data.hits.hits.length > 1) {
								foundSection = data.hits.hits.filter(function(section) {
									if (section._source.enrollmentOptions.relatedClassNumber) {
										return section._source.enrollmentOptions.relatedClasses["0"] == (Number(result.relatedClassNumber1));
									} else {
										return (section._source.courseId == result.courseId) && (section._source.subjectCode == result.subject.subjectCode);
									}
								})[0]._source;
							} else {
								foundSection = data.hits.hits[0]._source;
							}

							foundSection.sections.forEach(function(section, index) {
								section.creditRange = foundSection.creditRange;
								section.credits = result.sections[0].numCreditsTaken.toString();
								section.honors = result.honors;
								section.honorsChecked = (result.honors == 'Y' || result.honors) ? true : false;
								section.instructionMode = foundSection.sections[0].instructionMode;
								section.classMaterials = foundSection.sections[0].classMaterials;
								section.addConsent = foundSection.sections[0].addConsent;
								section.footnotes = foundSection.sections[0].footnotes;
								section.comB = foundSection.sections[0].comB;
								section.dropConsent = foundSection.sections[0].dropConsent;
								section.instructorProvidedClassDetails = foundSection.instructorProvidedClassDetails;
								section.enrollmentOptions = foundSection.enrollmentOptions;
								section.enrollmentRequirementGroups = foundSection.enrollmentRequirementGroups;
							});

							var numSections = foundSection.sections.length;
							foundSection.credits = result.sections[0].numCreditsTaken;

							if (numSections > 1) {
								vm.hasPackages = true;
								vm.parentPackageType = foundSection.sections[0].type;
								foundSection.sections[0].creditRange = '';
								foundSection.sections[1].enrolled = true;
								foundSection.sections[1].show = true;
								foundSection.sections[0].packages = [foundSection.sections];
								vm.courseSections = [foundSection.sections[0]];
							} else {
								vm.hasPackages = false;
								foundSection.sections[0].enrolled = true;
								foundSection.sections[0].show = true;
								vm.courseSections = foundSection.sections;
							}
						});

						if (vm.DEBUG) console.log('course sections enrolled', result);
					}
				}).catch(function(error) { console.error(error); });
			} else {
				$mdSidenav('right').toggle();
			}
		}
	}

})();
