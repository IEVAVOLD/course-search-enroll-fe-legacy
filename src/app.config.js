(function() {
	'use strict';

	angular
		.module('app')
		.run(['$http', function($http) {
			// $http.defaults.headers.common = { 'X-API-Key': '' };
			// $http.defaults.headers.common.wiscedupvi = '';
			// $http.defaults.headers.common.uid = '';
			// $http.defaults.headers.common.wisceduisisemplid = '';
		}])
		.config(config);

	function config(constants, $routeProvider, $locationProvider, $mdThemingProvider, AnalyticsProvider, $httpProvider, $provide, $qProvider, $compileProvider) {
		var DEFAULT_TIMEZONE = 'CST';
		$locationProvider.html5Mode(true);
		// Fixes annoying JS error
		$qProvider.errorOnUnhandledRejections(false);

		moment.tz.add([
			'America/Chicago|CST CDT EST CWT CPT|60 50 50 50 50|01010101010101010101010101010101010102010101010103401010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010|-261s0 1nX0 11B0 1nX0 1wp0 TX0 WN0 1qL0 1cN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 11B0 1Hz0 14p0 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 RB0 8x30 iw0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1qN0 WL0 1qN0 11z0 1o10 11z0 1o10 11z0 1o10 11z0 1o10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1cN0 1cL0 1cN0 1cL0 s10 1Vz0 LB0 1BX0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 1cN0 1fz0 1a10 1fz0 1cN0 1cL0 1cN0 1cL0 1cN0 1cL0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 14p0 1lb0 14p0 1lb0 14p0 1nX0 11B0 1nX0 11B0 1nX0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Rd0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0 Op0 1zb0|92e5'
		]);
		moment.tz.setDefault('America/Chicago');

		// Removes debug data from comments
		if (!constants.DEBUG) {
			$compileProvider.debugInfoEnabled(false);
			$compileProvider.commentDirectivesEnabled(false);

			$provide.decorator('$exceptionHandler', function($delegate) {
				return function(exception, cause) {
					$delegate(exception, cause);
					ga(
						'send',
						'event',
						'AngularJS error',
						exception.message,
						exception.stack,
						0,
						true
					);
				};
			});

			window.addEventListener('error', function(err) {
				var lineAndColumnInfo = err.colno ? ' line:' + err.lineno + ', column:' + err.colno : ' line:' + err.lineno;
				ga(
					'send',
					'event',
					'JavaScript Error',
					err.message,
					err.filename + lineAndColumnInfo + ' -> ' +  navigator.userAgent,
					0,
					true
				);
			});
		}

		// $provide.decorator('dateFilter', ['$delegate', function($delegate) {
		// 	var oldDelegate = $delegate;

		// 	var standardDateFilterInterceptor = function(date, format, timezone) {
		// 		if (angular.isUndefined(timezone)) {
		// 			timezone = DEFAULT_TIMEZONE;
		// 		}
		// 		return oldDelegate.apply(this, [date, format, timezone]);
		// 	};

		// 	return standardDateFilterInterceptor;
		// }]);

		$httpProvider.interceptors.push('redirectInterceptor');

		AnalyticsProvider
			.setAccount('UA-65916804-1')
			.trackUrlParams(true)
			.ignoreFirstPageLoad(true);

		$routeProvider
			.when('/search', {
				templateUrl: 'view-search/search.html',
				controller: 'SearchController',
				controllerAs: 'vm',
				reloadOnSearch: false
			})
			.when('/my-courses', {
				templateUrl: 'view-my-courses/my-courses.html',
				controller: 'CoursesController',
				controllerAs: 'vm'
			})
			.when('/scheduler', {
				templateUrl: 'view-scheduler/scheduler.html',
				controller: 'SchedulerController',
				controllerAs: 'vm'
			})
			.when('/planner', {
				templateUrl: 'view-planner/planner.html',
				controller: 'PlannerController',
				controllerAs: 'vm'
			})
			.when('/degree-planner', {
				reloadOnUrl: true
			})
			.when('/audit', {
				templateUrl: 'view-audit/audit.html'
			})
			.otherwise('/search');

		// primary palette is based on the blue accent color from www.wisc.edu (#0479a8)
		$mdThemingProvider.definePalette('uwPrimary', {
			'50': 'B8E9FD',
			'100': '6DD3FC',
			'200': '36C2FA',
			'300': '05A4E4',
			'400': '058FC6',
			'500': '0479A8',
			'600': '03638A',
			'700': '034E6C',
			'800': '02384E',
			'900': '012330',
			'A100': 'B8E9FD',
			'A200': '0479A8',
			'A400': '058FC6',
			'A700': '034E6C',
			'contrastDefaultColor': 'light',    // whether, by default, text (contrast) on this palette should be dark or light
			'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
				'200', '300', '400', 'A100'],
			'contrastLightColors': undefined    // could also specify this if default was 'dark'
		});

		// accent palette is based on the 2016 UW red color (#c5050c)
		$mdThemingProvider.definePalette('uwAccent', {
			'50': 'FED5D7',
			'100': 'FC8B8F',
			'200': 'FB545A',
			'300': 'F90E17',
			'400': 'E3060E',
			'500': 'C5050C',
			'600': 'A7040A',
			'700': '890308',
			'800': '6B0307',
			'900': '4E0205',
			'A100': 'FED5D7',
			'A200': 'C5050C',
			'A400': 'E3060E',
			'A700': '890308',
			'contrastDefaultColor': 'light',    // whether, by default, text (contrast) on this palette should be dark or light
			'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
				'200', '300', '400', 'A100'],
			'contrastLightColors': undefined    // could also specify this if default was 'dark'
		});

		// Warning palette is based on a generic orange color (#E28332)
		$mdThemingProvider.definePalette('uwWarn', {
			'50': 'f1f1f1',
			'100': 'dddddd',
			'200': 'c6c6c6',
			'300': 'afafaf',
			'400': '9d9d9d',
			'500': '646569',
			'600': '848484',
			'700': '797979',
			'800': '6f6f6f',
			'900': '5c5c5c',
			'A100': 'fad3d3',
			'A200': 'f5a5a5',
			'A400': 'ff6868',
			'A700': 'ff4e4e',
			'contrastDefaultColor': 'light', // on this palette should be dark or light
			'contrastDarkColors': ['50', '100', //hues which contrast should be 'dark' by default
				'200', '300', '400', 'A100'],
			'contrastLightColors': undefined    // could also specify this if default was 'dark'
		});

		// Set themes
		$mdThemingProvider.theme('default')
			.primaryPalette('uwPrimary')
			.warnPalette('uwWarn')
			.accentPalette('uwAccent');
	}

})();
