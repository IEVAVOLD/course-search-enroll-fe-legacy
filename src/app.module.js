(function() {
	'use strict';

	// Declare app level module which depends on views, and components
	angular
		.module('app', [
			'app.core',
			'app.templates',
			'app.tabs',
			'app.search',
			'app.courses',
			'app.scheduler',
			'app.version',
			'app.constants',
			'app.debug'
		]);

})();
