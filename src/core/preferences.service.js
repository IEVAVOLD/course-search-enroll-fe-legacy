(function() {
	'use strict';
	/**
	 * Service used to store user preferences.
	 */
	angular
		.module('app.core')
		.factory('preferencesService', preferencesService);

	/* @ngInject */
	function preferencesService($http, constants) {
		var service = {
			preferences: {},
			getPreferences: getPreferences,
			setPreferences: setPreferences
		};
		return service;

		function getPreferences() {
			return $http.get('/api/planner/v1/preferences')
				.then(getPreferencesComplete)
				.catch(getPreferencesCompleteFailed);

			function getPreferencesComplete(response) {
				if (angular.isUndefined(response.data.scheduler)) {
					service.preferences = response.data;
					service.preferences.scheduler = { options: { waitlistedPackages: false,
																	specialGroupsPackages: false,
																	allowConflicts: false }};
					if (constants.DEBUG) console.log('scheduler user prefs was undefined');
				} else {
					service.preferences = response.data;
				}
				if (constants.DEBUG) console.log('user prefs', service.preferences);
			}

			function getPreferencesCompleteFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function setPreferences(data) {
			return $http.post('/api/planner/v1/preferences', data)
				.then(setPreferencesComplete)
				.catch(setPreferencesCompleteFailed);

			function setPreferencesComplete(response) {
				return response.data;
			}

			function setPreferencesCompleteFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}
	}

})();
