(function() {
	'use strict';

	// Defines a module used for the template cache
	angular.module('app.templates', []);

})();
