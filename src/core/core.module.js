(function() {
	'use strict';
	/**
	 * Module that includes other angular modules, reusable modules, and 3rd party modules.
	 */
	angular.module('app.core', [
		/*
		 * Angular modules
		 */
		'ngRoute', 'ngAnimate', 'ngAria', 'ngMessages',
		/*
		 * Our reusable cross app code modules
		 */
		'ngMaterial', 'angular-google-analytics',
		/*
		 * 3rd Party modules
		 */
		'ui.calendar'
	]).run(['$rootScope', '$location', 'Analytics', function($rootScope, $location, Analytics) {
		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams, options) {
			try {
				Analytics.trackPage($location.url());
			}
			catch (err) {
			//user browser is disabling tracking
			}
		});
	}]);

})();
