(function() {
	'use strict';

	angular
		.module('app.core')
		.factory('scheduleService', scheduleService);

	/* @ngInject */
	function scheduleService($http, preferencesService) {

		var service = {
			schedule: schedule,
			status: status,
			getSchedules: getSchedules,
			getAllBlocks: getAllBlocks,
			getBlock: getBlock,
			addBlock: addBlock,
			deleteBlock: deleteBlock
		};

		return service;

		function schedule(termCode, data) {
			// return $http.post('/api/scheduler/v1/schedules/' + termCode, data)
			return $http({
					url: '/api/scheduler/v1/schedules/' + termCode,
					data: data,
					method: 'POST',
					responseType: 'blob'
				})
				.then(addCompleted)
				.catch(addFailed);

			function addCompleted(response) {
				preferencesService.getPreferences().then(function () {
					// If this is the first time a user has generated schedules for this term,
					// add this term ID to the user preferences object to indicate that the
					// "Introduction" message should never be displayed for this term again.
					var userPrefs = preferencesService.preferences;
					if (Array.isArray(userPrefs.termsWithGeneratedSchedules) === false) {
						userPrefs.termsWithGeneratedSchedules = [termCode];
						preferencesService.setPreferences(userPrefs);
					} else if (userPrefs.termsWithGeneratedSchedules.indexOf(termCode) === -1) {
						userPrefs.termsWithGeneratedSchedules.push(termCode);
						preferencesService.setPreferences(userPrefs);
					}
				});

				return response.data;
			}

			function addFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function status() {
			return $http.get('scheduling/status')
				.then(checkStatus)
				.catch(checkStatusError);

			function checkStatus(response) {
				return response.data;
			}

			function checkStatusError(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function getSchedules(term) {
			return $http.get('/scheduling/results/?termCode=' + term)
				.then(addCompleted)
				.catch(addFailed);

			function addCompleted(response) {
				return response.data;
			}

			function addFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function getAllBlocks(term) {
			return $http.get('/api/scheduler/v1/blocks/?termCode=' + term)
				.then(addCompleted)
				.catch(addFailed);

			function addCompleted(response) {
				return response.data;
			}

			function addFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function getBlock(id) {
			return $http.get('/api/scheduler/v1/blocks/' + id)
				.then(addCompleted)
				.catch(addFailed);

			function addCompleted(response) {
				return response.data;
			}

			function addFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function addBlock(data) {
			return $http.post('/api/scheduler/v1/blocks', data)
				.then(addCompleted)
				.catch(addFailed);

			function addCompleted(response) {
				return response.data;
			}

			function addFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function deleteBlock(id) {
			return $http.delete('/api/scheduler/v1/blocks/' + id)
				.then(addCompleted)
				.catch(addFailed);

			function addCompleted(response) {
				return response.data;
			}

			function addFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

	}
})();
