(function() {
	'use strict';

	angular
		.module('app.core')
		.factory('enrollmentService', enrollmentService);

	function enrollmentService(constants, cartService, toastService, $http, $mdToast) {
		var DEBUG = constants.DEBUG;
		var isToastOpen = false;
		var service = {
			enroll: enroll,
			lastEnrollment: lastEnrollment,
			updateEnrolledCourse: updateEnrolledCourse,
			getEnrollmentEventDate: getEnrollmentEventDate
		};
		return service;

		function enroll(cart, term) {
			var enrollmentPostData = [];

			cart.forEach(function(cartDataItem) {
				if (cartDataItem.details.enrollmentClassNumber !== null) {
					var data = { };

					if (cartDataItem.waitlist === 'Y' || cartDataItem.waitlist === true) { data.waitlist = true; }
					if (cartDataItem.honors === 'Y' || cartDataItem.honors === true) { data.honors = true; }
					if (cartDataItem.classPermissionNumber && cartDataItem.classPermissionNumber !== null) { data.classPermissionNumber = cartDataItem.classPermissionNumber; }
					if (cartDataItem.relatedClassNumber1) { data.relatedClassNumber1 = cartDataItem.relatedClassNumber1; }
					if (cartDataItem.relatedClassNumber2) { data.relatedClassNumber2 = cartDataItem.relatedClassNumber2; }
					if (!cartDataItem.relatedClassNumber1 && !cartDataItem.relatedClassNumber2 && cartDataItem.enrollmentOptions.relatedClassNumber) {
						data.relatedClassNumber1 = cartDataItem.enrollmentOptions.relatedClasses[0];
						if (cartDataItem.enrollmentOptions.relatedClasses.length == 2) data.relatedClassNumber2 = cartDataItem.enrollmentOptions.relatedClasses[1];
					}
					if (cartDataItem.credits) { data.credits = cartDataItem.credits; }

					var tempObj = {
						courseId: cartDataItem.courseId,
						subjectCode: cartDataItem.details.subject.subjectCode,
						classNumber: cartDataItem.classNumber,
						options: data
					};

					enrollmentPostData.push(tempObj);
				}
			});

			if (enrollmentPostData.length) {
				if (DEBUG) console.log('Enrollment post data:', enrollmentPostData);

				return cartService.enroll(term, enrollmentPostData);
			}
		}

		function lastEnrollment() {
			return $http.get('/api/lastenrollment')
				.then(lastEnrollmentComplete)
				.catch(lastEnrollmentFailed);

			function lastEnrollmentComplete(response) {
				return response.data;
			}

			function lastEnrollmentFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}

		function updateEnrolledCourse(updateData) {
			updateData.vm.updatingEnrolled = true;
			var postData = {
				honors: (updateData.to.honors ? 'Y' : 'N'),
				credits: updateData.to.credits
			};

			if (updateData.relatedClassNumber1) postData.relatedClassNumber1 = updateData.relatedClassNumber1;
			if (updateData.relatedClassNumber2) postData.relatedClassNumber2 = updateData.relatedClassNumber2;
			if (updateData.waitlist) postData.waitlist = true;
			if (updateData.classPermissionNumber) postData.classPermissionNumber = updateData.classPermissionNumber;

			// Hit the enrolled course update API with selected changes
			return $http.put('api/enroll/v1/update/' + updateData.termCode + '/' + updateData.enrollmentClassNumber, postData)
				.then(function(response) {
					// Check if there was an error
					if (response.data.length > 0 && response.data[0].description !== 'This class has been updated.') {
						updateData.vm.updatingEnrolled = false;
						updateData.vm.updatingEnrolledErrors = response.data;

						// Revert the data, nothing has updated
						updateData.course.honors = updateData.from.honors;
						updateData.course.credits = updateData.from.credits;

						updateData.section.honorsChecked = updateData.from.honors;
						updateData.section.credits = updateData.from.credits;
					} else {
						updateData.vm.updatingEnrolled = false;
						var text = "Your " + updateData.change + " selection has been updated.";
						updateData.vm.updatingEnrolledErrors = [];

						// Update the data in the vm
						updateData.course.honors = updateData.to.honors;
						updateData.course.credits = updateData.to.credits;

						showToastEnrolledChangeStatus(text, updateData);
					}
				}).catch(function(e) {
					updateData.vm.updatingEnrolled = false;
					updateData.vm.updatingEnrolledErrors = [{description: e.data.message}];
					if (DEBUG) console.error(e);
				});
		}

		function showToastEnrolledChangeStatus(text, updateData) {
			if (isToastOpen) {
				$mdToast
					.hide()
					.then(function() {
						isToastOpen = false;
						$mdToast.show({
							position: 'bottom center',
							controller: ToastController,
							templateUrl: 'components/enrolled-course-changes-toast/enrolled-course-changes-toast.html',
							locals: {
								text: text,
								updateData: updateData
							},
							hideDelay: 10000
						});
					});
			} else {
				$mdToast.show({
					position: 'bottom center',
					controller: ToastController,
					templateUrl: 'components/enrolled-course-changes-toast/enrolled-course-changes-toast.html',
					locals: {
						text: text,
						updateData: updateData
					},
					hideDelay: 10000
				});
				isToastOpen = true;
			}

			function ToastController($scope, text, updateData) {
				$scope.text = text;
				$scope.updateData = updateData;

				// Undo the selection and hit the API to re update this data
				$scope.undo = function() {
					$mdToast
						.hide()
						.then(function() {
							isToastOpen = false;
							updateData.vm.updatingEnrolled = true;
							var postData = {
								honors: updateData.from.honors,
								credits: updateData.from.credits,
							};

							if (updateData.relatedClassNumber1) postData.relatedClassNumber1 = updateData.relatedClassNumber1;
							if (updateData.relatedClassNumber2) postData.relatedClassNumber2 = updateData.relatedClassNumber2;
							if (updateData.classPermissionNumber) postData.classPermissionNumber = updateData.classPermissionNumber;

							return $http.put('api/enroll/v1/update/' + updateData.termCode + '/' + updateData.enrollmentClassNumber, postData)
								.then(function(response) {
									updateData.vm.updatingEnrolled = false;

									// Check if there was an error
									if(response.data.length > 0 && response.data[0].description !== 'This class has been updated.') {
										updateData.vm.updatingEnrolledErrors = response.data;
									} else {
										updateData.vm.updatingEnrolledErrors = [];
										// Update the data in the vm
										updateData.course.honors = updateData.from.honors;
										updateData.course.credits = updateData.from.credits;

										updateData.section.honorsChecked = updateData.from.honors;
										updateData.section.credits = updateData.from.credits;

										showToastEnrolledChangeStatus('Your ' + updateData.change + ' selection has been undone');
									}
								}).catch(function (e) {
									updateData.vm.updatingEnrolled = false;
									updateData.vm.updatingEnrolledErrors = [{description: e.data.message}];
								});
						});
				};
			}
		}

		function getEnrollmentEventDate(data) {
			return $http.get('/api/enroll/v1/studentInfo', { cache:true })
				.then(getEnrollmentEventDateComplete)
				.catch(getEnrollmentEventDateFailed);

				function getEnrollmentEventDateComplete(response) {
				return response.data;
			}

			function getEnrollmentEventDateFailed(error) {
				return {
					'errorDescription': 'Error retrieving data',
					'error': error
				};
			}
		}
	}
})();
