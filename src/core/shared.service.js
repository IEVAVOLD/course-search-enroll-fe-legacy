(function() {
	'use strict';
	/**
	 * Service used to share functions across controllers.
	 */
	angular
		.module('app.core')
		.filter('momentFilter', function() {
			return function(val, format) {
				return moment(new Date(val)).format(format);
			};
		})
		.factory('sharedService', sharedService);

	/* @ngInject */
	function sharedService() {

		var service = {
			hasEnrollmentOptions: hasEnrollmentOptions,
			getCheckedCartIds: getCheckedCartIds,
			getCartIdFromCourse: getCartIdFromCourse,
			getSelectedSections: getSelectedSections,
			getSelectedPackages: getSelectedPackages,
			getCheckedCartItems: getCheckedCartItems,
			getCheckedEnrolledNumbers: getCheckedEnrolledNumbers
		};
		return service;

		function hasEnrollmentOptions(course) {
			if (course.hasOwnProperty('enrollmentOptions')) {
				// Decided that displaying a which courses have related class numbers is not really helpful to a user.
				return (course.enrollmentOptions.honors ||
						course.enrollmentOptions.waitlist ||
						course.creditRange.indexOf('-') !== -1 ||
						// course.enrollmentOptions.relatedClassNumber ||
						course.enrollmentOptions.classPermissionNumberNeeded);
			} else {
				return false;
			}
		}

		function getCheckedCartIds(cart) {
			return cart.filter(function(item) {
					return item.cartItemChecked === true;
				}).map(function(item) {
					return item.id;
				}).sort(function(a, b) { return a.id - b.id; });
		}

		function getSelectedSections(sections) {
			return sections.filter(function(section) {
				return section.multiChecked == true;
			}).map(function(section) { return section.docId });
		}

		function getSelectedPackages(packages) {
			return [].concat.apply([], packages.map(function(section) {
				return section.packages.map(function(singlePackage) {
					if (singlePackage.length > 1 && singlePackage[1].multiChecked)
						return singlePackage[0].docId;
					else if (singlePackage[0].multiChecked){
						return singlePackage[0].docId;
					}
				});
			})).filter(function(docId) { return docId });
		}

		function getCartIdFromCourse(cart, course) {
			return cart.filter(function(item) {
				return item.courseId == course.courseId && item.subjectCode == course.subject.subjectCode;
			})[0].id.toString();
		}

		function getCheckedCartItems(cart) {
			return cart.filter(function(item) {
					return item.cartItemChecked === true;
				}).sort(function(a, b) { return a.id - b.id; });
		}

		function getCheckedEnrolledNumbers(enrolled) {
			return	[].concat.apply([], enrolled.filter(function(item) {
						return item.cartItemChecked === true;
					}).map(function(course) {
						return course.sections.map(function(section) {
							return Number(section.classNumber);
						});
					}));
		}
	}
})();
