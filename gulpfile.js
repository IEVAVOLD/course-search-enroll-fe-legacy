var gulp			= require('gulp');
var sass			= require('gulp-sass');
var concat			= require('gulp-concat');
var autoprefixer	= require('gulp-autoprefixer');
var sourcemaps		= require('gulp-sourcemaps');
var uglify			= require('gulp-uglify');
var ngAnnotate		= require('gulp-ng-annotate');
var bump			= require('gulp-bump');
var rename			= require('gulp-rename');
var runSequence		= require('run-sequence');
var ngConstant		= require('gulp-ng-constant');
var plumber			= require('gulp-plumber');
var templateCache	= require('gulp-angular-templatecache');
var htmlmin			= require('gulp-htmlmin');
var clean			= require('gulp-clean');
var gutil			= require('gulp-util');
var s3				= require('gulp-s3-upload');
// var debug			= require('gulp-debug');

var input			= './src/assets/sass/*.scss';
var templates		= ['./src/**/*.html', '!node_modules/**/*'];
var outputCSS		= './dist/css';
var outputJs		= './dist/js';

// Modules have to be first due to the concat that happens
// Adds analytics to bundle to avoid being blocked by ad blockers
var jsSource		= ['src/**/*module.js',
						'src/**/*.js',
						'src/.tmp/templates.js',
						'node_modules/**/*.js',
						'!node_modules/**/!(angular-google-analytics).js',
						'!src/**/*.min.js',
						'!src/**/*.test.js',
						'!src/**/*_test.js'];

var watchJsSource	= ['src/**/*module.js',
						'src/**/*.js',
						'!src/.tmp/templates.js',
						'!src/core/debug.module.js',
						'!src/**/*.min.js',
						'!src/**/*.min.js',
						'!src/**/*.test.js',
						'!src/**/*_test.js',
						'!node_modules/**/*.js'];

var sassOptions = {
	errLogToConsole: true,
	outputStyle: 'compressed'
};

gulp.task('s3upload', function() {
	'use strict';
	gulp.src('./node_modules/**')
		.pipe(s3({
			Bucket: 'enroll-app-bower-components',
			ACL:    'public-read'
		}, {
			maxRetries: 5
		}));
});

gulp.task('sass', function() {
	'use strict';
	return gulp.src('./src/assets/sass/imports.scss')
		.pipe(sourcemaps.init())
			.pipe(sass(sassOptions).on('error', sass.logError))
			.pipe(autoprefixer({
				browsers: ['last 2 versions'],
				cascade: false
			}))
			.pipe(concat('app.css'))
		.pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(outputCSS));
});

gulp.task('sass:watch', function() {
	'use strict';
	gulp.watch(input, ['sass']);
});

gulp.task('js', ['debug'], function() {
	'use strict';
	return gulp.src(jsSource, {base: './'})
	// .pipe(debug())
	.pipe(plumber())
	.pipe(ngAnnotate())
	.pipe(sourcemaps.init())
		.pipe(concat('app.js'))
	.pipe(sourcemaps.write('.', { sourceRoot: '.'}))
	.pipe(gulp.dest(outputJs));
});

gulp.task('js:watch', function() {
	'use strict';
	gulp.watch(watchJsSource, ['js']);
});

gulp.task('html:watch', function() {
	'use strict';
	gulp.watch(templates, function() {
		runSequence('min:temps', 'js');
	});
});

gulp.task('min:temps', function() {
	'use strict';
	return gulp.src(templates)
		.pipe(templateCache({ root: '',filename: 'templates.js', module: 'app.templates'}))
		.pipe(htmlmin({collapseWhitespace: true, removeComments: true}))
		.pipe(gulp.dest('src/.tmp'));
});

gulp.task('clean', function() {
	'use strict';
	return gulp.src(['src/.tmp/templates.js', 'dist'], {read: false})
		.pipe(clean());
});

// allows updating major,minor,patch version: "gulp bump --semver major"
// defaults to patch update
gulp.task('bumpVersion', function() {
	'use strict';
	var option, i = process.argv.indexOf('--semver');
	if (i > -1) {
		option = process.argv[i + 1];
	}
	return gulp
		.src('./config.json')
		.pipe(bump({type: option}))
		.pipe(gulp.dest('./'));
});

// Bumps version, compiles js for production
gulp.task('bump', ['bumpVersion'], function() {
	'use strict';
	var configJson = require('./config.json');
	return ngConstant({
		constants: configJson,
		stream: true,
		name: 'app.constants',
		wrap: true
	})
	.pipe(rename({basename: 'constants.module'}))
	.pipe(gulp.dest('src/core'));
});

// Sets debug to true for development. Updates debug.module.js
// Only runs with a npm install
gulp.task('debug', function() {
	'use strict';
	return ngConstant({
		constants: { 'constants': {'DEBUG': true, 'public': false}},
		stream: true,
		name: 'app.debug',
		wrap: true
	})
	.pipe(rename({ basename: 'debug.module'}))
	.pipe(gulp.dest('src/core'));
});

gulp.task('debug:false', function() {
	'use strict';
	return ngConstant({
		constants: {'constants': {'DEBUG': false}},
		stream: true,
		name: 'app.debug',
		wrap: true
	})
	.pipe(rename({basename: 'debug.module'}))
	.pipe(gulp.dest('src/core'));
});

gulp.task('js:minify', function() {
	'use strict';
	return gulp.src(jsSource, {base: './'})
		.pipe(ngAnnotate())
		.pipe(concat('app.js'))
		.pipe(uglify())
			.on('error', function(err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
		.pipe(gulp.dest(outputJs));
});

gulp.task('index', function() {
	'use strict';
	return gulp
		.src(['./src/index.html', './favicon.ico'])
		.pipe(gulp.dest('./dist', {overwrite: true}));
});

gulp.task('vendor', function() {
	'use strict';
	return gulp
		.src('./node_modules/**/*')
		.pipe(gulp.dest('./dist/node_modules', {overwrite: true}));
});

gulp.task('imgs', function() {
	'use strict';
	return gulp
		.src('./src/assets/img/*')
		.pipe(gulp.dest('./dist/img', {overwrite: true}));
});

gulp.task('build:prod', function(cb) {
	'use strict';
	runSequence('clean', 'min:temps', 'debug:false', 'js:minify', 'sass', 'index', 'imgs', 'vendor', cb);
});

gulp.task('build', function(cb) {
	'use strict';
	runSequence('clean', 'min:temps', 'index', 'imgs', [ 'sass', 'js'], cb);
});

gulp.task('default', ['sass:watch', 'js:watch', 'html:watch']);
